﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Culling : MonoBehaviour
{
    
    public List<Renderer> renderers = new List<Renderer>();


    // Start is called before the first frame update
    void Start()
    {
        Renderer[] rens = Resources.FindObjectsOfTypeAll<Renderer>();
        foreach (var ren in rens)
        {
            if (ren.gameObject.scene.isLoaded && ren != null)
            {
                renderers.Add(ren);
            }
        }
    }

    private void Update()
    {
        foreach (var ren in renderers)
        {
            if (ren != null)
            {
                if (ren.IsVisibleFrom(Camera.main))
                {
                    ren.enabled = true;
                }
                else
                {
                    ren.enabled = false;
                }
            }         
        }    
    }


}
