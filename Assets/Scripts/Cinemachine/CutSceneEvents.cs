﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

[RequireComponent(typeof(AudioSource))]

public class CutSceneEvents : MonoBehaviour
{
    public TextMesh ArcadeName;
    public InputField NameIF;
    public GameObject NameOfArcadeInput;
    public GameObject MaleOrFemaleCanvas;
    public GameObject MalePlayer;
    public GameObject FemalePlayer;
    public CurrentSave currentSave;
    public GameObject StoryCanvas;
    //public ParticleSystem Dust;
    public AudioSource PausedAudioSource;
    public AudioClip UiClick;
    public AudioClip KeyboardPress;


    [HideInInspector]
    public bool cutsceneEnded = false;
    private EventSystem eventSystem;
    public GameObject firstSelected;
    public GameObject secondSelected;

    public void Start()
    {

        MaleOrFemaleCanvas.SetActive(false);
        NameOfArcadeInput.SetActive(false);
        ArcadeName.text = "Wondercade";
        currentSave = FindObjectOfType<CurrentSave>();
        // Dust.Play();
        cutsceneEnded = false;
        Time.timeScale = 1;
        StoryCanvas.SetActive(true);
        eventSystem = FindObjectOfType<EventSystem>();

    }


    private void GenderUiAppears()
    {
        PausedAudioSource = GetComponent<AudioSource>();
        Time.timeScale = 0;
        StoryCanvas.SetActive(false);
        if (firstSelected != null && eventSystem != null)
        {
            eventSystem.SetSelectedGameObject(firstSelected);
        }
        MaleOrFemaleCanvas.SetActive(true);
        //sets the canvas ui to be seen to the player by an animation event
    }

    public void MaleActive()
    {
        StartCoroutine("A");

    }



    IEnumerator A()
    {
        yield return new WaitForSecondsRealtime(.5f);

        PausedAudioSource.PlayOneShot(UiClick, 1);
        //serialization.gameData.gender = "Male";

        MalePlayer.SetActive(true);
        FemalePlayer.SetActive(false);
        //timescale normalised on character selection resuming cutscene

        currentSave.playersGender = "Male";


    }


    public void FemaleActive()
    {
        StartCoroutine("B");
    }

    IEnumerator B()
    {
        yield return new WaitForSecondsRealtime(.5f);
        PausedAudioSource.PlayOneShot(UiClick, 1);
        //serialization.gameData.gender = "Female";

        //timescale normalised on character selection resuming cutscene
        FemalePlayer.SetActive(true);
        //if female butotn is pressed female player is set active
        MalePlayer.SetActive(false);
        //Male Player is set inactive
        currentSave.playersGender = "Female";



    }

    public void ConfirmedGender()
    {

        PausedAudioSource.PlayOneShot(UiClick, 1);
        // Dust.Play();
        Time.timeScale = 1;


        MaleOrFemaleCanvas.SetActive(false);



    }


    public void PlaySoundKeyboard()
    {
        PausedAudioSource.PlayOneShot(KeyboardPress, 1);


    }

    public void ArcadeNameChangeAppears()
    {
        if (Time.timeScale == 1)
        {
            // Dust.Stop();
            Time.timeScale = 0;

            NameOfArcadeInput.SetActive(true);
            //sets the canvas to be seen on animation event

            if (secondSelected != null && eventSystem != null)
            {
                eventSystem.SetSelectedGameObject(secondSelected);
            }


            //GameObject.Find("EventSystem").GetComponent<EventSystem>().SetSelectedGameObject(firstSelected);

            ArcadeName.text = NameIF.text;
            currentSave.arcadesName = ArcadeName.text;

            //whats written in the input field is transfered to the text mesh
        }

        ArcadeName.text = NameOfArcadeInput.GetComponentInChildren<InputField>().text;
        currentSave.arcadesName = ArcadeName.text;
    }



    private void NameInputSelected()
    {



        PausedAudioSource.PlayOneShot(UiClick, 1);
        //  Dust.Play();
        Time.timeScale = 1;
        //TimeScale resumes leading the animation to continue
        NameOfArcadeInput.SetActive(false);
        //The game object is removed as it is no longer useds in the playthrough








    }

   /*public void Footstep()

    {
       AudioClip = GetRandomClip();
        AudioSource.PlayOneShot(UiClick);



    }

   private AudioClip GetRandomClip();
    {
   return clips{UnityEngine.Random.Range(0, uiClips.Length)};

   }*/

    public void LoadScene()
    {
        Time.timeScale = 1;
         //serialization.SaveGame();
        cutsceneEnded = true;
       // SceneManager.LoadScene("James B New");
    }

    private void OnApplicationQuit()
    {
        Time.timeScale = 1;

    }


}



