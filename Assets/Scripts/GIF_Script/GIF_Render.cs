﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GIF_Render : MonoBehaviour{

    [SerializeField] private Texture2D[] frames;
    [SerializeField] private Texture2D[] upgradedFrames;
    private Texture2D[] currentFrames; 
    [SerializeField] private float fps = 10.0f;

    private Material mat;
    private Renderer ren;
    private bool displayNewGraphics = false;
    public Material upgradedMat;
    public Material brokenMat;
    private Arcade arcade;
    public bool mainMenu = false;

    void Start()
    {
        mat = GetComponent<Renderer>().material;
        arcade = transform.root.GetComponent<Arcade>();
        ren = GetComponent<Renderer>();
        if (arcade != null)
        {
            if (arcade.currentLevel < arcade.levelPrefabs.Count)
            {
                currentFrames = frames;
            }
        }
        if (mainMenu)
        {
            currentFrames = frames;
        }

    }

    void Update()
    {
        //if the main menu
        if (mainMenu)
        {
            RenderGIF(); //render the gif
            return;
        }

        if (arcade != null)
        {
            if (arcade.state != ArcadeState.BROKEN)
            {
                if (!displayNewGraphics)
                {
                    if (ren.material != mat)
                    {
                        ren.material = mat;
                    }
                }
                if (arcade.currentLevel == arcade.levelPrefabs.Count)
                {
                    if (!displayNewGraphics)
                    {
                        ren.material = upgradedMat;
                        currentFrames = upgradedFrames;
                        displayNewGraphics = true;
                    }
                }
                if (arcade.currentLevel < arcade.levelPrefabs.Count)
                {
                    if (displayNewGraphics)
                    {
                        ren.material = mat;
                        currentFrames = frames;
                        displayNewGraphics = false;
                    }
                }

                RenderGIF();

            }
            else
            {
                ren.material = brokenMat;
                displayNewGraphics = false;
            }
        }
    }

    public void RenderGIF()
    {
        int index = (int)(Time.time * fps);
        if (currentFrames.Length > 0)
        {
            index = index % currentFrames.Length;
            ren.material.mainTexture = currentFrames[index];
        }
    }

}