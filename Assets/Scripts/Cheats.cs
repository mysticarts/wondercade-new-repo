﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheats : MonoBehaviour {

    public Economy economy;
    public Progression progression;

    public bool coinsCheatActive = false;
    public void GiveMoneyCheat() {
        economy.Give(1000);
    }

    public void UnlockAllItemsCheat()
    {
        int itemCount = progression.itemsToUnlock.Count;
        for (int i = 0; i < itemCount; i++)
        {
            progression.UnlockItem(progression.itemsToUnlock[0].iname, true);
        }
    }

}
