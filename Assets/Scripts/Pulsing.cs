﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsing : MonoBehaviour {

    public bool pulsing = false;

    private float lerp = 0;
    private Vector3 minScale = new Vector3(.95f,.95f,.95f);
    private Vector3 maxScale = new Vector3(1.2f,1.2f,1.2f);
    public void Update() {

        if((MachineManager.vendors.Count == 0 && transform.parent.GetComponent<ShopItem>().item.iname == "Food Machine")
            || (MachineManager.vendors.Count == 1 && transform.parent.GetComponent<ShopItem>().item.iname == "Soda Machine Round")) {
            pulsing = true;
        } else {
            pulsing = false;
            transform.localScale = new Vector3(1,1,1);
        }

        if(pulsing) {
            lerp += 1.3f * Time.unscaledDeltaTime;
            transform.localScale = Vector3.Lerp(minScale, maxScale, lerp);

            //flip the lerp:
            if(lerp > 1f) {
                Vector3 temp = maxScale;
                maxScale = minScale;
                minScale = temp;
                lerp = 0;
            }
        }
    }
}
