﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlacementConditions : MonoBehaviour
{
    [HideInInspector]
    public bool canPlace = true;
    private Economy economy; //get access to wallet balance
    public TextMeshProUGUI moneyDisplay; //the UI displaying the current wallet balance text
    public BuildingManager buildingManager; 

    private void Start()
    {
        economy = FindObjectOfType<Economy>();
    }

    // Update is called once per frame
    void Update()
    {
        //if an item is selected and is in purchase mode not edit mode)
        if (buildingManager.selectedItem != null && !buildingManager.editMode)
        {
            int predictedBalance = economy.money - buildingManager.selectedItem.cost;
            if (predictedBalance < 0) //if the user cant afford the item, prevent placement
            {
                canPlace = false;
            }
        }

        if (canPlace) //set the light colour to green if conditions are met
        {
            SetColor(Color.green);
        }
        if (!canPlace) //set red if the conditions aren't met
        {
            SetColor(Color.red);

        }
    }

    void SetColor(Color color)
    {
        //if there is an item selected
        if (buildingManager.selectedItem != null)
        {
            buildingManager.placementLight.color = color;
        }
        
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer != 8) //if colliding with anything but the grid cells
        {
            canPlace = false; //dont allow placement

        }
    }


    private void OnTriggerExit(Collider other)
    {
            canPlace = true; //allow placement.
    }





}
