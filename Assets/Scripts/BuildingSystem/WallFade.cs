﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallFade : MonoBehaviour
{
    public List<Renderer> wallsRenders = new List<Renderer>();
    // private List
    public float alphaValue = 0;
    public BuildingManager buildingManager;
    //public float alphaValue = 0;
    public Transform overlapBox;
    public LayerMask layerMask;
    private bool resetWalls = false;
    // Update is called once per frame  


    void Update()
    {
        if (buildingManager.purchaseMode)
        {
            resetWalls = true; //reset the walls when not in edit mode.
            Collider[] closestWalls = Physics.OverlapBox(overlapBox.position, overlapBox.localScale / 2, overlapBox.rotation, layerMask);

            for (int i = 0; i < wallsRenders.Count; i++)
            {
                for (int j = 0; j < closestWalls.Length; j++)
                {
                    if (wallsRenders[i] != null)
                    {
                        if (wallsRenders[i] == closestWalls[j].GetComponent<Renderer>())
                        {
                            ChangeAlpha(wallsRenders[i], alphaValue);
                            break;
                        }
                        else
                        {
                            ChangeAlpha(wallsRenders[i], 1);
                        }
                    }                   
                }
            }
        }
        else
        {
            if (resetWalls) //upon exiting edit mode, reset the walls. (Done to only reset once).
            {
                foreach (var ren in wallsRenders)
                {
                    ChangeAlpha(ren, 1);
                }
                resetWalls = false;
            }
        }



    }
    void ChangeAlpha(Renderer ren, float alpha)
    {
        foreach (var mat in ren.materials)
        {
            if (mat != null)
            {
                mat.color = new Vector4(mat.color.r, mat.color.g, mat.color.b, alpha);
            }
        }
    }
}
