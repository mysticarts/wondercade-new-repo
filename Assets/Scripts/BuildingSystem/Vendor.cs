﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public enum VendorType {
    DRINK,
    FOOD
}

public class Vendor : Machines {
    public MenuUI_Vending menu_vending;

    private ViewManager viewManager;
    public VendorType vendorType;
    [HideInInspector]
    public bool occupied = false;

    //stock stats:
    public int costPerStock = 1;
    public float maxStock = 10;
    public int maxStockPerUpgrade = 0;
    public int stock = 0;

    //diegetic UI:
    public GameObject manageButton; //diegetic ui
    public GameObject stockButton; //diegetic ui
    public Vector3 poppedUpSizeManage = new Vector3(1, 0.03f, 0.5f);
    public Vector3 poppedUpSizeStock = new Vector3(1, 0.03f, 0.5f);
    
    private Vector3 initialDiegetic_Scale; //reference to the initial editor scale
    private Vector3 initialDiegetic_Pos; //reference to the initial editor position
    private GameObject firstPersonDiegeticTransform; //use this position for the manageButton in first person view

    //analytics:
    public int transactions = 0;
    public int totalSales = 0;
    public int totalCostOfStock = 0;

    public GameObject vendorItem;

    //the position the AI stand at when buying stock.
    public Transform AIStandingPos;

    private NotificationSystem notifications;

    private void Start() {
        base.Start();
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();

        UpdateBar(menu_vending.stockBar, stock / maxStock);
        menu_vending.stock.text = "" + (int)stock;
        UpdateBar(menu_vending.levelBar, (float)currentLevel / (float)levelPrefabs.Count);
        menu_vending.level.text = "" + currentLevel;
        UpdateBar(menu_vending.stockBar, (float)stock / (float)maxStock);

        viewManager = Camera.main.GetComponent<ViewManager>();
        initialDiegetic_Scale = poppedUpSizeManage;
        initialDiegetic_Pos = manageButton.transform.parent.position;
        firstPersonDiegeticTransform = this.transform.Find("FirstPersonDiegeticTransform").gameObject;
    }

    public void Update() {
        base.Update();

        if(customer != null) {
            if(Vector3.Distance(customer.transform.position, AIStandingPos.transform.position) > 2 && occupied) {
                customer = null;
                occupant = "";
            }
        }

        if(customer != null)
            occupied = true;
        else occupied = false;

        if(playerIsInRadius) {
            if(input.GetButtonDown("PlayArcade"))
                BuyStock();
        }

    }

    public override void Upgrade() {
        if(currentLevel < levelPrefabs.Count) {
            int costToUpgrade = initialCostToUpgrade + ((currentLevel - 1) * costIncreasePerLevel);
            if(economy.money >= costToUpgrade) {
                maxStock += maxStockPerUpgrade;
                UpdateBar(menu_vending.stockBar, (float)stock / (float)maxStock);
            }
        }
        base.Upgrade();      
    }


    public override void ShowDiegeticUI(bool show) {
        //pop up manage button:
        
        if(viewManager == null) viewManager = Camera.main.GetComponent<ViewManager>();
        if(viewManager != null && viewManager.currentView == View.FIRST) {
            poppedUpSizeManage = initialDiegetic_Scale/3;
            poppedUpSizeStock = initialDiegetic_Scale/3;
            manageButton.transform.parent.position = firstPersonDiegeticTransform.transform.position; //set the diegetic UI to first person size
        } else {
            poppedUpSizeManage = initialDiegetic_Scale;
            poppedUpSizeStock = initialDiegetic_Scale;
            manageButton.transform.parent.position = initialDiegetic_Pos;
        }

        if(show) {
            Tween.LocalScale(manageButton.transform, poppedUpSizeManage, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            if(stock != maxStock)
                //pop up stock button
                Tween.LocalScale(stockButton.transform, poppedUpSizeStock, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        } else {
            //pop down all buttons:
            Tween.LocalScale(manageButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            Tween.LocalScale(stockButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
        }

    }

    public override void SetMenu() {
        base.menu = menu_vending;
    }

    public void BuyStock() {
        if(stock < (maxStock)) {
            if(economy.money >= costPerStock) {
                economy.Deduct(costPerStock);
                stock++;

                //UI:
                menu_vending.stock.text = "" + (int)stock;
                UpdateBar(menu_vending.stockBar, (float)(stock/maxStock));

                totalCostOfStock += costPerStock;
            } else notifications.Add(new Notification("Financial Advisor", "Not enough funds to buy more stock!"));
        } else notifications.Add(new Notification("Vending Management", "This machine is already fully stocked!"));
    }

    public void TakeStock() {
        if(stock > 0) {
            stock--;
            menu_vending.stock.text = "" + (int)stock;
            UpdateBar(menu_vending.stockBar, (float)(stock / maxStock));
        }
    }

    //serialise vendor data
    public override void SaveData(GameData data) {
        iData.stockLevel = stock;
        iData.itemLevel = currentLevel;
        iData.maxStock = (int)maxStock;
        iData.occupied = occupied;
        if(occupant !=  null)
            iData.AIOccupant = occupant;

        base.SaveData(data);
    }

    //load serialised data
    public override void LoadData(GameData data) {  
        base.LoadData(data);

        stock = iData.stockLevel;
        currentLevel = iData.itemLevel;
        costSlider.value = ussageCost;
        maxStock = iData.maxStock;
        occupied = iData.occupied;
        if(iData.AIOccupant != null)
            occupant = iData.AIOccupant;
    }

}
