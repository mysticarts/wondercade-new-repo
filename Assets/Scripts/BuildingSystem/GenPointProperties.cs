﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum GridType
{
    FLOOR = 0,
    WALL = 1,
}


public class GenPointProperties : MonoBehaviour
{
    [Tooltip("Assign the grid type (Wall or Floor)")]
    public GridType gridType;
    [Tooltip("Is the grid available from the start of the game")]
    public bool unlockedOnStart = false;
    [Tooltip("Upon the grid getting destroyed, what walls do you want to be generated and unlocked")]
    public List<GameObject> gridsToUnlock = new List<GameObject>();
    [Tooltip("The display mesh")]
    public GameObject displayWall;
    [Tooltip("Grid size along the x axis")]
    public float sizeX = 0;
    [Tooltip("Grid size along the y axis")]
    public float sizeY = 0;
    [Tooltip("Grid size along the z axis")]
    public float sizeZ = 0;
    BuildingManager buildingManager;
    [Tooltip("If this wall is destroyable, attach the button or UI associated with destroying this wall")]
    public GameObject destructionUI;
    [Tooltip("if wall, how much does it cost to destroy this wall.")]
    public int destructionCost = 0;
    [Tooltip("The cleaning button to unlock when the this grid gets destroyed.")]
    public GameObject cleaningButton;
    [Tooltip("Attach all the lights available in the building that changes colour.")]
    public List<GameObject> roomLights = new List<GameObject>();

    private void Start()
    {
        buildingManager = FindObjectOfType<BuildingManager>();
    }

    /// <summary>
    /// set the destruction UI active or inactive
    /// </summary>
    private void SetDestructionUI(bool status)
    {
        //Error handeling
        if (buildingManager == null)
        {
            buildingManager = FindObjectOfType<BuildingManager>();
        }
        
        if (buildingManager != null && destructionUI != null) //if it has destruction UI
        {            
            if (status) //if setting UI active
            {
                if (buildingManager.destroyWallMode) //if in the correct mode to destroy walls.
                {
                    //add the destruction buttons of the newly accessed rooms.
                    if (!buildingManager.destructionButtons.Contains(destructionUI.transform.GetChild(0).gameObject))
                    {
                        buildingManager.destructionButtons.Add(destructionUI.transform.GetChild(0).gameObject);
                    }
                    destructionUI.SetActive(true);
                }
            }
            else
            {
                destructionUI.SetActive(false);
            }
        }    
    }

    private void OnEnable()
    {       
        SetDestructionUI(true); //when grid is turned on, set destruction UI, provided is in the correct mode.
    }

    private void OnDisable()
    {
        SetDestructionUI(false); //when turned off, turn off UI
    }

    private void OnDestroy()
    {   
        //if it has destruction UI, get rid of it.
        if (destructionUI != null)
        {
            Destroy(destructionUI);
        }

       
        //if in the main scene
        if (buildingManager != null && SceneManager.GetSceneByName("James B New").isLoaded)
        {
            buildingManager.destoryedGrids.Add(gameObject.name); //add the to known destroyed grids, (Game save system uses it to know what walls have been knocked down)
            buildingManager.unlockedGrids.Remove(gameObject); //no longer exists

            if (displayWall != null) //Destroy the display wall
            {
                Destroy(displayWall);
            }

            foreach (var grid in gridsToUnlock) //unlock the grids of the newly accessed area.
            {
                if (grid != null)
                {
                    if (!buildingManager.unlockedGrids.Contains(grid))
                    {
                        buildingManager.unlockedGrids.Add(grid);
                        if (grid.GetComponent<GenPointProperties>().gridType == GridType.FLOOR) //if one of the grids is a floor.
                        {
                            buildingManager.unlockedRooms++; //increase the known amount of rooms unlocked.
                            buildingManager.buildingCapacity += buildingManager.capacityInreaseAmount; //increase the arcades building capacity.

                        }
                    }
                }
            }

            if (roomLights != null)
            {
                //turn on the room lights if its not active already
                foreach (var light in roomLights)
                {
                    if (light != null)
                    {
                        if (!light.activeInHierarchy)
                        {
                            light.SetActive(true);
                        }
                    }
                }                  
            }

            buildingManager.OnDestroyWall();

            if (buildingManager.destroyWallMode) //if in destroy wall mode, only the wall grids are required to be on, so enable the newly unlocked ones.
            {               
                buildingManager.EnableGrids(true, GridType.WALL);
            }

        }     
    }
}



