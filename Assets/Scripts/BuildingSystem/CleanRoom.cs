﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct RoomData
{
    public bool cleaned;
    public List<MaterialData> matData;
    //public List<Vector4> matColor;
}

[System.Serializable]
public struct MaterialData
{
    public string objName;
    public string matName;
    public Vector4 matColor;
}


public class CleanRoom : MonoBehaviour
{
    public List<GameObject> objectsToClean = new List<GameObject>();
    public List<Material> dirtyMaterials = new List<Material>();
    public BuildingManager buildingManager;
    public GenPointProperties floorGrid;
    [HideInInspector]
    public RoomData rData;
    [HideInInspector]
    public bool cleaned = false;

    public void Clean()
    {
        buildingManager.audioSource.clip = buildingManager.cleanClip;
        buildingManager.audioSource.Play();
        foreach (var obj in objectsToClean)
        {
            if (obj != null)
            {
                if (obj.tag == "Dirty")
                {
                    obj.tag = "Untagged";
                }

                Renderer ren = obj.GetComponent<Renderer>();

                foreach (var dirtyMat in dirtyMaterials)
                {
                    for (int i = 0; i < ren.materials.Length; i++)
                    {
                        if (ren.materials[i].name.Contains(dirtyMat.name))
                        {
                            ren.materials[i].color = new Color(0, 0, 0, 0);
                            cleaned = true;
                            BuildingManager.firstRoomCleaned = true;
                        }
                    }
                }
            }
            
        }
        gameObject.SetActive(false);
    }

    public void SaveData(ref GameData data)
    {
        rData.cleaned = cleaned;
        rData.matData = new List<MaterialData>();

        foreach (var obj in objectsToClean)
        {
            if (obj != null)
            {
                Renderer ren = obj.GetComponent<Renderer>();

                foreach (var mat in ren.materials)
                {
                    MaterialData mData = new MaterialData();
                    mData.objName = obj.name;
                    mData.matName = mat.name;
                    mData.matColor = mat.color;
                    rData.matData.Add(mData);
                }
            }
        }
        data.roomData.Add(rData);
    }

    public void LoadData(RoomData data)
    {
        cleaned = data.cleaned;
        for (int i = 0; i < objectsToClean.Count;i++)
        {
            if (cleaned)
            {
                if (objectsToClean[i].tag == "Dirty")
                {
                    objectsToClean[i].tag = "Untagged";
                }
            }
            Renderer ren = objectsToClean[i].GetComponent<Renderer>();
            if (objectsToClean[i] != null)
            {
                for (int j = 0; j < ren.materials.Length; j++)
                {
                    for (int m = 0; m < data.matData.Count; m++)
                    {
                        if (ren.materials[j].name == data.matData[m].matName &&
                            objectsToClean[i].name == data.matData[m].objName)
                        {
                            ren.materials[j].color = data.matData[m].matColor;
                        }
                    }
                }
            }
        }
    }
}
