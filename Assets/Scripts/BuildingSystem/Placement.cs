﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Rewired;
using Pixelplacement;
using Rewired.ControllerExtensions;



public class Placement : MonoBehaviour
{
    [HideInInspector]
    public Player input; //Rewired
    public PlayerController_Default player; //player reference
    public Vector2 cursorPosition; //the cursors position in screen space (is set by the hardware cursor if no controller is connected..
    //and is set by rewired's virtual cursor if a controller is connected).
    private bool rotated = false; //if an object is being rotated
    public BuildingManager buildingManager;
    public List<GameObject> closetRendererCells = new List<GameObject>(); //the closet cells to enable their renderers.
    public float radius = 0; //how close does the cell have to be to the cursor to be rendered
    public Material mat; //the grid material
    public GameObject placementParticle; //the particle to play when placing an object.

    public int getClosetCellDelay = 0; //delay the fetching of the closet rendercells to avoid doing it every frame
    private int currentDelay;

    private float delayTimer = 0.5f; //placement delay timer
    private float currentDelayTimer = 0;
    private string prefabName; //the name of the prefab of the placed item. Loads a prefab into the cursor to use as a preview of the object.
    private NotificationSystem notifications;
    private Tutorial tutorial;
    private MachineManager machineManager;
    // Start is called before the first frame update
    void Start()
    {
        //Fetch References
        input = ReInput.players.GetPlayer(0);
        gameObject.SetActive(false);
        tutorial = FindObjectOfType<Tutorial>();
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
        machineManager = GameObject.Find("Managers").GetComponent<MachineManager>();
    }

    void SetClosetCells(float radius)
    {
        //disable any close renderers
        if (closetRendererCells.Count > 0)
        {
            foreach (var col in closetRendererCells)
            {
                col.SetActive(false);
            }
        }
        closetRendererCells.Clear(); //clear reference list

        //get closets colliders
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);

        //check if the hit is cell, if so get renderer and add it to the list.
        foreach (var hit in hitColliders)
        {
            if (hit.gameObject.layer == 8)
            {
                if (hit.transform.childCount > 0)
                {
                    closetRendererCells.Add(hit.transform.GetChild(0).gameObject);
                    if (hit.transform.childCount > 1)
                    {
                        closetRendererCells.Add(hit.transform.GetChild(1).gameObject);
                    }

                }
            }
        }
        //set renderers active.
        foreach (var col in closetRendererCells)
        {
            col.SetActive(true);
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (buildingManager.selectedItem != null) //if an item is selected for placing.
        {
            if (currentDelayTimer < delayTimer) //begin placement delay timer
            {
                currentDelayTimer += Time.unscaledDeltaTime;
            }
            if (buildingManager.selectedItem.placeType == CellType.FLOOR) //if the item is a floor item
            {
                //if rotate negavtive button is pressed and its not rotating, Rotate Right
                if (input.GetNegativeButton("RotateObj") && !rotated)
                {
                    Tween.Rotate(transform, new Vector3(0, 90, 0), Space.World, 0.2f, 0, null, Tween.LoopType.None, OnRotationStart, OnRotateComplete);

                    rotated = true;
                }
                //same as above but rotate left
                if (input.GetButton("RotateObj") && !rotated)
                {
                    Tween.Rotate(transform, new Vector3(0, -90, 0), Space.World, 0.2f, 0, null, Tween.LoopType.None, OnRotationStart, OnRotateComplete);

                    rotated = true;
                }
            }

            //set the material propertie
            mat.SetVector("_Centre", transform.position);

            //delay timer has passed threshold
            if (currentDelay >= getClosetCellDelay)
            {
                //set renderer cells
                SetClosetCells(radius);
                currentDelay = 0;
            }
            else
            {
                currentDelay++;
            }
        }

        if (input.GetButtonDown("Place")) //if the place button is pressed.
        {
            if (buildingManager.purchaseMode) //and in purchase mode
            {
                if (buildingManager.conditions.canPlace) //if placement is allowed
                {
                    Place(); //place object
                }
                else
                {
                    if (buildingManager.economy.money < buildingManager.selectedItem.cost) //display error message if the user cant afford item
                    {
                        notifications.Add(new Notification("Not Enough Funds", "You cannot afford this item."));
                    }
                    else //display if there is no room to place item
                    {
                        notifications.Add(new Notification("Cannot Place Here", "Insufficient space."));
                    }
                }
            }
            if (buildingManager.editMode) //if in edit mode
            {
                if (buildingManager.selectedItem == null) // no item is selected
                {
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(cursorPosition); //draw ray from cursor screen point
                    
                    if (Physics.Raycast(ray, out hit, 1000))
                    {
                        buildingManager.selectedItem = hit.transform.GetComponent<Item>(); //if the raycast hit an item
                        if (buildingManager.selectedItem != null)
                        {
                            if (buildingManager.selectedItem.itemType == ItemType.VENDOR || buildingManager.selectedItem.itemType == ItemType.ARCADE) //if either were a vendor or arcade
                            {
                                //if there is no customer using the machine
                                Machines machine = (Machines)buildingManager.selectedItem;
                                if (machine.customer == null)
                                {
                                    //set object as a child of the grid nav cursor and enable grids based of the placement type (Floor or wall)
                                    buildingManager.selectedItem.transform.parent = transform;
                                    buildingManager.placementLight.enabled = true;
                                    buildingManager.EnableGrids(true,GetGridType());

                                    buildingManager.selectedItem.transform.position = transform.position;
                                    buildingManager.selectedItem.transform.rotation = transform.rotation;
                                    buildingManager.IgnoreRayCasts(true);
                                }
                                else //disable error message if a customer is using the machine
                                {
                                    notifications.Add(new Notification("Cannot Move", "Machine is in use!! Wait until it's free before trying to move it."));
                                    buildingManager.selectedItem = null;
                                }
                            }
                            else //if the item is a decoration, just begin moving it.
                            {
                                buildingManager.selectedItem.transform.position = transform.position;
                                buildingManager.selectedItem.transform.rotation = transform.rotation;
                                buildingManager.EnableGrids(true, GetGridType());
                                buildingManager.IgnoreRayCasts(true);
                            }

                        }
                    }
                }
                else
                {
                    if (buildingManager.conditions.canPlace) //if an item is selected and conditions allow placement
                    {
                        //detach from cursor and disable grids.
                        buildingManager.selectedItem.transform.parent = null;
                        buildingManager.IgnoreRayCasts(false);
                        buildingManager.placementLight.enabled = false;
                        buildingManager.EnableGrids(false, GetGridType());
                        buildingManager.selectedItem = null;
                        buildingManager.IgnoreRayCasts(false);
                    }   
                }

            }        
        }
        //if the sell button is pressed
        if (input.GetButtonDown("SellItem"))
        {
            if (buildingManager.editMode) //is in edit mode
            {
                if (buildingManager.selectedItem != null) //an item was selected
                {
                    //destroy selected item. (On the item script the item will deposit 70% of the purchase cost to the users wallet)
                    buildingManager.IgnoreRayCasts(false);
                    buildingManager.placementLight.enabled = false;
                    Destroy(buildingManager.selectedItem.gameObject);
                    buildingManager.audioSource.clip = buildingManager.sellClip;
                    buildingManager.audioSource.Play();
                    buildingManager.placementLight.enabled = false;
                    
                    foreach (var cell in closetRendererCells)
                    {
                        cell.SetActive(false);                       
                    }
                    closetRendererCells.Clear();
                    buildingManager.selectedItem = null;
                }
            }
        }
    }

    //play sound at the beginning of the tween.
    private void OnRotationStart()
    {
        buildingManager.audioSource.clip = buildingManager.rotateClip;
        buildingManager.audioSource.Play();
    }

    //allow rotation at the end of the tween.
    private void OnRotateComplete()
    {
        rotated = false;
    }

    //get the place type of the currently selected item.
    public GridType GetGridType()
    {
        if (buildingManager.selectedItem.placeType == CellType.WALL)
        {
            return GridType.WALL;
        }
        if (buildingManager.selectedItem.placeType == CellType.FLOOR)
        {
            return GridType.FLOOR;
        }
        return 0;
    }

    public void SetSelectedObject(Item select)
    {
        prefabName = select.name; //get the prefabs name
        buildingManager.selectedItem = Instantiate(select, transform.position, transform.rotation, transform); //create preview
        if (select.placeType != CellType.WALL) //not a wall item
        {
            if (select.itemType == ItemType.ARCADE) //if selected item is an arcade
            {
                if (select.GetComponent<Arcade>().cabinetType == ArcadeType.CLASSIC) //if it is a classic type
                {
                    //create direction arrow in the correct orientation and position relative to the cabinet.
                    buildingManager.directionArrow = Instantiate(buildingManager.directionArrowPrefab, buildingManager.selectedItem.transform.position - transform.forward,
                        buildingManager.selectedItem.transform.GetChild(0).rotation, buildingManager.selectedItem.transform.GetChild(0));
                    buildingManager.directionArrow.transform.localRotation = Quaternion.Euler(90, 0, 90);
                }

            }
            if (buildingManager.directionArrow == null) //for any other item create direction arrow in the correct orientation and position relative to the cabinet.
            {
                 buildingManager.directionArrow = Instantiate(buildingManager.directionArrowPrefab, buildingManager.selectedItem.transform.position
                 - transform.right, Quaternion.Euler(0,0,0), buildingManager.cursor.transform.GetChild(0));
                    buildingManager.directionArrow.transform.localRotation = Quaternion.Euler(0, 180, 0);
            }


        }
        //dont allow preview items to be saved.
        buildingManager.selectedItem.ignoreSerialization = true;
        buildingManager.selectedItem.gameObject.layer = 2; //set to ignore raycast to allow the mouse cursor to detect which cell it is currently over.

        //turn all colliders into triggers to allow clipping
        BoxCollider[] itemColliders = buildingManager.selectedItem.GetComponents<BoxCollider>(); 
        foreach (var col in itemColliders)
        {
            col.isTrigger = true;
        }

        //if a controller is plugged in, set virtual cursor the center of the screen.
        if (input.controllers.joystickCount > 0)
        {
            cursorPosition = new Vector2(Screen.width / 2, Screen.height / 2);
        }

        if (buildingManager.grid.currentCell != null) //reposition to the current cell
        {
            buildingManager.grid.Reposition(buildingManager.grid.currentCell, Vector3.zero, buildingManager.selectedItem.placeType);
        }

        //have all items ignore raycasts
        buildingManager.IgnoreRayCasts(true);

        buildingManager.selectedItem.gameObject.SetActive(true);
        buildingManager.conditions.canPlace = true; //allow placement
    }

    void Place()
    {
        if (buildingManager.selectedItem != null && currentDelayTimer >= delayTimer) //if an item is selected and the place delay period is over
        {
            //if user hasnt hit item cap yet
            if(MachineManager.GetNumberOfMachines(buildingManager.selectedItem.iname) < 3) {
                GameObject prefab = Resources.Load<GameObject>("PlaceableObjects/" + prefabName); //fetch the prefab

                //play placement sound
                buildingManager.audioSource.clip = buildingManager.placeClip;
                buildingManager.audioSource.Play();

                //instantiate place object at desired position and rotation
                GameObject placedObj = Instantiate(prefab, transform.position, transform.rotation);
                Item placedItem = placedObj.GetComponent<Item>();

                //if the item is a vendor
                if (buildingManager.selectedItem.itemType == ItemType.VENDOR)
                {
                    Vendor vendor = placedObj.GetComponent<Vendor>();
                    string tutorialPrompt = "";

                    //get the camera location for the tutorial
                    Transform cameraLocation = placedObj.transform.Find("Tutorial Camera Location");
                    //if its a food vendor, add events to the tutorial section for food vending machines
                    if (vendor.vendorType == VendorType.FOOD)
                    {
                        tutorial.PromptTutorial("Food Vendor", cameraLocation, 3);
                        tutorialPrompt = "Food Vendor";
                        buildingManager.progression.UnlockItem("Soda Machine Round", true);

                    }
                    //if its a drink vendor, add events to the tutorial section for drink vending machines
                    if (vendor.vendorType == VendorType.DRINK)
                    {
                        tutorial.PromptTutorial("Drink Vendor", cameraLocation, 3);
                        tutorialPrompt = "Drink Vendor";
                        buildingManager.progression.UnlockItem("Arcade Cabinet - Phong", true);
                        buildingManager.progression.UnlockItem("Arcade Cabinet - Invaders", true);
                    }
                    ////a camera location wasd
                    //if(cameraLocation != null)
                    //    cameraLocation.transform.parent = null;

                    //if the desired tutorial isnt completed, disable buttons and sliders to prevent the user from interacting with while tutorial is active.
                    if (!tutorial.SectionCompleted(tutorialPrompt))
                    {
                        tutorial.buttonsToDisable.Add(vendor.upgradeButton.GetComponent<Button>());
                        tutorial.buttonsToDisable.Add(vendor.menu_vending.initialSelected.GetComponent<Button>());
                        tutorial.slidersToDisable.Add(vendor.costSlider.GetComponent<Slider>());
                        tutorial.EnableButtonsAndSliders(false);
                    }


                    //add pop up and down events for the UI
                    tutorial.AddEvent(tutorialPrompt, 1, delegate { vendor.menu_vending.transform.parent.gameObject.SetActive(true); });
                    tutorial.AddEvent(tutorialPrompt, 1, delegate { vendor.ShowDiegeticUI(true); });
                    tutorial.AddEvent(tutorialPrompt, 2, delegate { vendor.ShowDiegeticUI(false); });
                    tutorial.AddEvent(tutorialPrompt, 2, delegate { vendor.menu.PopUp(true); });
                    tutorial.AddDissmissEvent(tutorialPrompt, delegate { vendor.menu.PopDown(true); Destroy(cameraLocation.gameObject); });
                    tutorial.AddDissmissEvent(tutorialPrompt, delegate { vendor.menu_vending.transform.parent.gameObject.SetActive(false); });

                }
                else if (buildingManager.selectedItem.itemType == ItemType.DECORATION) //if the item is decorational
                {     
                    if (placedItem.placeType == CellType.WALL) //and is a wall.
                    {
                        if (buildingManager.grid.currentCell.GetComponent<CellProperties>().genPointProps.displayWall != null) //attach to cell (So when a wall is detroyed, the wall items are aswell)
                        {
                            placedObj.transform.parent = buildingManager.grid.currentCell.GetComponent<CellProperties>().genPointProps.displayWall.transform;
                        }
                    }
                    if (buildingManager.selectedItem.iname == "Juke Box") //if the item is a jukebox, add tutorial events
                    {
                        Transform cameraLocation = placedObj.transform.Find("Tutorial Camera Location");
                        tutorial.PromptTutorial("JukeBox", cameraLocation, 1);
                        tutorial.AddDissmissEvent("JukeBox", delegate { Destroy(cameraLocation.gameObject); });
                        cameraLocation.transform.parent = null;
                    }
                }

                //play particles for 2 seconds
                Destroy(Instantiate(placementParticle, transform.position, transform.rotation), 2);

                placedObj.GetComponent<Item>().placed = true;

                //Update item caps
                if(prefab.GetComponent<Arcade>() != null) {
                    MachineManager.arcades.Add(prefab.GetComponent<Arcade>());
                }
                if(prefab.GetComponent<Vendor>() != null) {
                    MachineManager.vendors.Add(prefab.GetComponent<Vendor>());
                }
                if(prefab.GetComponent<Item>().itemType == ItemType.DECORATION) {
                    MachineManager.decorations.Add(prefab.GetComponent<Item>());
                }

                //update shop
                machineManager.content.Populate(machineManager.content.currentDisplayed);

                //deduct item cost from players wallet.
                buildingManager.economy.Deduct(buildingManager.selectedItem.cost);
            } else//display error message if item cap is hit.
                notifications.Add(new Notification("Arcade Management", "We have the maximum amount of " + buildingManager.selectedItem.iname + " machines!")); 
        }
    }
}
