﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Rewired;
using TMPro;
using Pixelplacement;

public class BuildingManager : MonoBehaviour
{
    public static bool firstRoomCleaned = false;
    [Header("----Core System----")]
    [Tooltip("The script that handles positioning and placement of objects")]
    public Placement placement;
    [Tooltip("The script that handles if the conditions are met for an object to be placed")]
    public PlacementConditions conditions;
    [Tooltip("Script that handles current position in the grid")]
    public GridNav grid;
    [HideInInspector]
    public List<Arcade> placedArcades = new List<Arcade>(); //references to all the currently placed arcades.
    [HideInInspector]
    public Item selectedItem; //the currently selected item
    public GameObject cursor; //the cursor in scene that contains the selected item to place
    [HideInInspector]
    public List<GameObject> generatedGrids = new List<GameObject>(); //all the generated grids
    [HideInInspector]
    public List<GameObject> unlockedGrids = new List<GameObject>(); //the grids that have been unlocked (unlocked rooms)
    [HideInInspector]
    public List<GameObject> destructionButtons = new List<GameObject>(); //all the destruction buttons that appear on breakable walls
    public int unlockedRooms = 1; //the amount of rooms that have been unlocked.
    [HideInInspector]
    public List<string> destoryedGrids = new List<string>(); //what grids have been destroyed.
    public GameObject highlightedWall; //the highlighted wall to be destroyed
    public Economy economy; //economy
    private Office office; //office computer
    private DayNightCycle cycle;
    [Tooltip("The maximum number of customers that can be inside the building.")]
    public int buildingCapacity = 0;
  //  [HideInInspector]
    public int currentCapacity = 0; //current amount of AI in building
    [Tooltip("How much to increase the amount AI allowed in the building upon gaining access to a room.")]
    public int capacityInreaseAmount = 0;
    [Header("----Player----")]
    public PlayerController_Default player;
    public Image _cursorDisplay; //virtual cursor image (only active when controller in use)
    public EventSystem eventSystem;
    [HideInInspector]
    public Progression progression;
    private int wallCost = 0; //how much a wall costs to destroy (set in event)
    [HideInInspector]
    public bool purchaseMode = false;
    [HideInInspector]
    public bool editMode = false;
    [HideInInspector]
    public bool destroyWallMode = false;
    [HideInInspector]
    public bool quitting = false;
    [HideInInspector]
    public bool cleaningMode = false;
    public List<GameObject> cleaningButtons = new List<GameObject>(); //the cleaning buttons that appear above every unclean room thats unlocked (in cleaning mode).
    [HideInInspector]
    public MachineManager machineManager;

    public Tutorial tutorial;
    [HideInInspector]
    public bool firstCustomer = false; //determines when the first in game customer enters the store.

    public GameObject directionArrowPrefab; //the arrow that indicates the direction of selected item before placing.
    [HideInInspector]
    public GameObject directionArrow; //referenced to the spawned direction arrow.
    public Light placementLight; //the placement light that appears above the cursor
    [Header("----UI----")]
    private bool panelOpen = false; // is a open panel
    public GameObject confirmationPanel; //pops up asking for confirmation before destroying a wall.
    public GameObject errorPanel; //if you dont have enough funds.
    public MenuUI_Office officeUI;
    public GameObject virtualCursor; //the virtual cursor that is only active when controllers in use. (For placing items)
    [HideInInspector]
    public bool canCloseComputer = true;
    private float leaveDelayTimer = 0;
    [Header("----Audio----")]
    [HideInInspector]
    public AudioSource audioSource;
    public AudioClip cleanClip;
    public AudioClip destroyWallClip;
    public AudioClip placeClip;
    public AudioClip rotateClip;
    public AudioClip sellClip;



    private void Start()
    {
        //Get references
        office = FindObjectOfType<Office>();
        machineManager = FindObjectOfType<MachineManager>();
        progression = FindObjectOfType<Progression>();
        cycle = FindObjectOfType<DayNightCycle>();
        audioSource = GetComponent<AudioSource>();
    }


    private void Update()
    {
        if (currentCapacity > 0)
        {
            //if a customer is in the store and its the first ever in the game.
            if (!firstCustomer)
            {
                //get the first customer
                GameObject customer = FindObjectOfType<DayNightCycle>().customers[0].gameObject;
                EmotesHandler customerEmote = FindObjectOfType<DayNightCycle>().customers[0].GetComponentInChildren<EmotesHandler>();
                AISpawning spawner = FindObjectOfType<AISpawning>();

                //create tutorial event
                tutorial.PromptTutorial("Customer");
                spawner.TutorialEvent(false);
                float bubbleScale = customerEmote.bubbleScale;
                tutorial.AddEvent("Customer", 2, delegate { customerEmote.transform.localScale = new Vector3(bubbleScale, bubbleScale, bubbleScale); customerEmote.happyEmote.SetActive(true); });
                tutorial.AddEvent("Customer", 3, delegate { customerEmote.transform.localScale = Vector3.zero; customerEmote.happyEmote.SetActive(false); });
                tutorial.AddEvent("Customer", 3, delegate { customer.GetComponentInChildren<CollideWithCustomer>().menu.transform.parent.gameObject.SetActive(true); });
                tutorial.AddEvent("Customer", 3, delegate { customer.GetComponentInChildren<CollideWithCustomer>().menu.PopUp(true); });
                tutorial.AddEvent("Customer", 4, delegate { customer.GetComponentInChildren<CollideWithCustomer>().menu.PopDown(true); });
                tutorial.AddDissmissEvent("Customer", delegate { spawner.TutorialEvent(true); });
                firstCustomer = true;
            }
        }

        //unlock knock out if 6 rooms are unlocked
        if (unlockedRooms == 6)
        {
            progression.UnlockItem("Cocktail Cabinet - Knock Out", true);
        }
        //unlock lunar lander if 3 rooms are unlocked
        if (unlockedRooms == 3)
        {
            progression.UnlockItem("Cocktail Cabinet - Lunar Lander", true);
        }

        //if back button quit (ESC, Circle, B)
        if (placement.input.GetButtonDown("Back"))
        {
            //turn cursor off
            virtualCursor.SetActive(false);
            _cursorDisplay.gameObject.SetActive(false);
            machineManager.editMode_ui.PopDown(); //pop edit mode UI down
            //disable the mode that was currently enabled
            if (editMode)
            {
                SetEditMode(false);
            }
            if (destroyWallMode)
            {
                SetDestroyWallMode(false);
            }
            if (cleaningMode)
            {
                SetCleaningMode(false);
            }
            if (purchaseMode)
            {
                SetPurchaseMode(false);
            }
        }

        //not controllers are connected
        if (placement.input.controllers.joystickCount == 0)
        {
            virtualCursor.SetActive(true);
        }
        else
        {
            //controller is connnected, but not in a mode that requires the virtual cursor
            if (!purchaseMode && !editMode && !destroyWallMode && !cleaningMode)
            {
                virtualCursor.SetActive(false);
            }
        }
        //if in one of these modes
        if (purchaseMode || editMode || destroyWallMode || cleaningMode)
        {
            //disable office UI mode and enable the virtual cursor.
            leaveDelayTimer = 0;
            canCloseComputer = false;
            placement.input.controllers.maps.SetMapsEnabled(false, "Default");
            if (office.manageButton.activeInHierarchy)
            {
                office.manageButton.SetActive(false);
            }
            if (!panelOpen)
            {
                EnableCursor();
            }
        }
        else
        {
            //in office mode, re enable the default controls map
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
            if (!office.manageButton.activeInHierarchy)
            {
                office.manageButton.SetActive(true);
            }
        }
        if (leaveDelayTimer < 0.5f) //begin delay for being able to exit.
        {
            leaveDelayTimer += Time.deltaTime;
        }
        else
        {
            canCloseComputer = true;
        }

    }

    void EnableCursor()
    {
        if (placement.input.controllers.joystickCount == 0 || tutorial.sectionActive) //if no controller connected or a tutorial section is active
        {
            //turn cursor off.
            if (_cursorDisplay != null)
            {
                _cursorDisplay.gameObject.SetActive(false);
            }
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            //enable the virtual cursor.
            placement.cursorPosition = _cursorDisplay.transform.position;
            virtualCursor.SetActive(true);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            if (!_cursorDisplay.gameObject.activeInHierarchy)
            {             
                _cursorDisplay.gameObject.SetActive(true);

                eventSystem.SetSelectedGameObject(null);
            }
        }
    }


    public void DestroyWallPrompt(int cost)
    {
        wallCost = cost; //set the wall cost
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(placement.cursorPosition); //draw ray from camera from the cursor position
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.layer == 8 && hit.transform.position.y > 0) //if it hit a grid cell and it isnt the floor (floor y coord is 0)
            {
                CellProperties cell = hit.transform.GetComponent<CellProperties>(); //get cell
                if (cell != null) //cell reference was found
                {
                    panelOpen = true;
                    if (economy.money >= 150) //has enough money to destroy wall
                    {
                        //no highlighted wall was selected before, pop up confirmation panel.
                        if (highlightedWall == null)
                        {
                            Tween.LocalScale(confirmationPanel.transform, new Vector3(1, 1, 1), 0.2f, 0);
                        }
                        //the user had selected a wall before, pop down menu and re open it to display a new wall was selected.
                        if (highlightedWall != null)
                        {
                            Tween.LocalScale(confirmationPanel.transform, new Vector3(0, 0, 0), 0.2f, 0);
                            Tween.LocalScale(confirmationPanel.transform, new Vector3(1, 1, 1), 0.2f, 0.3f);
                        }
                        virtualCursor.gameObject.SetActive(false); //disable the virtual cursor
                        _cursorDisplay.gameObject.SetActive(false);
                        eventSystem.SetSelectedGameObject(confirmationPanel.transform.GetChild(1).gameObject); //set the selected UI element.
                        highlightedWall = hit.transform.parent.gameObject; //set highlight wall reference.

                    }
                    else
                    {
                        //NOT ENOUGH FUNDS, display error panel.
                        virtualCursor.gameObject.SetActive(false);
                        _cursorDisplay.gameObject.SetActive(false);
                        eventSystem.SetSelectedGameObject(confirmationPanel.transform.GetChild(1).gameObject);
                        Tween.LocalScale(errorPanel.transform, new Vector3(1, 1, 1), 0.2f, 0);
                    }
                }
            }
        }
    }

    public void DestroyWall()
    {
        //destroy the wall
        Destroy(highlightedWall);
        Tween.LocalScale(confirmationPanel.transform, new Vector3(0, 0, 0), 0.2f, 0); //pop down panel
        economy.Deduct(wallCost); //take cost from wallet

        panelOpen = false;
        audioSource.clip = destroyWallClip; //play audio effect
        audioSource.Play();
        OnDestroyWall();
    }
     /// <summary>
     /// Enable lights and assigns correct colour to the newly accessed room.
     /// </summary>
    public void OnDestroyWall()
    {
        foreach (var light in cycle.lightsInRooms)
        {
            if (light != null)
            {
                if (cycle.isDay)
                {
                    light.color = cycle.lightsColours_dayTime;
                }
                else
                {
                    light.color = cycle.lightsColours_nightTime;
                }
            }

        }
    }

    //close error panel
    public void DismissError()
    {
        panelOpen = false;
        Tween.LocalScale(errorPanel.transform, new Vector3(0, 0, 0), 0.2f, 0);
    }

    //close confirmation panel.
    public void CancelWallDestruction()
    {
        Tween.LocalScale(confirmationPanel.transform, new Vector3(0, 0, 0), 0.2f, 0);
        highlightedWall = null;
        panelOpen = false;
    }

    //when placing an item, make every placed item ignore raycasts to prevent them from blocking the raycast from the cursor when navigating the grid with a selected object.
    public void IgnoreRayCasts(bool status)
    {
        Item[] placedItems = FindObjectsOfType<Item>();

        if (status)
        {
            foreach (var item in placedItems)
            {
                item.gameObject.layer = 2;
            }
        }
        else
        {
            foreach (var item in placedItems)
            {
                item.gameObject.layer = 0;
            }
        }      
    }  

    /// <summary>
    /// Set active/inactive of all unlocked grids of a specified type (WALL or FLOOR)
    /// </summary>
    public void EnableGrids(bool status, GridType type)
    {
        if (!unlockedGrids.Contains(null))
        {
            foreach (var grid in unlockedGrids)
            {
                if (grid.GetComponent<GenPointProperties>().gridType == type)
                {
                    grid.SetActive(status);
                }
            }
        }
       
    }

    //disable the cells that are displaying the grid
    void DisableCellRenderers()
    {
        foreach (var cell in placement.closetRendererCells)
        {
            cell.SetActive(false);
        }
        placement.closetRendererCells.Clear();
    }
 
    public void SetEditMode(bool status)
    {
        editMode = status;

        //exit the office if entering edit mode amd disable the default control map.
        if (status)
        {
            officeUI.PopDown(true);
            player.viewManager.Switch(View.EDIT);
            placement.input.controllers.maps.SetMapsEnabled(false, "Default");
            machineManager.editMode_ui.PopUp();
        }
        else
        {
            //re enter the office if exiting edit mode.
            player.viewManager.Switch(player.viewManager.previousView);
            office.EnterComputer();
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
        }
        //set edit mode control maps.
        placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
        placement.input.controllers.maps.SetMapsEnabled(status, "Building");

        //set the grid cursor active
        cursor.SetActive(status);
    }

    public void SetDestroyWallMode(bool status)
    {
        //if entering destroywall mode
        if (status)
        {
            //exit the office and disable the default control map.
            officeUI.PopDown(true);
            player.viewManager.Switch(View.EDIT);
            placement.input.controllers.maps.SetMapsEnabled(false, "Default");
            machineManager.editMode_ui.PopUp();
            //tutorial will prompt a destroy wall mode if it is first time entering this mode.
            tutorial.PromptTutorial("Destroy Wall");
        }
        else
        {
            //enter the office and re enable the default maps if exiting destroywall mode.
            player.viewManager.Switch(player.viewManager.previousView);
            office.EnterComputer();
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
        }
        //set edit mode control maps.
        placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
        destroyWallMode = status;
        placement.input.controllers.maps.SetMapsEnabled(status, "Building");
        EnableGrids(status, GridType.WALL); //only enable/disable the wall grids.


    }

    public void SetCleaningMode(bool status)
    {
        //if entering cleaning mode
        if (status)
        {
            //close office, and disable default control map.
            officeUI.PopDown(true);
            player.viewManager.Switch(View.EDIT);
            placement.input.controllers.maps.SetMapsEnabled(false, "Default"); 
            machineManager.editMode_ui.PopUp();
            tutorial.PromptTutorial("Clean Arcade"); //enable cleaning tutorial if its first time.
        }
        else
        {
            //re enter office mode if leaving cleaning mode and enable default control map.
            player.viewManager.Switch(player.viewManager.previousView);
            office.EnterComputer();
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
        }
        //set edit mode control maps.
        placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
        placement.input.controllers.maps.SetMapsEnabled(status, "Building");

        //enable/disable cleaning buttons.
        foreach(var button in cleaningButtons)
        {
            CleanRoom cleaning = button.GetComponentInChildren<CleanRoom>();
            if (cleaning != null)
            {
                //if the room isnt cleaned already
                if (!cleaning.cleaned)
                {
                    //if room is unlocked.
                    if (unlockedGrids.Contains(cleaning.floorGrid.gameObject))
                    {
                        //set the cleaning button.
                        button.SetActive(status);
                    }
                }
            }          
        }
        cleaningMode = status;

    }


    public void SetPurchaseMode(bool status)
    {
        //if not in a tutorial section
        if(!tutorial.sectionActive) {
            cursor.SetActive(status); //set grid nav cursor active

            //set edit mode controls
            placement.input.controllers.maps.SetMapsEnabled(status, "Building");
            placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
            purchaseMode = status;
            placementLight.enabled = status; //set placement light
            DisableCellRenderers();
        
            if (!status && placement != null) //if exiting purchase mode
            {
                //re enter the computer
                player.viewManager.Switch(player.viewManager.previousView);
                office.EnterComputer();
                //allow placed items to ignore raycasts
                IgnoreRayCasts(false);
                //enable default control map
                placement.input.controllers.maps.SetMapsEnabled(true, "Default");
                //set grids based on the selected items place type.
                if (selectedItem != null)
                {
                    if (selectedItem.placeType == CellType.WALL)
                    {
                        EnableGrids(status, GridType.WALL);
                    }
                    else
                    {
                        EnableGrids(status, GridType.FLOOR);
                    }
                    selectedItem = null;
                }
                //get all the display objects attached to the cursor
                List<Transform> displayObjects = new List<Transform>();

                for (int i = 0; i < cursor.transform.childCount; i++)
                {
                    if (cursor.transform.GetChild(i).name != "PlacementLight")
                    {
                        displayObjects.Add(cursor.transform.GetChild(i));
                    }
                }
                //destroy them
                foreach (var obj in displayObjects)
                {
                    Destroy(obj.gameObject);
                }
                directionArrow = null;
            }
            if (status) //enter purchase mode
            {
                player.viewManager.Switch(View.EDIT);
                placement.input.controllers.maps.SetMapsEnabled(false, "Default");
            }
        }
    }
}
