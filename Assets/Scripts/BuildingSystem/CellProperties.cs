﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum CellType
{
    FLOOR = 0,
    WALL = 1
}


public class CellProperties : MonoBehaviour
{
    public CellType cellType;
    public BuildingManager buildingManager;
    private SpriteRenderer m;
    [HideInInspector]
    public GameObject grid;
   // [HideInInspector]
    public GenPointProperties genPointProps;

}
