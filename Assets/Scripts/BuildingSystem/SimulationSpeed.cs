﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Rewired;

public class SimulationSpeed : MonoBehaviour
{
    public Button pauseButton;
    public Button playButton;
    public Button fastButton;
    public Button superFastButton;

    public Sprite playStandard;
    public Sprite pauseStandard;
    public Sprite fastStandard;
    public Sprite superFastStandard;
    public Sprite playHighlighted;
    public Sprite pauseHighlighted;
    public Sprite fastHighlighted;
    public Sprite superFastHighlighted;

    public VHSRenderer vhsEffect;

    Player input;

    public void Start()
    {
        input = ReInput.players.GetPlayer(0);
    }

    public void Update()
    {
        if (input.GetButtonDown("AddSpeed") && PlayerManager.menuUsing == null)
        {
            if (Time.timeScale == 0)
            {
                SetSpeed(1);
                return;
            }
            if (Time.timeScale == 1)
            {
                SetPlayButtonHighlighted(false);
                SetPauseButtonHighlighted(false);
                SetFastButtonHighlighted(true);
                SetSpeed(4);
                return;
            }
            if (Time.timeScale == 4)
            {
                SetFastButtonHighlighted(false);
                SetSuperFastButtonHighlighted(true);
                SetSpeed(8);
                return;
            }
        }
        if (input.GetButtonDown("TakeSpeed") && PlayerManager.menuUsing == null )
        {
            if (Time.timeScale == 8)
            {
                SetFastButtonHighlighted(true);
                SetSuperFastButtonHighlighted(false);
                SetSpeed(4);
                return;
            }
            if (Time.timeScale == 4)
            {
                SetPlayButtonHighlighted(true);
                SetPauseButtonHighlighted(true);
                SetFastButtonHighlighted(false);
                SetSpeed(1);
                return;
            }
            if (Time.timeScale == 1)
            {
                SetSpeed(0);
                return;
            }
        }
    }

    public void SetPlayButtonHighlighted(bool status)
    {
        if (status)
        {
            playButton.image.sprite = playHighlighted;
        }
        else
        {
            playButton.image.sprite = playStandard;
        }
    }

    public void SetPauseButtonHighlighted(bool status)
    {
        if (status)
        {
            pauseButton.image.sprite = pauseHighlighted;
        }
        else
        {
            pauseButton.image.sprite = pauseStandard;
        }
    }
    public void SetFastButtonHighlighted(bool status)
    {
        if (status)
        {
            fastButton.image.sprite = fastHighlighted;
        }
        else
        {
            fastButton.image.sprite = fastStandard;
        }
    }
    public void SetSuperFastButtonHighlighted(bool status)
    {
        if (status)
        {
            superFastButton.image.sprite = superFastHighlighted;
        }
        else
        {
            superFastButton.image.sprite = superFastStandard;
        }
    }
    public void SetSpeed(float speed)
    {
        if (speed == 0)
        {
            pauseButton.gameObject.SetActive(false);
            playButton.gameObject.SetActive(true);
        }
        if (speed == 1)
        {
            pauseButton.gameObject.SetActive(true);
            playButton.gameObject.SetActive(false);
        }

        Time.timeScale = speed;

        if (Time.timeScale > 1)
        {
            vhsEffect.enabled = true;
        }
        else
        {
            vhsEffect.enabled = false;
        }

        EventSystem.current.SetSelectedGameObject(null);
    }
  
}
