﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Rewired;

public class GridNav : MonoBehaviour {
    public BuildingManager buildingManager;
    [HideInInspector]
    public Transform currentCell; //the current cell the preview cursor is in.

    // Update is called once per frame
    void Update() {
        //if no controller is connected, set the cursor position to use the hardwares, else rewireds virtual cursor position will be used
        if (buildingManager.placement.input != null) {
            if (buildingManager.placement.input.controllers.joystickCount == 0) {
                buildingManager.placement.cursorPosition = Input.mousePosition;
            } 
        }

        if (Camera.main != null) { //Error handeling
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(buildingManager.placement.cursorPosition); //fire a raycast from the cursorposition in screenspace
            if (Physics.Raycast(ray, out hit, 10000))
            {
                if (hit.transform.gameObject.tag == "GridSquare" && buildingManager.selectedItem != null) //if it hits a cell
                {
                    if (currentCell != hit.transform) //not already in the cell
                    {
                        CellProperties cell = hit.transform.GetComponent<CellProperties>(); //fetch cell
                        Item item = buildingManager.selectedItem;
                        if (item.placeType == cell.cellType) { //reposition to current cell if cell is on the correct grid type.
                            buildingManager.selectedItem.gameObject.SetActive(true);
                            currentCell = hit.transform;
                            Reposition(hit.transform, hit.normal * -1, item.placeType);
                        }
                        else
                        {
                            buildingManager.selectedItem.gameObject.SetActive(false); //turn preview off if highlighting incorrect cell type (i.e. floor item is selected but a wall cell was
                            //highlighted).
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Correctly reposition the currently selected item.
    /// </summary>
    /// <param name="desiredPosition"></param>
    /// <param name="direction"></param>
    /// <param name="type"></param>
    public void Reposition(Transform desiredPosition, Vector3 direction, CellType type) {
        if (type == CellType.FLOOR) {
            buildingManager.placement.transform.position = desiredPosition.position + new Vector3(0, 1, 0); //move up to be flush with floor
        }
        if (type == CellType.WALL) {
            if (direction != new Vector3(0,-1,0))
            {
                //face in the correct direction.
                buildingManager.placement.transform.position = desiredPosition.position;
                buildingManager.placement.transform.forward = direction;
                //offset to be flush with the wall.
                buildingManager.placement.transform.position += buildingManager.placement.transform.forward * -0.6f;
            }

        }

    }

}
