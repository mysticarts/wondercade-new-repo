﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GridGen : MonoBehaviour
{
    public GameObject floorCell; //the cells that are used for floor generation
    public GameObject wallCell; //the cells that are used for wall generation
    private BuildingManager buildingManager;
    [Tooltip("The name of the generated section")]
    public string SectionName = ""; 
    [HideInInspector]
    public GameObject section; //the empty object that all the grids get childed to.
    public List<GenPointProperties> genPoints = new List<GenPointProperties>(); //the points where the grids get generated

    // Start is called before the first frame update
    void Start()
    {
        buildingManager = FindObjectOfType<BuildingManager>();
        GenerateRoom(); //generate room grid.
    }

    /// <summary>
    /// Generate the room with the provided gen points.
    /// </summary>
    public void GenerateRoom()
    {
        if (section == null) //section doesnt exist yet
        {
            section = new GameObject(SectionName); //create it
        }
        
        

        //generate each section and add it to unlocked grids
        foreach(var point in genPoints)
        {
            GenerateSection(point); //generate the section (i.e. wall or floor)
            if (point.unlockedOnStart)
            {
                buildingManager.unlockedGrids.Add(point.gameObject);
            }
            point.gameObject.SetActive(false);
        }
        buildingManager.generatedGrids.Add(section);
    }

    /// <summary>
    /// Genrate an individual grid of the room
    /// </summary>
    void GenerateSection(GenPointProperties genPoint)
    {
        genPoint.transform.parent = section.transform; //attach to section
        GameObject cell = null;
        //Check the grid type
        if (genPoint.gridType == GridType.FLOOR)
        {
            cell = floorCell;
        }
        if (genPoint.gridType == GridType.WALL)
        {
            cell = wallCell;
        }
        //Generate grid based on set size
        for (int x = 0; x <= genPoint.sizeX; x++)
        {
            for (int y = 0; y <= genPoint.sizeY; y++)
            {
                for (int z = 0; z <= genPoint.sizeZ; z++)
                {
                    Transform point = genPoint.transform; //get genPoint points positions
                    Vector3 offset = new Vector3(point.position.x + x, point.position.y + y, point.position.z + z); //apply offset
                    GameObject gridCube = Instantiate(cell, offset, point.rotation, genPoint.transform); //spawn cell
                    CellProperties cellProps = gridCube.GetComponent<CellProperties>(); //get it properties
                    cellProps.genPointProps = genPoint; //set gen point reference
                    cellProps.grid = genPoint.gameObject; //set what grid its in.
                    gridCube.name = SectionName + " X:" + x + " Y:" + y + " Z:" + z; //set name
                }
            }
        }
    }
}
