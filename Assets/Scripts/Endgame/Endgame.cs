﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Pixelplacement;

public class Endgame : MonoBehaviour
{

    private Economy economy;
    public int daysInDebt = 0;
    public int maxDaysInDebt = 0;
    public bool displayingWarning = false;
    public bool bankRupt = false;
    public Camera endGameCamera;
    private PlayerController_Default player;
    public GameObject panel;
    public List<GameObject> walls = new List<GameObject>();
    public GameObject destructionParticles;
    public List<GameObject> UIHud = new List<GameObject>();
    public RawImage screenFade;
    private AsyncOperation asyncOperation;
    // Start is called before the first frame update
    void Start()
    {
        economy = GetComponent<Economy>();
        player = FindObjectOfType<PlayerController_Default>();
    }

    // Update is called once per frame
    void Update()
    {      
        if (daysInDebt == maxDaysInDebt)
        {
            if (!bankRupt)
            {
                Debug.Log("bankrupt");
                FindObjectOfType<DayNightCycle>().enabled = false;
                foreach (var element in UIHud)
                {
                    element.SetActive(false);
                }
                Time.timeScale = 1;
                bankRupt = true;
                player.gameObject.SetActive(false);
                endGameCamera.gameObject.SetActive(true);
                panel.SetActive(true);
                Tween.LocalScale(panel.transform, new Vector3(2, 3, 1), 0.4f, 0);
            }
        }
    }

    public void EndGame()
    {
        Tween.LocalScale(panel.transform, Vector3.zero, 0.4f, 0,null,Tween.LoopType.None,null,DestroyBuilding);
        asyncOperation = SceneManager.LoadSceneAsync(1);
        asyncOperation.allowSceneActivation = false;
    }


    private void DestroyBuilding()
    {
        FindObjectOfType<Serialization>().enabled = false;

        GameObject[] objMarkers = GameObject.FindGameObjectsWithTag("Objective Marker");
        foreach (var obj in objMarkers)
        {
            Destroy(obj);
        }

        Item[] items = FindObjectsOfType<Item>();

        Destroy(FindObjectOfType<BuildingManager>().gameObject);

        foreach (var item in items)
        {
            Destroy(item.gameObject);
        }

        foreach (var wall in walls)
        {
            Tween.Position(wall.transform, wall.transform.position - Vector3.up * 5, 4, 0);
            if (wall.name == "Outer_Walls")
            {
                Destroy(Instantiate(destructionParticles, wall.transform.position, Quaternion.Euler(-90,0,0)), 10);
            }
        }

        screenFade.gameObject.SetActive(true);

        Tween.Color(screenFade, new Color(0, 0, 0, 1), 4, 7,null,Tween.LoopType.None, null, Exit);
    }

    private void Exit()
    {
        CurrentSave currentSave = FindObjectOfType<CurrentSave>();
        if (currentSave != null)
        {
            Destroy(currentSave.gameObject);
        }


        GameObject sceneManagerObj = new GameObject("Scene Manager");
        SceneManage sceneManage = sceneManagerObj.AddComponent<SceneManage>();
        sceneManage.selectedlevel = "Title_Screen";

        DontDestroyOnLoad(sceneManagerObj);

        Destroy(FindObjectOfType<CurrentSave>());


        asyncOperation.allowSceneActivation = true;
    }


}
