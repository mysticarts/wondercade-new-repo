﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class DoorSensor : MonoBehaviour
{

    public DayNightCycle cycle;
    private AudioSource audioSource;
    public DoorBehaviour[] doors;

    public Transform sensorPoint;

    private int frameCount = 0;
    public int checkFrequency = 0;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnDrawGizmos()
    {
        if (sensorPoint != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(sensorPoint.position, sensorPoint.localScale);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (frameCount >= checkFrequency)
        {
            bool AIDetected = false;

            Collider[] hitColliders = Physics.OverlapBox(sensorPoint.position, sensorPoint.localScale / 2, sensorPoint.rotation);

            List<Transform> AI = new List<Transform>();

            foreach (var hit in hitColliders)
            {
                if (hit.tag == "AI")
                {
                    AIDetected = true;
                    AI.Add(hit.transform);
                }
            }
            foreach (var door in doors)
            {
                if (door != null)
                {
                    if (AIDetected)
                    {
                        if (cycle.isDay || FromInside(AI))
                        {
                            if (audioSource != null)
                            {
                                audioSource.Play();
                            }
                            door.Open();
                        }
                    }
                    else
                    {
                        if (audioSource != null)
                        {
                            audioSource.Play();
                        }
                        door.Close();

                    }
                }
            }
        }

        frameCount++;
    }


    bool FromInside(List<Transform> Entities)
    {
        Vector3 forward = sensorPoint.transform.right;

        foreach (var AI in Entities)
        {
            if (Vector3.Dot(forward, sensorPoint.position - AI.position) < 0)
            {
                continue;
            }
            else
            {
                return false;
            }
        }


        return true;
    }
}

