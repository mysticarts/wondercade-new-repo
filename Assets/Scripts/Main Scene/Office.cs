﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Rewired;

public class Office : MonoBehaviour {

    public ViewManager viewManager;
    public MenuUI menu;
    protected Player input;
    protected DayNightCycle cycle;
    private BuildingManager buildingManager;

    //diegetic UI:
    public GameObject manageButton;
    public Vector3 poppedUpSize = new Vector3(1, 0.03f, 0.5f);
    private bool playerIsInRadius = false;

    private Vector3 initialDiegetic_Scale; //reference to the initial editor scale
    private Vector3 initialDiegetic_Pos; //reference to the initial editor position
    private GameObject firstPersonDiegeticTransform; //use this position for the manageButton in first person view

    //tutorial:
    [HideInInspector]
    public bool firstTime = true; //first use of the item, will prompt the tutorial.
    private Tutorial tutorial;

    public void Start() {
        cycle = GameObject.Find("DayNightCycle").GetComponent<DayNightCycle>();
        input = ReInput.players.GetPlayer(0);
        buildingManager = FindObjectOfType<BuildingManager>();
        tutorial = FindObjectOfType<Tutorial>();

        //ensure menu is popped down:
        PlayerManager.menuUsing = menu; //must be menuUsing to pop it down
        menu.PopDown(true);

        //set references to initial diegetic UI size:
        initialDiegetic_Scale = poppedUpSize;
        initialDiegetic_Pos = manageButton.transform.parent.position;
        firstPersonDiegeticTransform = this.transform.parent.transform.Find("FirstPersonDiegeticTransform").gameObject;
    }

    public void Update() {
        //use menu?
        if(input.GetButtonDown("Manage"))
        {
            EnterComputer();
        }

        //leave menu?
        if (input.GetButtonDown("Back") && buildingManager.canCloseComputer && !tutorial.sectionActive)
        {
            LeaveComputer();
        }
    }

    public void EnterComputer() {
        if(playerIsInRadius) {
            if(viewManager.currentView != View.EDIT) {
                if(PlayerManager.menuUsing == null)
                    menu.PopUp(true);
                Debug.Log("Enter");
            }
        }
    }

    public void LeaveComputer() {

        //check menuUsing is office or EOD, pop it down
        if(PlayerManager.menuUsing != null && (PlayerManager.menuUsing == menu
               || (cycle.currentDayStats != null && PlayerManager.menuUsing == cycle.currentDayStats.menu))) {

            PlayerManager.menuUsing.PopDown(true); //should call this to pop down EOD
            Debug.Log("Leave");
        }

    }

    //show diegetic UI:
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && this.CompareTag("OfficeComputer")) {
            playerIsInRadius = true;

            PopUpButton();
        }
    }

    public void PopUpButton() {

        //scale/position the button depending on first/third view:
        if(viewManager != null && viewManager.currentView == View.FIRST) {
            poppedUpSize = initialDiegetic_Scale/5;
            manageButton.transform.position = firstPersonDiegeticTransform.transform.position; //set the diegetic UI to first person size
        } else {
            poppedUpSize = initialDiegetic_Scale;
            manageButton.transform.parent.position = initialDiegetic_Pos;
        }

        //pop up manage button:
        Tween.LocalScale(manageButton.transform, poppedUpSize, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
     }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player") && this.CompareTag("OfficeComputer")) {
            playerIsInRadius = false;

            PopDownButton();
        }
    }
    public void PopDownButton() {
        //pop down manage button:
        Tween.LocalScale(manageButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
    }

}