﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;

[System.Serializable]
public struct AdvertisingData {
    public List<string> currentHints;
    public List<int> currentEffectiveness;
    public List<AdvertisingType> effectType;
    public List<float> spending;
}


public enum AdvertisingType {
    NEWSPAPERS,
    BILLBOARDS,
    WEBSITES,
    TELEVISION
}

public class AdvertisingManager : MonoBehaviour {

    public DayNightCycle cycle;

    [Range(0, 100)]
    public float chanceOfChangingEffectivenessEachDay = 60;

    //for inspector only: - fill in with hint strings
    public List<string> greatEffectivenessHints_Newspapers;
    public List<string> greatEffectivenessHints_Billboards;
    public List<string> greatEffectivenessHints_Websites;
    public List<string> greatEffectivenessHints_Television;

    public List<string> poorEffectivenessHints_Newspapers;
    public List<string> poorEffectivenessHints_Billboards;
    public List<string> poorEffectivenessHints_Websites;
    public List<string> poorEffectivenessHints_Television;
    //EXAMPLES:
    //  GREAT:
    //eg. There is a front page opportunity in the Herald Sun!
    //or The Super Bowl is on this weekend! There is great opportunity in websites.

    //  POOR:
    //eg. Television commercials aren't going to reach locals until a local blackout is fixed!

    //for code use: - attach the list of strings to the corresponding type for ease of use
    private Dictionary<AdvertisingType, List<string>> greatEffectivenessHints = new Dictionary<AdvertisingType, List<string>>();
    private Dictionary<AdvertisingType, List<string>> poorEffectivenessHints = new Dictionary<AdvertisingType, List<string>>();

    //effectivness values for slider: 0 -> 100
    private Dictionary<AdvertisingType, int> currentEffectiveness = new Dictionary<AdvertisingType, int>();

    public List<string> currentEffectivenessHints = new List<string>();

    public NotificationSystem notifications;
    public Text hintsText;

    public void Start() {
        if(FindObjectOfType<CurrentSave>() == null) {
            CalculateNewEffectiveness(); //initially set effectiveness'
        }
    }

    //assign string lists to corresponding types for ease of use 
    public void CreateDictionary() {
        greatEffectivenessHints.Add(AdvertisingType.NEWSPAPERS, greatEffectivenessHints_Newspapers);
        greatEffectivenessHints.Add(AdvertisingType.BILLBOARDS, greatEffectivenessHints_Billboards);
        greatEffectivenessHints.Add(AdvertisingType.WEBSITES, greatEffectivenessHints_Websites);
        greatEffectivenessHints.Add(AdvertisingType.TELEVISION, greatEffectivenessHints_Television);

        poorEffectivenessHints.Add(AdvertisingType.NEWSPAPERS, poorEffectivenessHints_Newspapers);
        poorEffectivenessHints.Add(AdvertisingType.BILLBOARDS, poorEffectivenessHints_Billboards);
        poorEffectivenessHints.Add(AdvertisingType.WEBSITES, poorEffectivenessHints_Websites);
        poorEffectivenessHints.Add(AdvertisingType.TELEVISION, poorEffectivenessHints_Television);
    }

    //calculate new effectiveness values for each type
    public void CalculateNewEffectiveness() {

        if(greatEffectivenessHints.Count == 0 && poorEffectivenessHints.Count == 0)
            CreateDictionary();

        currentEffectivenessHints.Clear(); //reset hints

        //loop advertising types:
        for(int count = 0; count <= (int)Enum.GetValues(typeof(AdvertisingType)).Cast<AdvertisingType>().Last(); count++) {
            AdvertisingType type = (AdvertisingType)count;
            
            //remove old value
            if(currentEffectiveness.ContainsKey(type)) currentEffectiveness.Remove(type);

            int effectiveness = UnityEngine.Random.Range(0, 100);

            //add effectiveness hint if applicable
            if(effectiveness > 75) {

                //effective hints:
                List<string> hintsToChooseFrom = greatEffectivenessHints[type];
                string hintToUse = hintsToChooseFrom[UnityEngine.Random.Range(0, hintsToChooseFrom.Count)];
                currentEffectivenessHints.Add(hintToUse);

            } else if(effectiveness < 25) {

                //ineffective hints:
                List<string> hintsToChooseFrom = poorEffectivenessHints[type];
                string hintToUse = hintsToChooseFrom[UnityEngine.Random.Range(0, hintsToChooseFrom.Count)];
                currentEffectivenessHints.Add(hintToUse);

            }

            //add new value
            currentEffectiveness.Add((AdvertisingType)count, effectiveness);
        }

        //set the hints text in the office - send new hints as notifications
        if(hintsText != null) {
            string hints = "";
            foreach(string hint in currentEffectivenessHints) {
                hints += " - " + hint + "\n\n";
                notifications.Add(new Notification("Advertising Agency", hint));
            }
            if(hints == "") hints = " - There's nothing to report on today.";
            hintsText.text = hints; //update office hints text
        }

    }

    //references to office menu UI stuff:
    public Text dayNumber;
    public Text totalToSpendTomorrow;
    public Text timeUntilLockedIn;
    public List<Text> moneySpentText;
    public List<Slider> moneySpent;

    public int totalSpentToday = 0;
    //lock in advertising expenses - start new day
    public void StartDay() {

        totalSpentToday = 0;
        for(int count = 0; count < moneySpent.Count; count++)
            totalSpentToday += Mathf.RoundToInt(moneySpent[count].value);

        dayNumber.text = "Select amount to spend on day " + (cycle.daysPlayed + 2);
    }

    //update the office menu texts
    public void UpdateData() {

        float total = 0;
        for(int count = 0; count < moneySpent.Count; count++) {
            total += moneySpent[count].value;
            moneySpentText[count].text = "$" + moneySpent[count].value;
        }

        totalToSpendTomorrow.text = "$" + total;
    }

    [Tooltip("This value represents how much it costs per customer attracted with advertising. Eg 5$ to attract 1 customer. The higher the value, the less customers you'll get for the money spent")]
    public float dollarValuePerCustomer = 5;

    //calculate the multiplier effect of advertising for AI spawning
    public int GetAdvertisingAddition() {
        float total = 0;

        //loop advertising types:
        for(int count = 0; count < moneySpent.Count; count++) {
            float cost = moneySpent[count].value;
            float effectiveness = currentEffectiveness[(AdvertisingType)count] / 100.0f;
            total += (cost*effectiveness);
        }
        total /= dollarValuePerCustomer;

        //returns an integer - the number of AI to be added to spawning
        return Mathf.CeilToInt(total);
    }

    //check all hints for key word
    public bool GetKeyWord(string key) {
        foreach(var hint in currentEffectivenessHints) {
            if(hint.Contains(key)) return true;
        }
        return false;
    }

    //save serialisable data
    public void SaveData(ref GameData data) {
        data.advertisingData.currentHints = currentEffectivenessHints;
        data.advertisingData.effectType = new List<AdvertisingType>();
        data.advertisingData.currentEffectiveness = new List<int>();

        foreach (var effect in currentEffectiveness) {
            data.advertisingData.effectType.Add(effect.Key);
            data.advertisingData.currentEffectiveness.Add(effect.Value);
        }

        data.advertisingData.spending = new List<float>();

        foreach (var category in moneySpent)
            data.advertisingData.spending.Add(category.value);

    }

    //load serialised data
    public void LoadData(AdvertisingData data) {

        currentEffectivenessHints = data.currentHints;
        if(hintsText != null) {
            string hints = "";
            foreach (string hint in currentEffectivenessHints)
                hints += " - " + hint + "\n\n";

            if(hints == "") hints = " - There's nothing to report on today.";
            hintsText.text = hints;
        }

        for(int i = 0; i < data.currentEffectiveness.Count; i++)
            currentEffectiveness.Add(data.effectType[i], data.currentEffectiveness[i]);

        for(int i = 0; i < data.spending.Count; i++)
            moneySpent[i].value = data.spending[i];

    }



}
