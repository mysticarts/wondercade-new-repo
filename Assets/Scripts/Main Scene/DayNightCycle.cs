﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct CycleData
{
    public string weekText;
    public string timeText;
    public float timeOfDay;
    public Vector3 sunPos;
    public Vector3 sunRotation;
    public int dayCount;
    public bool isDay;
    public bool storeOpen;
}

public class DayNightCycle : MonoBehaviour {

    [HideInInspector]
    public CycleData cData = new CycleData();
    public Renderer neonSign;
    public List<Light> streetLights = new List<Light>();
    //reference to AIspawning manager
    public AISpawning AIspawning;

    [HideInInspector]
    public bool storeClosed = false;
    [HideInInspector]
    public bool storeOpen = false;
    private Endgame endGame;
    public List<AIStats> customers = new List<AIStats>();
    //reference to advertising manager
    public AdvertisingManager advertisingManager;
    //reference to expenses manager
    public ExpensesManager expensesManager;
    //reference to analytics ui
    public AnalyticsUI analytics;
    //reference to the current day stats
    public DayStats currentDayStats = null;
    //default day stats prefab that gets instantiated on new day
    public GameObject dayStatsPrefab;

    public Text computerTimeText;
    public Text timeText;
    public Text meridiesText;
    public RawImage cycleImage;
    public List<Texture2D> textures;

    public GameObject pivotPoint;
    public float timePerDay = 10; //in seconds

    public bool isDay = false;

    private float speed = 0;
    [HideInInspector]
    public float t = 0; //lerp value

    public int time; //in minutes //is non-editable

    private NotificationSystem notifications;
    public bool debug = false;
    void Start() {
        speed = 180 / (timePerDay/2); //180 degrees revolution / time
        if(!gameSaveLoaded) SetMidnight();

        if (gameObject.scene.name != "CutSceneUpdated")
        {

            notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
            endGame = advertisingManager.GetComponent<Endgame>();
        }
        SetOpenSign();

    }

    public float minPoint = 0;
    public float maxPoint = 0;
    public Vector3 midnightPosition;
    public Quaternion midnightRotation;

    public void SetMidnight() {
        neonSign.material.SetColor("_EmissionColor", neonSign.material.color * -5);
        this.gameObject.transform.position = midnightPosition;
        this.gameObject.transform.rotation = midnightRotation;
    }

    [Tooltip("The number of customers to come in before sending morale notification")]
    public int howOftenToSendMoraleNotification = 8;

    public BuildingManager buildingManager; 
    public void StartDay() {

        roofEmission.color = lightsColours_dayTime;
        //change lights in room
        foreach(Light light in lightsInRooms) {
            light.color = lightsColours_dayTime;
        }

        isDay = true;
        addedDayForToday = false;
        storeClosed = false;
        storeOpen = true;
        advertisingManager.StartDay();
        SetOpenSign();
        if(dayStatsPrefab != null) {
            GameObject newDayStats = Instantiate(dayStatsPrefab, GameObject.Find("WeekStats").gameObject.transform);
            currentDayStats = newDayStats.GetComponent<DayStats>();
        }

        AIspawning.DeterminePopulationDensityforDay();
        if(AIspawning.populationDensity > buildingManager.buildingCapacity) {
            notifications.Add(new Notification("Building Manager", "There has been an increase in customer traffic, but your building isn't big enough to hold this many customers."));
            notifications.Add(new Notification("Building Manager", "Renovate your rooms to allow more customers to enter. Visit the office to expand to more rooms."));
        }

        SetStreetLights(false);

        notifications.Add(new Notification("Start of Day", "The store is open for business!"));
    }

    public Material roofEmission;
    public List<Light> lightsInRooms;
    public Color lightsColours_dayTime;
    public Color lightsColours_nightTime;

    public void EndDay() {

        roofEmission.color = lightsColours_nightTime;
        //change lights in room
        foreach(Light light in lightsInRooms) {
            light.color = lightsColours_nightTime;
        }

        SetOpenSign();

        foreach (var arcade in buildingManager.placedArcades)
        {
            arcade.queue.Clear();
        }

        if (expensesManager.economy.money < 0)
        {
            endGame.daysInDebt++;
            if (!endGame.displayingWarning)
            {
                notifications.Add(new Notification("In Debt", "Ensure to leave debt within " + endGame.maxDaysInDebt + " days to avoid bankruptcy!"));
                endGame.displayingWarning = true;
            }
        }
        else
        {
            endGame.daysInDebt = 0;
            endGame.displayingWarning = false;
        }

        neonSign.material.SetColor("_EmissionColor", neonSign.material.color * -5);
        if (endGame.daysInDebt < endGame.maxDaysInDebt)
        {
            notifications.Add(new Notification("End of Day", "The store is closed!"));
            expensesManager.AddExpensesForDay(currentDayStats);
            storeClosed = true;
            storeOpen = false;
            isDay = false;
            if (!WeekStats.dayStats.Contains(currentDayStats))
            {
                WeekStats.AddDay(currentDayStats);
            }
            GatherCustomerData();
            currentDayStats.menu.PopUp(true);
            analytics.UpdateData();
            customers.Clear();

            SetStreetLights(true);

            //chance of changing advertising effectiveness
            if (UnityEngine.Random.Range(0, 100) <= advertisingManager.chanceOfChangingEffectivenessEachDay)
            {
                advertisingManager.CalculateNewEffectiveness();
            }

        }


    }

    public Text dayText;
    public Text weekText;
    public Text daysPlayedText;

    public enum WeekDay {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }
    public int daysPlayed = 1;
    public int GetWeekNumber() {
        return (Mathf.CeilToInt(daysPlayed / 7))+1;
    }
    public WeekDay GetWeekDay() {
        return (WeekDay)(daysPlayed % 7);
    }

    public void SetStreetLights(bool status)
    {
        foreach (var light in streetLights)
        {
            light.enabled = status;
        }
    }
    public void GatherCustomerData() {
        foreach(AIStats customer in customers) {
            if (!float.IsNaN(customer.morale))
            {
                currentDayStats.totalCustomerSatisfaction += customer.morale;
            }
        }
        currentDayStats.numberOfCustomers = customers.Count;
    }

    void SetOpenSign()
    {
        if (isDay)
        {
            neonSign.material.SetColor("_EmissionColor", neonSign.material.color * 3);
        }
        else
        {
            neonSign.material.SetColor("_EmissionColor", neonSign.material.color * -3);
        }
    }
    public float GetCurrentAverageMorale() {
        float totalMorale = 0;
        foreach(AIStats customer in customers) {
            totalMorale += customer.morale;
        }
        return totalMorale;
    }

    public float skybox_minExposure = 0.2f;
    public float skybox_maxExposure = 1;

    [Range(0,24)] public int hourOfSunRise = 5;
    [Range(0,24)] public int hourOfSunSet = 17;
    [Range(0,12)] public int sunRiseFallSpeed = 1;

    private bool sentMoraleNotification = false;
    void Update() {

        if (gameObject.scene.name != "CutSceneUpdated")
        {
            //morale notifications
            if (!sentMoraleNotification && customers.Count != 0 && (customers.Count % howOftenToSendMoraleNotification) == 0)
            { //every X customers, send notification
                sentMoraleNotification = true;
                if (GetCurrentAverageMorale() > 66)
                    notifications.Add(new Notification("Customer Morale", "Your customers are happy!"));

                else if (GetCurrentAverageMorale() > 33)
                    notifications.Add(new Notification("Customer Morale", "Your customers are indifferent."));

                else notifications.Add(new Notification("Customer Morale", "Your customers are angry!"));
            }
            else if (customers.Count == 0 || (customers.Count % howOftenToSendMoraleNotification) != 0) sentMoraleNotification = false;

            //move light (sun):
            if (this.gameObject.transform.position.y < 0)
            {
                if (isDay) //just turned to night
                    EndDay();
            }
            else
            {
                if (!isDay) //just turned to day
                    StartDay();
            }
            t += Time.deltaTime; //t is the seconds passed

            transform.RotateAround(pivotPoint.transform.position, Vector3.forward, speed * Time.deltaTime);

            //debugging stuff:
            if (transform.position.y < minPoint)
            {
                if (debug) minPoint = transform.position.y;
            }
            if (transform.position.y < -50.2f)
            {
                //if starting transform changes:
                if (debug) midnightPosition = transform.position;
                if (debug) midnightRotation = transform.rotation;

                //t = 0; //reset day at minimum point
            }

            //update time:
            time = (int)(1440 * (t / timePerDay)); //time is the minutes passed IN GAME
            if(time >= 1440) { t = 0; time = 0; } //restart day

            //send clean notification
            if(time == 630 && !BuildingManager.firstRoomCleaned) notifications.Add(new Notification("Health Inspector", "As soon as I walked in I was disgusted. Please organise someone to clean the main room before I come again."));

            //Debug.Log("Time: " + time + "(t=" + t + ")" + " (passed: " + GetPercentageOfDay(time) + "%)");
            UpdateTimeDisplay();

            //update skybox:
            float exposureIntegral = 0; //from 0-1
            uint hour = (uint)time / 60;
            if (hour >= hourOfSunRise && hour < (hourOfSunRise + sunRiseFallSpeed))
            {
                //getting brighter

                //get percentage from 6 to 12
                int minutesPassedSinceSix = time - (hourOfSunRise * 60); //current time - minutesofsunrise
                exposureIntegral = minutesPassedSinceSix / (sunRiseFallSpeed * 60f);

            }
            else if (hour >= hourOfSunSet && hour < (hourOfSunSet + sunRiseFallSpeed))
            {
                //getting darker

                //get percentage from 6 to 12
                int minutesPassedSinceTwelve = time - (hourOfSunSet * 60); //current time - minutesofsunset
                exposureIntegral = minutesPassedSinceTwelve / (sunRiseFallSpeed * 60f);
                //invert as getting darker
                exposureIntegral = (1 - exposureIntegral);
            }
            else if (hour > hourOfSunRise && hour < hourOfSunSet)
            {
                exposureIntegral = skybox_maxExposure;
            }
            //else is min, exposure integral stays as min value

            float exposure = skybox_minExposure + exposureIntegral;
            RenderSettings.skybox.SetFloat("_Exposure", exposure);

            int timeUntilNewDay = 0;
            if (hour >= 6) timeUntilNewDay = 6 + (24 - (int)hour);
            else timeUntilNewDay = 6 - (int)hour;
            advertisingManager.timeUntilLockedIn.text = "Time until locked in: " + timeUntilNewDay + " hours";

        }

    }

    public float GetPercentageOfDay(int minute) {
        int minutesInDay = 1440;
        return (minute/minutesInDay)*100f;
    }

    public bool addedDayForToday = true;
    public bool gameSaveLoaded = false;
    public void UpdateTimeDisplay() {

        uint hour = (uint)time / 60;

        ////keep hour relative to day, not time
        //int days = Mathf.CeilToInt(hour/24f);
        //hour -= 24*(uint)days;

        uint remainder = (uint)time - (hour * 60);

        if(textures.Count > 0) {
            if(cycleImage != null && hour >= 0) {
                if(hour <= 24)
                    cycleImage.texture = textures[(int)hour];
                else Debug.Log("TIME ERROR!");
            }
        }

        if(meridiesText != null) {
            if (hour >= 12) meridiesText.text = "PM";
            else meridiesText.text = "AM";
        }

        if(hour > 12) hour -= 12;

        string hourString = "" + hour;
        if(hour == 0) hourString = "12";
        string remainderString = "" + remainder;
        if(remainder < 10) remainderString = "0" + remainder;
        if(timeText != null) {
            timeText.text = hourString + ":" + remainderString;
            computerTimeText.text = hourString + ":" + remainderString;
        }

        if((hour == 0 && remainder == 0 && !addedDayForToday) || gameSaveLoaded) {
            if (!gameSaveLoaded)
            {
                daysPlayed++;
            }          
            dayText.text = GetWeekDay().ToString();
            weekText.text = "Week " + GetWeekNumber();
            daysPlayedText.text = "Day " + (daysPlayed + 1);
            addedDayForToday = true;    
            gameSaveLoaded = false;
        }

    }


    public void SaveData(ref GameData data)
    {
        cData.timeOfDay = t;
        cData.sunPos = transform.position;
        cData.sunRotation = transform.rotation.eulerAngles;
        cData.dayCount = daysPlayed;
        cData.isDay = isDay;
        cData.timeText = timeText.text + meridiesText.text;
        cData.weekText = GetWeekNumber().ToString();
        cData.storeOpen = storeOpen;
        data.cycleData = cData;

    }

    public void LoadData(GameData data)
    {
        cData = data.cycleData;
        t = cData.timeOfDay;
        isDay = cData.isDay;
        transform.position = cData.sunPos;
        transform.rotation = Quaternion.Euler(cData.sunRotation);
        daysPlayed = cData.dayCount;
        storeOpen = cData.storeOpen;
        gameSaveLoaded = true;
    }



}
