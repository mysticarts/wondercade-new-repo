﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerController : MonoBehaviour {

    public Player input;

    virtual public void Initialise() { }

    public void Start() {
        input = ReInput.players.GetPlayer(0);
        Initialise(); //call whenever game loads
    }

}
