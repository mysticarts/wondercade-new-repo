﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Rewired;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct PlayerData {
    public Vector3 playerPos;
    public Vector3 playerRot;
}

public class PlayerController_Default : PlayerController {

    //player info
    [HideInInspector]
    public string m_gender; //character gender
    public float speed;
    private float currentSpeed;
    public float rotateSpeed;
    public bool isRotating = false;

    //references
    public MachineManager machineManager;
    public ViewManager viewManager;
    public BuildingManager buildingManager;
    public Animator animations;
    private Tutorial tutorial;
    private NotificationSystem notifications;

    private float horizontalAnim = 0;
    private float verticalAnim = 0;

    public GameObject maleModel;
    public GameObject femaleModel;

    //clamp the up/down first person view
    public float firstPersonVerticalMin = 60;
    public float firstPersonVerticalMax = 300;

    //clamp the max/min zoom for third/edit view
    public float minZoomY = 5;
    public float maxZoomY = 25;

    [HideInInspector]
    public PlayerData pData = new PlayerData();

    override public void Initialise() {
        // Initialise default input controller
        input.controllers.maps.SetMapsEnabled(true, "Default");
        animations.SetFloat("Vertical", 0);
        animations.SetFloat("Horizontal", 0);
    }

    //track the last selected UI object - if the player tries selecting an inactive UI object, cancel it and go back to this
    public GameObject lastSelected;

    private void Start() {
        base.Start();
        tutorial = FindObjectOfType<Tutorial>();
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
    }

    //lock the initial item position when buying item in edit mode
    public Vector3 initialItemPosition = Vector3.zero;

    // Update is called once per frame
    public void Update() {

        //lock selected item when rotating in edit mode:
        if(input.GetButtonDown("Rotate") && !isRotating) {
            isRotating = true;
            if(buildingManager.selectedItem != null) initialItemPosition = buildingManager.selectedItem.transform.position;
        } else if(input.GetButtonUp("Rotate") && isRotating) isRotating = false;

        if(isRotating) {
            if(buildingManager.selectedItem != null) 
                buildingManager.cursor.transform.position = initialItemPosition;
        }

        //fix for not selecting inactive UI:

        //player is in menu
        if(PlayerManager.menuUsing != null) {
            GameObject currentSelected = EventSystem.current.currentSelectedGameObject;
            if(currentSelected != null) {
                if(!currentSelected.activeSelf || currentSelected.GetComponent<RectTransform>().localScale == Vector3.zero || currentSelected.CompareTag("IgnoreUI")){
                    EventSystem.current.SetSelectedGameObject(lastSelected); //will set selection to null if last selection is null
                    //Debug.Log("UNDONE! - is inactive, go back to: " + lastSelected);
                } else if(!GameObject.ReferenceEquals(currentSelected,lastSelected)) {
                    lastSelected = currentSelected;
                    //Debug.Log("UPDATED - new 'last selected': " + lastSelected);
                }
            }
        } else {
            lastSelected = null;
            if (Time.timeScale > 0)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
            //Debug.Log("Closed menu - nothing selected");
        }

        //change views:

        if(Time.timeScale == 1) {

            if(!buildingManager.destroyWallMode && !buildingManager.editMode && !buildingManager.purchaseMode) {
                //change to third/first
                if(input.GetAxis("CameraSwitch") != 0 && viewManager.t < 0.01) {
                    if(viewManager.currentView == View.FIRST) viewManager.Switch(View.THIRD);
                    else if (viewManager.currentView == View.THIRD) viewManager.Switch(View.FIRST);
                }
            }

            //enter/exit edit mode:
            if(buildingManager.purchaseMode) {
                if(input.GetButtonDown("Manage") && viewManager.t < 0.01) {
                    if(viewManager.currentView != View.EDIT) viewManager.Switch(View.EDIT);
                }
            }

        }     
    }

    public void FixedUpdate() {
        // Player movement & rotation
        if(!viewManager.isUpdating && PlayerManager.menuUsing == null) {
            if(Time.timeScale == 1) {
                Zoom();
                Move();
            }
            Rotate(); //can rotate when paused, but can't move/zoom
        }   
    }

    public void SetGender(string gender) {
        if(gender == "Male") {
            maleModel.SetActive(true);
            femaleModel.SetActive(false);
        }
        if(gender == "Female") {
            maleModel.SetActive(false);
            femaleModel.SetActive(true);
        }
        m_gender = gender;
    }

    //track last position that wasn't out of bounds:
    public Vector3 lastBestCameraPosition = Vector3.zero;
    public bool sentBoundaryNotification = false;

    public void Move() {
        Vector3 direction = Vector3.zero;

        horizontalAnim = 0;
        verticalAnim = 0;

        if(!viewManager.isInBoundary) {
            lastBestCameraPosition = Camera.main.transform.position;
            sentBoundaryNotification = false;

            if(input.GetButton("MoveHorizontal")) {
                direction += viewManager.cameraFollow.transform.right;
                currentSpeed += Time.deltaTime * 30;
                horizontalAnim += currentSpeed;
            }
            if(input.GetNegativeButton("MoveHorizontal")) {
                direction -= viewManager.cameraFollow.transform.right;
                currentSpeed += Time.deltaTime * 30;
                horizontalAnim -= currentSpeed;
            }
            if(input.GetButton("MoveVertical")) {
                direction += viewManager.cameraFollow.transform.forward;
                currentSpeed += Time.deltaTime * 30;
                verticalAnim += currentSpeed;
            }
            if(input.GetNegativeButton("MoveVertical")) {
                direction -= viewManager.cameraFollow.transform.forward;
                currentSpeed += Time.deltaTime * 30;
                verticalAnim -= currentSpeed;
                horizontalAnim *= -1;
            }

            if(input.GetAxis("MoveHorizontal") == 0 && input.GetAxis("MoveVertical") == 0)
                currentSpeed = Mathf.Clamp(currentSpeed, -speed, speed);

            //animation:
            horizontalAnim = Mathf.Clamp(horizontalAnim,-1, 1);
            verticalAnim = Mathf.Clamp(verticalAnim , -1, 1);
            animations.SetFloat("Vertical", verticalAnim);
            animations.SetFloat("Horizontal", horizontalAnim);

            MoveDirection(direction);

        } else {
            notifications.Add(new Notification("Boundary", "You can't pass this point!"));
            sentBoundaryNotification = true;
            Camera.main.transform.position = lastBestCameraPosition;
        }
    }

    public void MoveDirection(Vector3 direction) {
        if(viewManager.currentView == View.EDIT) {
            //move only the cameras
            viewManager.cameraFollow.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Camera.main.transform.position + direction, speed * Time.unscaledDeltaTime);
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Camera.main.transform.position + direction, speed * Time.unscaledDeltaTime);
        } else //move the player (which has the cameras as children)
            this.gameObject.GetComponent<Rigidbody>().MovePosition(transform.position + direction * Time.unscaledDeltaTime * speed);
    }

    float controllerSensitivity_firstPerson = 5;
    public void Rotate() {
        if(viewManager.currentView == View.FIRST) {

            //first person rotation:
            float sensitivity = 1; //pc sensitivity
            if(input.controllers.joystickCount > 0) sensitivity = controllerSensitivity_firstPerson;

            float rotX = Camera.main.transform.eulerAngles.x + (input.GetAxis("LookVertical")*sensitivity);
            if(rotX > firstPersonVerticalMax || rotX < firstPersonVerticalMin)
                Camera.main.transform.Rotate(new Vector3(input.GetAxis("LookVertical")*sensitivity, 0, 0));
            transform.Rotate(new Vector3(0, input.GetAxis("LookHorizontal")*sensitivity, 0));

        } else {

            if(input.controllers.joystickCount > 0) {
                // JOYSTICK:

                Vector3 rotateDirection = Vector3.zero;
                if(input.GetNegativeButton("Rotate")) rotateDirection += Vector3.up;
                if(input.GetButton("Rotate")) rotateDirection -= Vector3.up;

                if(viewManager.currentView == View.THIRD)
                    transform.Rotate(new Vector3(0, rotateDirection.y*rotateSpeed*Time.unscaledDeltaTime, 0));

            } else {
                // PC:

                //rotate camera around player:
                if(viewManager.currentView == View.THIRD && isRotating)
                    transform.Rotate(new Vector3(0, input.GetAxis("LookHorizontal")*20*Time.unscaledDeltaTime, 0));
            }

            if(viewManager.currentView == View.EDIT && isRotating) {
                //edit mode - only rotate the camerafollow & camera - not the player
                viewManager.cameraFollow.transform.Rotate(new Vector3(0, input.GetAxis("LookHorizontal")* rotateSpeed * Time.unscaledDeltaTime, 0)); //rotate camera follow
                Camera.main.transform.RotateAround(initialItemPosition, Vector3.up, input.GetAxis("LookHorizontal")* rotateSpeed * Time.unscaledDeltaTime); //edit mode - don't rotate player
            }

        }
    }

    public void Zoom() {
        if(viewManager.currentView == View.THIRD || viewManager.currentView == View.EDIT) {
            //zoom forward axis
            Vector3 direction = Camera.main.transform.forward;

            //zoom in
            if(input.GetButton("Zoom") && Camera.main.transform.position.y > minZoomY)
                Camera.main.transform.position += direction;           

            //zoom out
            if(input.GetNegativeButton("Zoom") && Camera.main.transform.position.y < maxZoomY)
                Camera.main.transform.position -= direction;
        }
    }

    //serialise player data:
    public void SaveData(ref GameData data) {
        pData.playerPos = transform.position;
        pData.playerRot = transform.rotation.eulerAngles;
        data.playerData = pData;
        data.gender = m_gender;
    }

    //load player data:
    public void LoadData(GameData data) {
        pData = data.playerData;
        if(data.gender.Length > 0)
            SetGender(data.gender);
        transform.position = pData.playerPos;
        transform.rotation = Quaternion.Euler(pData.playerRot);
    }

}
