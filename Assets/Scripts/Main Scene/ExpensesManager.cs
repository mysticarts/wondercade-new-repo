﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

[System.Serializable]
public struct ExpensesData {
    public List<int> currentBills;
    public List<ExpenseType> currentExpenseTypes;
    public List<ExpenseType> dueExpenseTypes;
    public List<int> dueDate;
}


public enum ExpenseType {
    Tax,
    Rent,
    Electricity,
    Marketing
}

public class ExpensesManager : MonoBehaviour {

    public Economy economy;
    public BuildingManager buildingManager;
    public AdvertisingManager advertisingManager;

    public List<Text> amounts;
    public List<Button> buttons;
    public List<Text> dueText;

    //references to the total expenses per each expense type
    private Dictionary<ExpenseType, int> expenses = new Dictionary<ExpenseType, int>(); //type, value
    //references to the total days since paying per each expense type
    private Dictionary<ExpenseType, int> daysSincePaying = new Dictionary<ExpenseType, int>(); //type, value

    private NotificationSystem notifications;

    //per day values:
    [Range(0,100)] public float taxPercent = 0.2f;
    public int rentCostPerRoom = 50;
    public int electricityCostBaseRate = 20;
    public int electricityCostPerMachine = 5;

    //debt collection:
    public int daysUntilOverdue = 3;
    [Range(0, 100)] public int overdueInterestPercent = 10; //charge this percent per overdue day (2 days overdue = *2)
    public int daysOverdueUntilDebtCollect = 3;

    private CurrentSave currentSave;

    private void Start() {
        currentSave = FindObjectOfType<CurrentSave>();
        if(currentSave == null) //for testing without having to start a new game
            NewGame();
    }

    public void NewGame() {
        notifications = GetComponent<NotificationSystem>();

        //for each expense:
        for(int count = 0; count <= (int)Enum.GetValues(typeof(ExpenseType)).Cast<ExpenseType>().Last(); count++) {
            //set values to 0

            expenses.Add((ExpenseType)count, 0);
            daysSincePaying.Add((ExpenseType)count, 0);

            if(buttons[count] != null) {
                buttons[count].transform.Find("Text").GetComponent<Text>().text = "Paid!";
                amounts[count].text = "$0";
            }

        }

    }

    public void AddExpensesForDay(DayStats day) {

        //tax:
        int tax = Mathf.CeilToInt(day.GetTotalSales() * taxPercent);
        AddExpense(ExpenseType.Tax, tax, false);
        day.billsExpenses += tax;

        //rent:
        int rent = rentCostPerRoom * buildingManager.unlockedRooms;
        AddExpense(ExpenseType.Rent, rent, false);
        day.billsExpenses += rent; //update analytics

        //bills:
        int numberOfMachines = MachineManager.arcades.Count + MachineManager.vendors.Count;
        int electricity = electricityCostBaseRate + (electricityCostPerMachine * numberOfMachines);
        AddExpense(ExpenseType.Electricity, electricity, false);
        day.billsExpenses += electricity; //update analytics

        //marketing:
        AddExpense(ExpenseType.Marketing, advertisingManager.totalSpentToday, false);
        day.marketingExpenses += advertisingManager.totalSpentToday; //update analytics

        //check if overdue, add 1 day if so:
        for(int count = 0; count < daysSincePaying.Count; count++) {
            ExpenseType expense = (ExpenseType)count;

            //not paid? add 1 to days since paying
            if(expenses[expense] > 0) {
                daysSincePaying[expense] += 1;
            } else daysSincePaying[expense] = 0;

            //if bill is overdue
            if(daysSincePaying[expense] >= daysUntilOverdue) {
                //overdue
                int overdueDays = daysSincePaying[expense] - daysUntilOverdue;

                if(daysSincePaying[expense] > (daysUntilOverdue + daysOverdueUntilDebtCollect - 1))
                    notifications.Add(new Notification("Debt Collector", "Pay fees to the " + expense.ToString() + " Agency NOW to avoid debt collection enforcement."));
                else if(overdueDays == 0) 
                    notifications.Add(new Notification(expense.ToString() + " Agency", "You have fees owed. Please pay promptly to avoid extra costs."));
                else notifications.Add(new Notification(expense.ToString() + " Agency", "You have OUTSTANDING fees owed. A " + (overdueDays * overdueInterestPercent) + "% interest fee was charged."));

                if(overdueDays >= 2) 
                    dueText[(int)expense].GetComponent<Text>().text = "OVERDUE! By " + overdueDays + " days - PAY NOW!";
                else if(overdueDays > 0)
                    dueText[(int)expense].GetComponent<Text>().text = "OVERDUE! By " + overdueDays + " days";
                else dueText[(int)expense].GetComponent<Text>().text = "OVERDUE!";

                if(overdueDays > daysOverdueUntilDebtCollect) {
                    notifications.Add(new Notification("Debt Collector", "DEBT COLLECTED! " + expense.ToString() + " Agency was paid $" + expenses[expense] + "."));
                    economy.Deduct(expenses[expense]);
                    expenses[expense] = 0;
                    buttons[count].transform.Find("Text").GetComponent<Text>().text = "Debt Collected!";
                    dueText[(int)expense].GetComponent<Text>().text = "";
                    daysSincePaying[expense] = 0;
                }
                amounts[count].text = "$" + expenses[expense];

            } else {
                //update when bill is due
                int daysUntilDue = daysUntilOverdue - daysSincePaying[expense];
                dueText[(int)expense].GetComponent<Text>().text = "Due in " + daysUntilDue + " days";
            }
        }

    }

    //determine extra cost (multiplier) for overdue bill
    public float GetOverdueMultiplier(ExpenseType expense) {
        int overdueDays = (daysSincePaying[expense]+1) - daysUntilOverdue;
        if(overdueDays >= 1) {
            return 1 + ((overdueDays * overdueInterestPercent) / 100f); //1-2
        }
        return 1;
    }

    //update expense value and UI (modify +/-)
    public void AddExpense(ExpenseType expense, int amount, bool loadSave) {

        if(!loadSave) {
            expenses[expense] += amount;
            expenses[expense] = Mathf.CeilToInt(expenses[expense] * GetOverdueMultiplier(expense));
        }

        if(expenses[expense] == 0) {
            buttons[(int)expense].transform.Find("Text").GetComponent<Text>().text = "Paid";
            amounts[(int)expense].text = "$" + expenses[expense];
        }
        if(expenses[expense] >= 1) {
            buttons[(int)expense].transform.Find("Text").GetComponent<Text>().text = "Pay Now";
            amounts[(int)expense].text = "$" + expenses[expense];
            int daysUntilDue = daysUntilOverdue - daysSincePaying[expense];
            dueText[(int)expense].GetComponent<Text>().text = "Due in " + daysUntilDue + " days";
        }

    }

    //clear expense - deduct money & update UI
    public void Pay(int expenseID) {
        ExpenseType expense = (ExpenseType) expenseID;

        int cost = expenses[expense];
        if(cost > 0) {
            if(economy.money >= cost) {
                economy.Deduct(cost);
                expenses[expense] = 0;
                buttons[expenseID].transform.Find("Text").GetComponent<Text>().text = "Paid!";
                dueText[(int)expense].GetComponent<Text>().text = "";
                daysSincePaying[expense] = 0;

                //unlock popcorn machine after paying any bills
                buildingManager.progression.UnlockItem("Popcorn Machine", true);
            } else {
                //can still pay for bills if not enough funds to clear
                expenses[expense] -= economy.money;
                economy.Deduct(economy.money);
                buttons[expenseID].transform.Find("Text").GetComponent<Text>().text = "Not enough funds!";
            }
        }

        amounts[expenseID].text = "$" + expenses[expense];
    }

    //serialise expense data
    public void SaveData(ref GameData data) {
        if(expenses.Count > 0) {
            data.expensesData.currentBills = new List<int>();
            data.expensesData.currentExpenseTypes = new List<ExpenseType>();
            data.expensesData.dueDate = new List<int>();
            data.expensesData.dueExpenseTypes = new List<ExpenseType>();

            foreach (var expense in expenses) {
                data.expensesData.currentBills.Add(expense.Value);
                data.expensesData.currentExpenseTypes.Add(expense.Key);
            }
            foreach (var dueDate in daysSincePaying) {
                data.expensesData.dueDate.Add(dueDate.Value);
                data.expensesData.dueExpenseTypes.Add(dueDate.Key);
            }
        }
    }

    //load serialised data
    public void LoadData(ExpensesData data) {
        for(int i = 0; i < data.currentBills.Count; i++)
            expenses.Add(data.currentExpenseTypes[i], data.currentBills[i]);
        for(int i = 0; i < data.dueDate.Count; i++)
            daysSincePaying.Add(data.dueExpenseTypes[i], data.dueDate[i]);

        foreach(var expense in expenses)
            AddExpense(expense.Key, expense.Value, true);
    }

}
