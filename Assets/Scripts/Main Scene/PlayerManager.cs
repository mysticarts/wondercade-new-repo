﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class PlayerManager : MonoBehaviour {

    public static Arcade arcadeUsing = null; //the arcade that the player is currently accessing
    public static MenuUI menuUsing = null; //the menu the player is currently using
    public static AIStats customerInteractingWith = null; //the customer the player is currently interacting with

    public void Start() {
        //update the switch view icons
        ScreenUI screen = GameObject.Find("UI").GetComponent<ScreenUI>();
        if(screen.viewManager == null) screen.viewManager = Camera.main.GetComponent<ViewManager>();
        if(screen.viewManager.previousView != View.EDIT && screen.spaceBar != null && screen.spaceBar_poppedDownPos != null) Tween.AnchoredPosition(screen.spaceBar, screen.spaceBar_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(screen.viewManager.previousView != View.EDIT && screen.rightStick != null && screen.spaceBar_poppedDownPos != null) Tween.AnchoredPosition(screen.rightStick, screen.spaceBar_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }
}
