﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManage : MonoBehaviour
{
    [HideInInspector]
    public string selectedlevel;
    public void Start()
    {
        DontDestroyOnLoad(gameObject);    
    }
    // Start is called before the first frame update
    public void LoadScene(string level)
    {
        selectedlevel = level;
        //yield return new WaitForSeconds(3.0f);
        //SceneManager.LoadSceneAsync(level);            
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
