﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

[System.Serializable]
public struct LoadScreenInfo
{
    public Sprite image;
    [TextArea]
    public string description;
}


public class LoadingScene : MonoBehaviour
{
    [SerializeField]
    private Image _progressBar;
    SceneManage sceneManager;
    AsyncOperation asyncOperation;
    public float delayTimer = 0f;
    private float currentDelayTimer = 0f;
    private bool beginLoad = false;
    public Image background;
    public TextMeshProUGUI tipText;
    public Image loadingImage;
    public SpriteRenderer loadingSprite;
    public List<LoadScreenInfo> loadScreenInfo = new List<LoadScreenInfo>();


    // Start is called before the first frame update
    void Start()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        int ran = Random.Range(0, loadScreenInfo.Count);
        background.sprite = loadScreenInfo[ran].image;
        tipText.text = loadScreenInfo[ran].description;

        //Invoke("ToolTipDelay", 5f);
        //Start async operation 
        // StartCoroutine(LoadAsyncOperation());

        LoadScene();

    }

    public void LoadScene()
    {
        Time.timeScale = 1;
        sceneManager = FindObjectOfType<SceneManage>();
        asyncOperation = SceneManager.LoadSceneAsync(sceneManager.selectedlevel, LoadSceneMode.Single);
#if UNITY_STANDALONE_WIN
        asyncOperation.allowSceneActivation = false;
#endif
        Destroy(sceneManager.gameObject);
        beginLoad = true;


    }




    private void Update()
    {
        //#if UNITY_EDITOR || UNITY_WINDOWS

        if (currentDelayTimer >= 0.2f)
        {
            if (!beginLoad)
            {
                LoadScene();
            }
        }

#if UNITY_STANDALONE_WIN
        currentDelayTimer += Time.unscaledDeltaTime % 60;
        _progressBar.fillAmount = 1 / (delayTimer - currentDelayTimer);
#endif
#if UNITY_PS4
        _progressBar.fillAmount = asyncOperation.progress;
#endif
        loadingImage.sprite = loadingSprite.sprite;

#if UNITY_STANDALONE_WIN
        if (currentDelayTimer >= delayTimer)
        {
            asyncOperation.allowSceneActivation = true;
        }
#endif



    }
}
