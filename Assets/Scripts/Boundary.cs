﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundary : MonoBehaviour {

    private ViewManager manager;
    private void Start() {
        manager = Camera.main.GetComponent<ViewManager>();
    }

    public void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("MainCamera"))
            manager.isInBoundary = true;
    }
    public void OnTriggerExit(Collider other) {
        if(other.gameObject.CompareTag("MainCamera"))
            manager.isInBoundary = false;
    }

}
