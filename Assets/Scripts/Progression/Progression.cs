﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct ProgressionData
{
    public List<string> unlockedItems;
    public int totalMachinesPlayed;
    public int totalMachinesRepaired;
    public int totalWallsDestroyed;
    public bool thongAIBeaten;
    public bool invaders2DBeaten;
    public bool invaders3DBeaten;
}

public class Progression : MonoBehaviour
{
    public PopulateGrid store;
    private NotificationSystem notificationSystem;
    public DayNightCycle cycle;
    public BuildingManager buildingManager;
    public List<Item> itemsToUnlock = new List<Item>();
    private List<Item> unlockedItems = new List<Item>();
    [HideInInspector]
    public bool firstGamePlayed = false; //first mini game
    private CurrentSave currentSave;
    public Button cleanStartRoomButton;
    private Economy economy;
    public GameObject achievementList;
    public List<GameObject> achievementCheckList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        notificationSystem = GetComponent<NotificationSystem>();
        economy = GetComponent<Economy>();
        if (currentSave == null)
        {
            currentSave = FindObjectOfType<CurrentSave>();
        }

        if (cleanStartRoomButton != null)
        {
            cleanStartRoomButton.onClick.AddListener(delegate { UnlockItem("Welcome Sign", true); });
        }

    }

    private void Update()
    {
        //After first day of play
        if (cycle.daysPlayed == 1)
        {
            UnlockItem("Poster", true);
        }
        //After first week of play
        if (cycle.daysPlayed == 8)
        {
            UnlockItem("Juke Box", true);
        }

        if (cycle.daysPlayed == 20)
        {
            UnlockItem("Arcade Cabinet - Generic Block Builder", true);
        }


        if (economy.money >= 1000)
        {
            UnlockItem("Arcade Cabinet - Kombat Mortal", true);
        }

        if (economy.money >= 1500)
        {
            UnlockItem("Cocktail Cabinet - Demolition Man", true);
        }

        if (currentSave != null)
        {
            if (currentSave.totalGamesPlayed == 1)
            {
                UnlockItem("Pot Plant Small", true);
                UnlockItem("Pot Plant Large", true);
            }
            if (currentSave.totalMachinesRepaired == 5)
            {
                UnlockItem("Air Hockey Table", true);
            }
            if (currentSave.beatenPongAI)
            {
                UnlockItem("Dartboard", true);
            }
            if (currentSave.beatenSpaceInvaders2D && currentSave.beatenSpaceInvaders3D)
            {
                UnlockItem("Arcade Cabinet - Bomb Squad", true);
            }
        }
     

        if (buildingManager.unlockedRooms == 6)
        {
            UnlockItem("Cocktail Cabinet - Flogger", true);
        }
    }


    public void SaveData(ref GameData data)
    {
        if (currentSave == null)
        {
            currentSave = FindObjectOfType<CurrentSave>();
        }
        data.progressionData.totalMachinesPlayed = currentSave.totalGamesPlayed;
        data.progressionData.totalMachinesRepaired = currentSave.totalMachinesRepaired;
        data.progressionData.thongAIBeaten = currentSave.beatenPongAI;
        data.progressionData.invaders2DBeaten = currentSave.beatenSpaceInvaders2D;
        data.progressionData.invaders3DBeaten = currentSave.beatenSpaceInvaders3D;

        //  data.progressionData.totalMachinesPlayed = currentSave.totalGamesPlayed;
        //  data.progressionData.totalMachinesRepaired = currentSave.totalMachinesRepaired;

        data.progressionData.unlockedItems = new List<string>();

        foreach (var item in unlockedItems)
        {
            data.progressionData.unlockedItems.Add(item.iname);
        }
    }

    public void LoadData(ProgressionData pData)
    {

        if (currentSave == null)
        {
            currentSave = FindObjectOfType<CurrentSave>();
        }
        //most likely to be true if loading in from main menu.
        currentSave.totalGamesPlayed = pData.totalMachinesPlayed;

        currentSave.totalMachinesRepaired = pData.totalMachinesRepaired;


        if (currentSave.loadFromMenu)
        {
            currentSave.beatenPongAI = pData.thongAIBeaten;
            currentSave.beatenSpaceInvaders2D = pData.invaders2DBeaten;
            currentSave.beatenSpaceInvaders3D = pData.invaders3DBeaten;
        }


      //  currentSave.totalGamesPlayed = pData.totalMachinesPlayed;
       // currentSave.totalMachinesRepaired = pData.totalMachinesRepaired;


        for (int i = 0; i < pData.unlockedItems.Count; i++)
        {
            UnlockItem(pData.unlockedItems[i], false);
        }
    }



    /// <summary>
    /// Type the item name (case sensitive).
    /// </summary>
    public void UnlockItem(string itemName, bool displayNotification)
    {

        Item item = FindLockedItem(itemName);


        if (item != null)
        {
            if (displayNotification)
            {
                notificationSystem.Add(new Notification("New Item Unlocked: ", item.iname + " is now available for purchase in the shop."));
            }
            store.items.Add(item);
            store.Populate(ButtonType.ALL);


            unlockedItems.Add(item);

            for (int i = 0; i < achievementList.transform.childCount; i++)
            {
                if (achievementList.transform.GetChild(i).name.Contains(item.iname))
                {
                    achievementCheckList[i].GetComponentInChildren<Image>().color = Color.green;
                }
            }
        }
    }

    public Item FindLockedItem(string name)
    {
        Item item = null;

        foreach (var i in itemsToUnlock)
        {
            if (i.iname == name)
            {
                item = i;
                break;
            }
        }


        if (item != null)
        {
            itemsToUnlock.Remove(item);
        }


        return item;
    }




}
