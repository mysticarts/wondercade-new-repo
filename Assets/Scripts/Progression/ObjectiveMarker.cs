﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveMarker : MonoBehaviour
{
    public Tutorial tutorial;
    public PlayerController_Default player;
    public string tutorialPrompt = "";


    // Update is called once per frame
    void Update()
    {
        //if the AI is close to the objective marker
        if (Vector3.Distance(transform.position, player.transform.position) < 1.5f)
        {
            tutorial.PromptTutorial(tutorialPrompt); //prompt it.
        }
    }
}
