﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;

[System.Serializable]
public struct EconomyData
{
    public int money;
    public List<AnalyticsData> dayData;
    //public ExpensesData expensesData;
}

[System.Serializable]
public struct AnalyticsData
{
    public int numberOfCustomers;
    public float drinkSales;
    public float foodSales;
    public float arcadeSales;

    public float repairExpenses;
    public float upgradeExpenses;
    public float billsExpenses;
    public float marketingExpenses;

    public float customerSatisfaction;

    public int dirtyMachines;
    public int brokenMachines;
}

public class Economy : MonoBehaviour {

    public RectTransform coin;
    public Vector2 coin_poppedDownSize;
    public Vector2 coin_poppedUpSize;
    public Text computerMoneyText;
    public Text moneyText;
    public Vector2 updateText_defaultPos;
    public Text updateText;

    public int money = 0;

    private void Start() {
        computerMoneyText.text = "" + money;
        moneyText.text = "" + money;
    }

    //give player money, update the UI
    public void Give(int amount) {
        money += amount;
        UpdateCoins();
        updateText.text = "+" + amount;
    }
    //take player money, update the UI
    public void Deduct(int amount) {
        money -= amount;
        UpdateCoins();
        updateText.text = "-" + amount;
    }

    //updates the coins UI:
    public void UpdateCoins() {
        computerMoneyText.text = "" + money;
        moneyText.text = "" + money;
        Tween.Size(coin, coin_poppedUpSize, 0.2f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        Tween.Size(coin, coin_poppedDownSize, 0.1f, 0.3f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);

        Tween.AnchoredPosition(updateText.rectTransform, updateText_defaultPos + (Vector2.down*350), 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        Tween.AnchoredPosition(updateText.rectTransform, updateText_defaultPos, 0.5f, 1.1f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);

        Tween.Size(coin, coin_poppedUpSize, 0.2f, 1.1f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        Tween.Size(coin, coin_poppedDownSize, 0.1f, 1.4f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    } 

    void ApplyDayData(DayStats dayStat, GameData data) {
        AnalyticsData dayData = new AnalyticsData();
        dayData.numberOfCustomers = dayStat.numberOfCustomers;
        dayData.drinkSales = dayStat.drinkSales;
        dayData.foodSales = dayStat.foodSales;
        dayData.arcadeSales = dayStat.arcadeSales;

        dayData.repairExpenses = dayStat.repairExpenses;
        dayData.upgradeExpenses = dayStat.upgradeExpenses;
        dayData.billsExpenses = dayStat.billsExpenses;
        dayData.marketingExpenses = dayStat.marketingExpenses;

        dayData.customerSatisfaction = dayStat.totalCustomerSatisfaction;
        data.economyData.dayData.Add(dayData);
    }

    public void SaveData(ref GameData data) {
        data.economyData.money = money;

        if(WeekStats.dayStats.Count > 0) {
            data.economyData.dayData = new List<AnalyticsData>();

            foreach(var stat in WeekStats.dayStats)
                ApplyDayData(stat, data);

        } else {

            DayNightCycle cycle = FindObjectOfType<DayNightCycle>();
            if(cycle.currentDayStats != null)
                ApplyDayData(cycle.currentDayStats, data);

        }

    }

    public void LoadData(GameData data) {
        money = data.economyData.money;
        moneyText.text = "" + money;

        DayNightCycle cycle = FindObjectOfType<DayNightCycle>();
        
        if(data.economyData.dayData.Count > 0) {
            foreach(var stat in data.economyData.dayData) {
                DayStats dayStats = Instantiate(cycle.dayStatsPrefab, GameObject.Find("WeekStats").transform).GetComponent<DayStats>();
                dayStats.numberOfCustomers = stat.numberOfCustomers;
                dayStats.drinkSales = stat.drinkSales;
                dayStats.foodSales = stat.foodSales;
                dayStats.arcadeSales = stat.arcadeSales;

                dayStats.repairExpenses = stat.repairExpenses;
                dayStats.upgradeExpenses = stat.upgradeExpenses;
                dayStats.billsExpenses = stat.billsExpenses;
                dayStats.marketingExpenses = stat.marketingExpenses;

                dayStats.totalCustomerSatisfaction = stat.customerSatisfaction;

                WeekStats.dayStats.Enqueue(dayStats);
            }

            int dayStatsSize = WeekStats.dayStats.Count - 1;
            int iter = 0;

            foreach(var day in WeekStats.dayStats) {
                if(iter == dayStatsSize) {
                    cycle.currentDayStats = day;
                    break;
                }
                iter++;
            }

        }

    }

}
