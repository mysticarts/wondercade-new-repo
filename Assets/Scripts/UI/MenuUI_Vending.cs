﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public class MenuUI_Vending : MenuUI {

    public Vendor vendor;
    public TextMeshProUGUI title;
    public TextMeshProUGUI stock;
    public UIobject stockBar;

    //analytics:
    public GameObject backButton;
    public List<GameObject> analyticsObjects;
    public List<TextMeshProUGUI> analyticsTextInOrder; //text to display analytic numbers (in order)

    public override void PopUpAnalytics() {
        base.PopUpAnalytics();
        
        //update text
        analyticsTextInOrder[0].text = (GameObject.Find("DayNightCycle").GetComponent<DayNightCycle>().daysPlayed - vendor.dayPurchased) + " days ago";
        analyticsTextInOrder[1].text = "" + vendor.transactions; //transactions
        analyticsTextInOrder[2].text = "$" + vendor.totalSales; //sales
        analyticsTextInOrder[3].text = "$" + vendor.totalCostOfStock; //cost of stock
        analyticsTextInOrder[4].text = "$" + vendor.totalCostOfMaintenance; //cost of stock
        analyticsTextInOrder[5].text = "$" + (vendor.totalSales - vendor.totalCostOfStock - vendor.totalCostOfMaintenance); //total profit

        //pop up analytics
        foreach(GameObject analyticObject in analyticsObjects)
            Tween.LocalScale(analyticObject.gameObject.transform, Vector3.zero, new Vector3(2.56f,2.56f,2.56f), .2f, .2f, Tween.EaseInStrong, Tween.LoopType.None, null, null, false);
    }

    public override void PopDownAnalytics(bool backToMain) {
        base.PopDownAnalytics(backToMain);

        //pop down analytics
        foreach(GameObject analyticObject in analyticsObjects)
            Tween.LocalScale(analyticObject.gameObject.transform, new Vector3(2.56f,2.56f,2.56f), Vector3.zero, .1f, .4f, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
    }
}
