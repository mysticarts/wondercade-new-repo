﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Playables;

public class ScreenFader : MonoBehaviour
{

    public Image screenFade;
    
    [Range(0, 1)] public float alphaValue;
    public GameObject storyCanvas;
    public float time2;



    public void Start()
    {
        StartCoroutine(FadeScreen(time2, alphaValue));
        storyCanvas.SetActive(true);
    }

    public IEnumerator FadeScreen(float _time2, float _alphaValue)
    {
        float alpha = screenFade.color.a;
        for (float t = 0; t < 1; t += Time.deltaTime / time2)
        {
            Color newColor = new Color(screenFade.color.r, screenFade.color.g, screenFade.color.b, Mathf.Lerp(alpha, alphaValue, t));
            screenFade.color = newColor;
            yield return null;
          

        }
        {

            StopCoroutine(FadeScreen(time2, alphaValue));
            storyCanvas.SetActive(false);

        }

    }
}
   /* public IEnumerator FadeTextToZeroAlpha(float _time, float _alphaValue)
    {
        
        float alpha = storyTxt1.color.a;
        for (float t = 0; t < 1; t += Time.deltaTime / time)
        {

            
            Color newColor = new Color(storyTxt1.color.r, storyTxt1.color.g, storyTxt1.color.b, Mathf.Lerp(alpha, alphaValue, t));
            storyTxt1.color = newColor;
            yield return null;
            
            {
               
               
                storyTxt2.color = newColor;
                yield return null;
              

                {
                    storyTxt2.color = newColor;
                    yield return null;

                }
                {
                    
                        StopCoroutine(FadeScreen(time, alphaValue));
                        StopCoroutine(FadeTextToZeroAlpha(time, alphaValue));
                        storyCanvas.SetActive(false);
                    

                }





           


            }
        }
    }

    
}
       */ 