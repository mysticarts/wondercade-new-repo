﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class ControlsDisplay : MonoBehaviour
{
    Player input;
    private SpriteRenderer controlDisplay;
    private Image icontrolDisplay;
    private RawImage rcontrolDisplay;
    public Sprite pcControl;
    public Sprite ps4Control;
    public Sprite XboxControl;

    private string ps4ID = "Wireless Controller";
    private string xboxID = "XInput Gamepad";

    // Start is called before the first frame update
    void Start()
    {
        input = ReInput.players.GetPlayer(0);
        controlDisplay = GetComponent<SpriteRenderer>();
        icontrolDisplay = GetComponent<Image>();
        rcontrolDisplay = GetComponent<RawImage>();
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_STANDALONE_WIN
        if (input.controllers.joystickCount == 0)
        {
            if (controlDisplay != null && pcControl != null)
            {
                controlDisplay.sprite = pcControl;
            }
            if (icontrolDisplay != null && pcControl != null)
            {
                icontrolDisplay.sprite = pcControl;
            }
            if (rcontrolDisplay != null && pcControl != null)
            {
                rcontrolDisplay.texture = pcControl.texture;
            }
            if (pcControl != null)
            {
                SetRenderers(true);
            }
            else
            {
                gameObject.SetActive(false);
                SetRenderers(false);
            }
        }
        else
        {
            if (input.controllers.Joysticks[0].hardwareName.Contains(ps4ID))
            {
                if (ps4Control != null)
                {
                    if (controlDisplay != null)
                    {
                        controlDisplay.sprite = ps4Control;
                    }
                    if (icontrolDisplay != null)
                    {
                        icontrolDisplay.sprite = ps4Control;
                    }
                    if (rcontrolDisplay != null)
                    {
                        rcontrolDisplay.texture = ps4Control.texture;
                    }
                } else gameObject.SetActive(false);
              
            }
            if (input.controllers.Joysticks[0].hardwareName.Contains(xboxID))
            {
                if (XboxControl != null)
                {
                    if (controlDisplay != null)
                    {
                        controlDisplay.sprite = XboxControl;
                    }
                    if (icontrolDisplay != null)
                    {
                        icontrolDisplay.sprite = XboxControl;
                    }
                    if (rcontrolDisplay != null)
                    {
                        rcontrolDisplay.texture = XboxControl.texture;
                    }
                } else gameObject.SetActive(false);
            }

            


            if (XboxControl == null && ps4Control == null)
            {
                SetRenderers(false);
            }
            else
            {
                SetRenderers(true);
            }



        }
#endif
#if UNITY_PS4
        if (ps4Control != null)
                {
                    if (controlDisplay != null)
                    {
                        controlDisplay.sprite = ps4Control;
                    }
                    if (icontrolDisplay != null)
                    {
                        icontrolDisplay.sprite = ps4Control;
                    }
                    if (rcontrolDisplay != null)
                    {
                        rcontrolDisplay.texture = ps4Control.texture;
                    }
                }
#endif            
    }

    void SetRenderers(bool status)
    {
        if (controlDisplay != null)
        {
            controlDisplay.enabled = status;
        }
        if (icontrolDisplay != null)
        {
            icontrolDisplay.enabled = status;
        }
        if (rcontrolDisplay != null)
        {
            rcontrolDisplay.enabled = status;
        }
    }


}
