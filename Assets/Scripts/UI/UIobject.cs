﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIobject : MonoBehaviour {
    public Vector2 poppedUpSize;
    public Vector2 currentSize;
    public Vector2 poppedDownSize;
}
