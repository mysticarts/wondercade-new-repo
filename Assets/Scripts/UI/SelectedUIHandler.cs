﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Rewired;

public class SelectedUIHandler : MonoBehaviour
{

    private Player input;
    private EventSystem eventSystem;
    private GameObject lastSelected;
    // Start is called before the first frame update
    void Start()
    {
        eventSystem = GetComponent<EventSystem>();
        input = ReInput.players.GetPlayer(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (input.GetAxis("CursorX") != 0 || input.GetAxis("CursorY") != 0)
        {
            eventSystem.SetSelectedGameObject(null);
        }

        if (input.GetButtonDown("UIVertical") || input.GetNegativeButtonDown("UIVertical") || input.GetNegativeButtonDown("UIHorizontal") || input.GetButtonDown("UIHorizontal"))
        {
            if (eventSystem.currentSelectedGameObject == null && lastSelected != null)
            {
                eventSystem.SetSelectedGameObject(lastSelected);
            }
        }

        if (eventSystem.currentSelectedGameObject != null)
        {
            lastSelected = eventSystem.currentSelectedGameObject;
        }



    }
}
