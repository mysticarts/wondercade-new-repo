﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIobject_Text : UIobject_3D {
    public TextMeshProUGUI text;
    public RectTransform bar = null;

    public void Start() {
        text = this.gameObject.GetComponent<TextMeshProUGUI>();
    }

    void Update() {
        //set text in middle of bar
        if(bar != null)
            text.margin = new Vector4(text.margin.x, text.margin.y, 200-bar.sizeDelta.x, text.margin.w); 
    }

}
