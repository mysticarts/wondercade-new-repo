﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour {
    public Image placeHolderImage;
    public Text costPlaceholder;
    public Text amountPlaceholder;
    public Text namePlaceholder;
    public Button button;

    public Item item; //item reference - holds data like the shop image
}
