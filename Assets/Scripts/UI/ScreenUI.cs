﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;

public class ScreenUI : MonoBehaviour {

    public RectTransform saving;
    public Vector2 saving_poppedUpPos;
    public Vector2 saving_poppedDownPos;

    public Text totalMoney;
    public RectTransform coins;
    public Vector2 coins_poppedUpPos;
    public Vector2 coins_poppedDownPos;

    public RectTransform clock;
    public Vector2 clock_poppedUpPos;
    public Vector2 clock_poppedDownPos;

    public RectTransform spaceBar;
    public RectTransform rightStick;
    public Vector2 spaceBar_poppedUpPos;
    public Vector2 spaceBar_poppedDownPos;

    public ViewManager viewManager;

    private void Start() {
        viewManager = Camera.main.GetComponent<ViewManager>();
    }

    public void PopUp() {
        if(saving != null && saving_poppedDownPos != null) Tween.AnchoredPosition(saving, saving_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(coins != null && coins_poppedDownPos != null) Tween.AnchoredPosition(coins, coins_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(clock != null && clock_poppedDownPos != null) Tween.AnchoredPosition(clock, clock_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        
        if(viewManager == null) viewManager = Camera.main.GetComponent<ViewManager>();
        if(viewManager.previousView != View.EDIT && spaceBar != null && spaceBar_poppedDownPos != null) Tween.AnchoredPosition(spaceBar, spaceBar_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(viewManager.previousView != View.EDIT && rightStick != null && spaceBar_poppedDownPos != null) Tween.AnchoredPosition(rightStick, spaceBar_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }

    public void PopDown() {
        if(saving != null && saving_poppedDownPos != null) Tween.AnchoredPosition(saving, saving_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(coins != null && coins_poppedDownPos != null) Tween.AnchoredPosition(coins, coins_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(clock != null && clock_poppedDownPos != null) Tween.AnchoredPosition(clock, clock_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(spaceBar != null && spaceBar_poppedDownPos != null) Tween.AnchoredPosition(spaceBar, spaceBar_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(rightStick != null && spaceBar_poppedDownPos != null) Tween.AnchoredPosition(rightStick, spaceBar_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }

}
