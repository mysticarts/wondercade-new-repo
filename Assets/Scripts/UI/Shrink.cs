﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class Shrink : MonoBehaviour {

    public Vector2 poppedUpSize;
    public Vector2 poppedDownSize;

    void Start() {
        Tween.LocalScale(this.GetComponent<RectTransform>(), poppedUpSize, new Vector2(poppedDownSize.x, poppedUpSize.y/2), .5f, 0.5f, Tween.EaseInOutStrong);
        Tween.LocalScale(this.GetComponent<RectTransform>(), Vector2.zero, 1f, 1f, Tween.EaseOutStrong);
    }
}
