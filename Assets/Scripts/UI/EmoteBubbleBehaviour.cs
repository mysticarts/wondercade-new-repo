﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmoteBubbleBehaviour : MonoBehaviour
{
    public Transform rotationPoint;
    private PlayerController_Default player;
    public float fadeScalar = 0;
    public List<SpriteRenderer> renderers;

    private void Start()
    {
        player = FindObjectOfType<PlayerController_Default>();
    }
    // Update is called once per frame
    void Update()
    {
        if (transform.localScale.magnitude != 0)
        {
            foreach (var ren in renderers)
            {
                ren.color = new Color(1,1,1, (1 / Vector3.Distance(ren.transform.position, player.transform.position)) * fadeScalar);
            }
          


            transform.LookAt(Camera.main.transform);

            if (Camera.main.WorldToScreenPoint(transform.position).x > Camera.main.WorldToScreenPoint(rotationPoint.position).x)
            {
                FlipBubble(180, 1);
            }
            if (Camera.main.WorldToScreenPoint(transform.position).x < Camera.main.WorldToScreenPoint(rotationPoint.position).x)
            {
                FlipBubble(0, -1);
                //transform.rotation *= Quaternion.Euler(0, 0, 0);
            }
        }
    }

    void FlipBubble(float angle, float offset)
    {
        transform.rotation *= Quaternion.Euler(0, angle, 0);

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
           // child.localPosition = new Vector3(child.localPosition.x, child.localPosition.y, -0.011f * offset);
        }


    }




}
