﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public class MenuUI_Arcade : MenuUI {

    public Arcade arcade;
    public TextMeshProUGUI title;
    public TextMeshProUGUI dirtiness;
    public TextMeshProUGUI breakage;
    public UIobject dirtinessBar;
    public UIobject breakageBar;

    public void Start() {
        //if (arcade != null)
        //{
        //    title.text = arcade.gameName.ToString() + " Arcade Machine";

        //}

        Tween.LocalScale(this.GetComponent<RectTransform>(), Vector3.zero, 0.01f, 0, Tween.EaseOutStrong);
        if (arcade != null)
        {
            if (arcade.rotatingCabinet != null) arcade.rotatingCabinet.SetActive(true);
        }
    }

    public List<GameObject> analyticsObjects;
    public List<TextMeshProUGUI> analyticsTextInOrder; //text to display analytic numbers (in order)

    public override void PopUpAnalytics() {
        base.PopUpAnalytics();
        
        //update text
        analyticsTextInOrder[0].text = "" + arcade.timesPlayed;
        analyticsTextInOrder[1].text = "$" + arcade.totalSales;
        analyticsTextInOrder[2].text = "$" + arcade.totalCostOfMaintenance;
        analyticsTextInOrder[3].text = "$" + (arcade.totalSales - arcade.totalCostOfMaintenance);

        //pop up analytics
        foreach(GameObject analyticObject in analyticsObjects)
        {
            if (analyticObject.name.Contains("Title") || analyticObject.name.Contains("Back"))
            {
                Tween.LocalScale(analyticObject.gameObject.transform, Vector3.zero, new Vector3(2.56f, 2.56f, 2.56f), .1f, .4f, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            }
            else
            {
                Tween.LocalScale(analyticObject.gameObject.transform, Vector3.zero, new Vector3(1, 1, 1), .1f, .4f, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            }
        }
    }

    public override void PopDownAnalytics(bool backToMain) {
        base.PopDownAnalytics(backToMain);

        //pop down analytics
        foreach(GameObject analyticObject in analyticsObjects)
            Tween.LocalScale(analyticObject.gameObject.transform, analyticObject.gameObject.transform.localScale, Vector3.zero, .1f, .4f, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
    }

}
