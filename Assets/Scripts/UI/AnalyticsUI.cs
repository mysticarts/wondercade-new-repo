﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//analytics UI for office menu and EOD
public class AnalyticsUI : MonoBehaviour {

    //sales
    public PieGraph spendingGraph;
    public Text numberOfCustomers;
    public Text totalSalesText;
    public Text averageSpending;
    public Text arcadesPercent;
    public Text drinksPercent;
    public Text foodPercent;

    //profit
    public PieGraph expensesGraph;
    public Text expenses;
    public Text profit;
    public Text repairsPercent;
    public Text upgradePercent;
    public Text billsPercent;
    public Text marketingPercent;

    //morale
    public Text moraleText;
    public GameObject happyFace;
    public GameObject neutralFace;
    public GameObject angryFace;
    public List<GameObject> points;
    public List<Text> pointsText;
    public Progression progression;
    
    //update the data in the office menu analytics - weekly
    public void UpdateData() {

        //sales

        numberOfCustomers.text = "" + WeekStats.GetTotalCustomers();

        float totalSales = WeekStats.GetTotalSales();
        totalSalesText.text = "$" + totalSales;
        averageSpending.text = "$" + WeekStats.GetAverageSpending();
        
        if(totalSales == 0) {
            for(int count = 0; count <= 2; count++) {
                expensesGraph.values[count] = 1.0f/3.0f;
            }
        } else {
            spendingGraph.values[0] = WeekStats.GetArcadeSales() / totalSales;
            spendingGraph.values[1] = WeekStats.GetDrinkSales() / totalSales;
            spendingGraph.values[2] = WeekStats.GetFoodSales() / totalSales;
        }
        arcadesPercent.text = Mathf.RoundToInt(spendingGraph.values[0] * 100) + "%";
        drinksPercent.text = Mathf.RoundToInt(spendingGraph.values[1] * 100) + "%";
        foodPercent.text = Mathf.RoundToInt(spendingGraph.values[2] * 100) + "%";
        spendingGraph.Display();

        //profit

        float totalExpenses = WeekStats.GetTotalExpenses();
        expenses.text = "$" + totalExpenses;
        
        if(totalExpenses == 0) {
            for(int count = 0; count <= 3; count++) {
                expensesGraph.values[count] = 0.25f;
            }
        } else {
            expensesGraph.values[0] = WeekStats.GetRepairExpenses() / totalExpenses;
            expensesGraph.values[1] = WeekStats.GetBillsExpenses() / totalExpenses;
            expensesGraph.values[2] = WeekStats.GetUpgradeExpenses() / totalExpenses;
            expensesGraph.values[3] = WeekStats.GetMarketingExpenses() / totalExpenses;
        }
        
        repairsPercent.text = Mathf.RoundToInt(expensesGraph.values[0] * 100) + "%";
        billsPercent.text = Mathf.RoundToInt(expensesGraph.values[1] * 100) + "%";
        upgradePercent.text = Mathf.RoundToInt(expensesGraph.values[2] * 100) + "%";
        marketingPercent.text = Mathf.RoundToInt(expensesGraph.values[3] * 100) + "%";
        expensesGraph.Display();

        profit.text = "$" + WeekStats.GetTotalProfit();

        //morale 

        float morale = WeekStats.GetSatisfaction();

        if(morale > 70) {
            progression.UnlockItem("Cocktail Cabinet - Hungry Mungry", true);
        }

        moraleText.text = Mathf.RoundToInt(morale) + "%";

        //set morale face
        happyFace.SetActive(false); neutralFace.SetActive(false); angryFace.SetActive(false);
        if(morale > ((100/3)*2)) happyFace.SetActive(true);
        else if(morale > ((100/3))) neutralFace.SetActive(true);
        else angryFace.SetActive(true);

        //graph dots
        int height = 70;
        for(int count = 0; count < WeekStats.dayStats.Count; count++) {
            DayStats dayStats = WeekStats.dayStats.ToArray()[count];

            int percent = (int) dayStats.GetAverageSatisfaction() / 100;

            RectTransform rect = points[count].GetComponent<RectTransform>();
            rect.localPosition = new Vector3(rect.localPosition.x, -115 + (height * percent), rect.localPosition.z);

            pointsText[count].text = dayStats.GetAverageSatisfaction() + "%";
        }

    }

    //update analytics UI for EOD - one day
    public void UpdateData(DayStats stats) {

        //sales

        numberOfCustomers.text = "" + stats.numberOfCustomers;

        float totalSales = stats.GetTotalSales();
        totalSalesText.text = "$" + totalSales;
        averageSpending.text = "$" + stats.GetAverageSpending();
        
        if(totalSales == 0) {
            for(int count = 0; count <= 2; count++) {
                expensesGraph.values[count] = 1.0f/3.0f;
            }
        } else {
            spendingGraph.values[0] = stats.arcadeSales / totalSales;
            spendingGraph.values[1] = stats.drinkSales / totalSales;
            spendingGraph.values[2] = stats.foodSales / totalSales;
        }
        arcadesPercent.text = Mathf.RoundToInt(spendingGraph.values[0] * 100) + "%";
        drinksPercent.text = Mathf.RoundToInt(spendingGraph.values[1] * 100) + "%";
        foodPercent.text = Mathf.RoundToInt(spendingGraph.values[2] * 100) + "%";
        spendingGraph.Display();

        //profit

        float totalExpenses = stats.GetTotalExpenses();
        expenses.text = "$" + totalExpenses;
        
        if(totalExpenses == 0) {
            for(int count = 0; count <= 3; count++) {
                expensesGraph.values[count] = 0.25f;
            }
        } else {
            expensesGraph.values[0] = stats.repairExpenses / totalExpenses;
            expensesGraph.values[1] = stats.billsExpenses / totalExpenses;
            expensesGraph.values[2] = stats.upgradeExpenses / totalExpenses;
            expensesGraph.values[3] = stats.marketingExpenses / totalExpenses;
        }
        
        repairsPercent.text = Mathf.RoundToInt(expensesGraph.values[0] * 100) + "%";
        billsPercent.text = Mathf.RoundToInt(expensesGraph.values[1] * 100) + "%";
        upgradePercent.text = Mathf.RoundToInt(expensesGraph.values[2] * 100) + "%";
        marketingPercent.text = Mathf.RoundToInt(expensesGraph.values[3] * 100) + "%";
        expensesGraph.Display();

        profit.text = "$" + stats.GetProfit();

        //morale 

        float morale = stats.GetAverageSatisfaction();
        moraleText.text = Mathf.RoundToInt(morale) + "%";

        //set morale face
        happyFace.SetActive(false); neutralFace.SetActive(false); angryFace.SetActive(false);
        if(morale > ((100/3)*2)) happyFace.SetActive(true);
        else if(morale > ((100/3))) neutralFace.SetActive(true);
        else angryFace.SetActive(true);

        //graph dots
        int height = 70;
        for(int count = 0; count < WeekStats.dayStats.Count; count++) {
            DayStats dayStats = WeekStats.dayStats.ToArray()[count];

            int percent = (int) dayStats.GetAverageSatisfaction() / 100;

            RectTransform rect = points[count].GetComponent<RectTransform>();
            rect.localPosition = new Vector3(rect.localPosition.x, -115 + (height * percent), rect.localPosition.z);

            pointsText[count].text = dayStats.GetAverageSatisfaction() + "%";
        }
    }

}
