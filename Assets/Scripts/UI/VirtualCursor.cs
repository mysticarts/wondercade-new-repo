﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VirtualCursor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Enable()
    {
        transform.position = new Vector2(Screen.width / 2, Screen.height / 2);
    }


    public void OnScreenPositionChanged(Vector2 screenPosition)
    {
        transform.position = screenPosition;
    }

}
