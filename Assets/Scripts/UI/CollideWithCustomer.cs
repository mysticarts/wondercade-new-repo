﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Pixelplacement;

//player interactions with customers
public class CollideWithCustomer : MonoBehaviour {

    private bool playerIsInRadius = false;
    private ViewManager viewManager;
    public MenuUI_Customer menu;
    public GameObject manageButton; //the 'interact' diegetic ui button
    public Vector3 poppedUpSize = new Vector3(1, 1, 0.5f);

    private Vector3 initialDiegetic_Scale; //reference to the initial editor scale
    private Vector3 initialDiegetic_Pos; //reference to the initial editor position
    public GameObject firstPersonDiegeticTransform; //use this position for the manageButton in first person view

    private void OnTriggerEnter(Collider other) {
        //if player collides with customers 'interaction collider':
        if(other.CompareTag("Player") && this.CompareTag("AI") && viewManager.currentView != View.EDIT) {
            ShowDiegeticUI(true);
            playerIsInRadius = true;
            PlayerManager.customerInteractingWith = transform.parent.GetComponent<AIStats>(); //stats is on parent of collider
        }
    }

    private void OnTriggerExit(Collider other) {
        //if player leaves customers collider:
        if(other.CompareTag("Player") && this.CompareTag("AI")) {
            ShowDiegeticUI(false);
            playerIsInRadius = false;
            PlayerManager.customerInteractingWith = null;
        }
    }

    public void ShowDiegeticUI(bool show) {

        //change the size/position of the diegetic UI depending on view:
        if(firstPersonDiegeticTransform == null) firstPersonDiegeticTransform = this.gameObject.transform.parent.transform.Find("FirstPersonDiegeticTransform").gameObject;
        if(viewManager.currentView == View.FIRST) {
            poppedUpSize = initialDiegetic_Scale/3;
            manageButton.transform.localPosition = firstPersonDiegeticTransform.transform.localPosition; //set the diegetic UI to first person size
        } else {
            poppedUpSize = initialDiegetic_Scale;
            manageButton.transform.localPosition = initialDiegetic_Pos;
        }

        if(show) {
            PlayerManager.customerInteractingWith = this.GetComponent<AIStats>();

            //pop up manage button:
            Tween.LocalScale(manageButton.transform, poppedUpSize, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);

        } else {
            PlayerManager.customerInteractingWith = null;

            //pop down all buttons:
            Tween.LocalScale(manageButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            
        }
    }

    protected Player input;
    private NotificationSystem interactions;
    private AISpawning spawning;

    public void Start() {
        input = ReInput.players.GetPlayer(0);
        viewManager = FindObjectOfType<ViewManager>();
        spawning = FindObjectOfType<AISpawning>();
        interactions = GameObject.Find("UI").transform.Find("InteractionBar").GetComponent<NotificationSystem>();

        //set references to initial diegetic UI size:
        initialDiegetic_Scale = poppedUpSize;
        initialDiegetic_Pos = manageButton.transform.localPosition;
      //  firstPersonDiegeticTransform = this.gameObject.transform.parent.transform.Find("FirstPersonDiegeticTransform").gameObject;
    }

    //player clicks interact button near customer:
    public void Update() {


        if (viewManager != null)
        {
            if (viewManager.currentView == View.THIRD)
            {
                //third person interaction:

                //pop up customer menu
                if (input.GetButtonDown("Interact"))
                {
                    if (playerIsInRadius && menu != null && PlayerManager.menuUsing == null)
                    {
                        menu.transform.parent.gameObject.SetActive(true);
                        menu.PopUp(true);
                    }
                }

                //pop down customer menu
                if (input.GetButtonDown("Back"))
                {
                    if (menu != null && PlayerManager.menuUsing != null && GameObject.ReferenceEquals(PlayerManager.menuUsing, menu)
                       && menu.transform.localScale != Vector3.zero)
                        menu.PopDown(true);
                }

            }
            else if (viewManager.currentView == View.FIRST)
            {
                //first person interaction:

                //add interaction notification
                if (playerIsInRadius && input.GetButtonDown("Interact"))
                {
                    if (PlayerManager.customerInteractingWith != null) interactions.Add(new Notification("Interaction with " + PlayerManager.customerInteractingWith.firstName, spawning.GenerateComment(PlayerManager.customerInteractingWith)));
                }
            }
        }
        

    }
}