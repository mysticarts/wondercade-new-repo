﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//put on the cleaning button in the office shop menu to get users to click it for the first time
public class FirstTimeClean : MonoBehaviour {

    public bool pulsing = false;

    private float lerp = 0;
    private Vector3 minScale = new Vector3(.5f,.08f,0);
    private Vector3 maxScale = new Vector3(.6f,.1f,0);
    public void Update() {

        if(!BuildingManager.firstRoomCleaned) {
            pulsing = true;
        } else {
            pulsing = false;
            transform.localScale = new Vector3(.55f,.1f,0);
        }

        if(pulsing) {
            lerp += 1.3f * Time.unscaledDeltaTime;
            transform.localScale = Vector3.Lerp(minScale, maxScale, lerp);

            //flip the lerp:
            if(lerp > 1f) {
                Vector3 temp = maxScale;
                maxScale = minScale;
                minScale = temp;
                lerp = 0;
            }
        }
    }

}
