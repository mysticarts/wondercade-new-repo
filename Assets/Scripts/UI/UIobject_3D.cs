﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class UIobject_3D : MonoBehaviour {

    public Vector3 poppedDownSize;
    public Vector3 poppedUpSize;
    public bool rotating = false;
    public float rotateSpeed = 1;
    public bool pulse = false;
    private Vector3 pulseInScale;
    private Vector3 pulseOutScale;

    private void Start()
    {
        pulseInScale = transform.localScale / 2;
        pulseOutScale = transform.localScale;
    }


    void Update() {
        if(rotating && Time.timeScale > 0) {
            this.gameObject.transform.Rotate(new Vector3(0, rotateSpeed, 0));
        }

        if (pulse)
        {
            if (transform.localScale == pulseOutScale)
            {
                Tween.LocalScale(transform, pulseInScale, 2, 0);
            }
            if (transform.localScale == pulseInScale)
            {
                Tween.LocalScale(transform, pulseOutScale, 2, 0);
            }
        }

    }

}
