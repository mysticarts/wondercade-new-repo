﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;

public class MenuUI_EOD : MenuUI {
    
    public DayNightCycle cycle;
    public AnalyticsUI analytics;
    public GameObject content;

    public void Start() {
        cycle = GameObject.Find("DayNightCycle").GetComponent<DayNightCycle>();    
    }

    override public void PopUp(bool panel) {
        //update stats for day current day
        analytics.UpdateData(cycle.currentDayStats);

        base.PopUp(true);
        PlayerManager.menuUsing = this;

        Tween.LocalScale(content.transform, new Vector3(1, 1, 1), .2f, 0.5f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
        Tween.LocalScale(content.transform, Vector3.zero, .2f, 20, Tween.EaseInOutStrong, Tween.LoopType.None, delegate { PopDown(true); }, null, false);
    }

    override public void PopDown(bool panel) {
        //if the user hasnt dismissed the UI.
        if (base.GetComponent<RectTransform>().localScale.x > 0)
        {
            base.PopDown(true);

            Tween.LocalScale(content.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
        }

    }

}