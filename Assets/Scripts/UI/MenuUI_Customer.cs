﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuUI_Customer : MenuUI {
    
    public AIStats stats;

    public GameObject realModel; //the actual customer model
    
    public GameObject rotatingPosition;
    public float rotateSpeed = 1;
    private GameObject rotatingModel; //an instantiated version of the model that rotates at rotatingPosition;


    public TextMeshProUGUI moodStatus;
    public TextMeshProUGUI excitementStatus;
    public TextMeshProUGUI cleanlinessStatus;
    public TextMeshProUGUI hungerStatus;
    public TextMeshProUGUI thirstStatus;
    public TextMeshProUGUI walletBalance;


    public TextMeshProUGUI title;

    public void Start() {
        GameObject rotatingModel = GameObject.Instantiate(realModel, rotatingPosition.transform);
        SetLayer(rotatingModel.transform, 10); //set as menuUI
        Canvas canvas = gameObject.GetComponentInParent<Canvas>();
        canvas.worldCamera = FindObjectOfType<Tutorial>().player.viewManager.UICamera;

        title.text = stats.firstName + " " + stats.lastName;
    }

    //set children as same layer
    public void SetLayer(Transform transform, int layer) {
        transform.gameObject.layer = layer;
        foreach(Transform child in transform)
            SetLayer(child, layer);
    }

}

