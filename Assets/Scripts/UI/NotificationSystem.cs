﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;

public class Notification {
    public string title;
    public string message;

    public Notification(string title, string message) {
        this.title = title;
        this.message = message;
    }
}

public class NotificationSystem : MonoBehaviour {

    public GameObject notificationBar;
    public float timeShown = 5; //in seconds
    public Vector2 poppedUpPos = new Vector2(0, 427);
    public Vector2 poppedDownPos = new Vector2(0, 581);

    public List<Notification> notifications = new List<Notification>(); // a queue of messages
    public void Add(Notification notification) { //use this when displaying a message
        if(!containsMessage(notification.message)
            && (current == null || (current.message != null && current.message != notification.message)))
            notifications.Add(notification);
    }

    private bool containsMessage(string message) {
        List<Notification> notificationsCopy = notifications;
        List<string> messages = new List<string>();
        //add messages:
        for(int count = 0; count < notifications.Count; count++) {
            messages.Add(notificationsCopy[0].message);
        }
        return messages.Contains(message);
    }

    private Notification current = null;
    private void Update() {
        if(notifications.Count > 0 && current == null) {
            //Debug.Log("SHOWING: " + notifications[0].title + " - " + notifications[0].message);
            Show(notifications[0]);
        }
    }

    private void Show(Notification notification) {
        current = notification;

        notifications.RemoveAt(0);

        //update notificationBar text
        Text title = notificationBar.transform.Find("Title").GetComponent<Text>();
        title.text = notification.title;

        Text message = notificationBar.transform.Find("Message").GetComponent<Text>();
        message.text = notification.message;

        //tween notificationBar
        Tween.AnchoredPosition(notificationBar.GetComponent<RectTransform>(), poppedUpPos, 0.5f, 0, Tween.EaseBounce);
        StartCoroutine(PopDown());
        StartCoroutine(Clear());
    }

    //time scale not working with Tween - made my own
    private IEnumerator PopDown() {
        yield return new WaitForSecondsRealtime(timeShown + .5f);
        Tween.AnchoredPosition(notificationBar.GetComponent<RectTransform>(), poppedDownPos, 0.5f, 0, Tween.EaseInOutStrong);
    }

    private IEnumerator Clear() {
        yield return new WaitForSecondsRealtime(6f);
        current = null;
    }

}