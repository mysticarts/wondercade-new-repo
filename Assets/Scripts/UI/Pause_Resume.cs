﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Pixelplacement;
[RequireComponent(typeof(AudioSource))]


public class Pause_Resume : MonoBehaviour
{
    public GameObject pauseMenuPanel;
   // public EventSystem eventSystem;
    public AudioSource pauseMenuActive;
    public AudioClip pauseSound;
    public AudioClip unPause;
    public GameObject howToPlay;
    public GameObject settingPanel;
    public GameObject achievements;
    public GameObject pauseSelected;
    public Player input;
    public SceneManage sceneManage;
    private CurrentSave currentSave;
    public ViewManager viewManager;
    private EventSystem eventSystem;
    private Tutorial tutorial;
    [HideInInspector]
    public bool allowPause = false;

    private void Start()
    {
        input = ReInput.players.GetPlayer(0);
        pauseMenuPanel.SetActive(false);
        settingPanel.SetActive(false);
        howToPlay.SetActive(false);
        currentSave = FindObjectOfType<CurrentSave>();
        eventSystem = FindObjectOfType<EventSystem>();
        tutorial = FindObjectOfType<Tutorial>();
    }

    private void Update()
    {
        //Registers when the escape key is pressed 
        if (input.GetButtonDown("Pause") && PlayerManager.menuUsing == null && !pauseMenuPanel.activeInHierarchy && viewManager.currentView != View.EDIT
           && !viewManager.isUpdating && !tutorial.sectionActive && allowPause) //We have to use rewired, for all controllers to work. - James B
        {
            pauseMenuPanel.SetActive(!pauseMenuPanel.gameObject.activeSelf);
            pauseMenuActive = GetComponent<AudioSource>();
            pauseMenuActive.PlayOneShot(pauseSound, 1);
            input.controllers.maps.SetMapsEnabled(false, "Default");

            ResetPause();
            eventSystem.SetSelectedGameObject(pauseSelected);

            Time.timeScale = 0;

            return;
        }

        if (input.GetButtonDown("Back") && PlayerManager.menuUsing == null && !pauseMenuPanel.activeInHierarchy && viewManager.currentView != View.EDIT
           && !viewManager.isUpdating && !tutorial.sectionActive && Tween.activeTweens.Count == 0 && allowPause)
        {
            ResetPause();
            pauseMenuPanel.SetActive(true);
        }

        if (input.GetButtonDown("Pause") && PlayerManager.menuUsing == null && pauseMenuPanel.activeInHierarchy)
        {
            input.controllers.maps.SetMapsEnabled(true, "Default");
            pauseMenuPanel.SetActive(false);
            Time.timeScale = 1;
            return;
        }
    }

        //restricts the time scale to 0, meaning anything synamic is idle

    public void ContinueButton()
        {
            Time.timeScale = 1;
            pauseMenuPanel.SetActive(false);
        //Nomralises the time scale

    }

    public void ResetPause()
    {
        howToPlay.SetActive(false);
        settingPanel.SetActive(false);
        achievements.SetActive(false);
    }

    public void TitleScreen()
     {
         SceneManage manage = Instantiate(sceneManage);
         manage.selectedlevel = "Title_Screen";
         
        if (currentSave != null)
        {
            currentSave.serialization.SaveGame();
            Destroy(currentSave.gameObject);
        }

        manage.LoadScene("Title_Screen");
         
    
     }

  
}