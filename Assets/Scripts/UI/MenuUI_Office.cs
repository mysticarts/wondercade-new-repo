﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public class MenuUI_Office : MenuUI {

    public CRTUI crtUI;
    public ScreenUI screenUI;
    private EventSystem eventSystem;
    public MachineManager machineManager;
    public GameObject activeTab; //the current selected tab in the office menu

    private void Start() {
        base.Start();
        eventSystem = FindObjectOfType<EventSystem>();
        machineManager = GameObject.Find("Managers").GetComponent<MachineManager>();
    }

    public void PopUpTab(GameObject tab) {
        activeTab.SetActive(false);
        activeTab = tab;
        activeTab.SetActive(true);

        //update items
        if(tab.transform.parent.name.Contains("Shop"))
            machineManager.content.Populate(machineManager.content.currentDisplayed);
    }

    public GameObject playerStandingPos;
    public GameObject viewingPos;
    override public void PopUp(bool panel) {
        base.PopUp(true);
        PlayerManager.menuUsing = this;
        crtUI.PopUp();

        // zoom into screen:
        ViewManager viewManager = GameObject.Find("Managers").GetComponent<MachineManager>().viewManager;
        viewManager.gameViewPos.transform.SetPositionAndRotation(viewingPos.transform.position, viewingPos.transform.rotation);
        viewManager.Switch(View.GAME);

        screenUI.PopDown();
    }

    override public void PopDown(bool panel) {
        base.PopDown(true);
        crtUI.PopDown();

        ViewManager viewManager = GameObject.Find("Managers").GetComponent<MachineManager>().viewManager;
        viewManager.Switch(viewManager.previousView);

        screenUI.PopUp();
    }

}