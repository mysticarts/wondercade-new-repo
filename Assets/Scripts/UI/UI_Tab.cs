﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum ButtonType {
    ALL,
    ARCADE,
    VENDING,
    DECORATION
}

public class UI_Tab : MonoBehaviour {

    public ButtonType type = ButtonType.ALL;
    public PlayerController_Default player;
    public PopulateGrid content;

    public void Start() {
        player = FindObjectOfType<PlayerController_Default>();
    }

    public void TutorialPopUp(bool status) {
        if(status)
            content.Populate(type);
        gameObject.SetActive(status);
    }

    public void PopUpTab() {
        content.Populate(type);
    }

    public void Update() {
        if(EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.CompareTag("RightButton")
            && (player.input.GetButtonDown("UIHorizontal") || player.input.GetButtonDown("Right"))) {
            if(content.items.Count != 0) {
                GameObject button = content.transform.GetChild(0).transform.Find("Button").gameObject;
                EventSystem.current.SetSelectedGameObject(button);
            }
        }
    }

}
