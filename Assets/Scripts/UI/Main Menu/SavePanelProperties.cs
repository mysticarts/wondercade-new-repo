﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SavePanelProperties : MonoBehaviour
{
    public Button panelButton;
    public Text saveFileName;
    public Text currencyText;
    public Text gameTimeText;
    public Text weekCount;
    public Text saveTime;
    public Image saveImage;
    public CurrentSave currentSave;
    [HideInInspector]
    public string saveFile;
    public void SetButton()
    {
        panelButton.onClick.AddListener(delegate { currentSave.enabled = true; });
        panelButton.onClick.AddListener(delegate { currentSave.LoadGame(saveFile); });
        SceneManage sceneManger = FindObjectOfType<SceneManage>();
        panelButton.onClick.AddListener(delegate { sceneManger.LoadScene("James B New"); });
    }

    public Sprite LoadImage(string path)
    {
        byte[] bytes = File.ReadAllBytes(path);

        Texture2D texture = new Texture2D(600,600, TextureFormat.RGBA32, false);
        texture.filterMode = FilterMode.Trilinear;
        ImageConversion.LoadImage(texture, bytes);

        Sprite sprite = Sprite.Create(texture, new Rect(0,0,texture.width,texture.height), new Vector2(0.5f, 0.5f));
        sprite.name = path;

        return sprite;
    }


}
