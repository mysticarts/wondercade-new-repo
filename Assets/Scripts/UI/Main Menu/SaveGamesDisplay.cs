﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SaveGamesDisplay : MonoBehaviour
{
    public GameObject displayPrefab;
    public CurrentSave currentSave;
    public List<GameObject> savePanels = new List<GameObject>();
    public int maxSaves = 0;
    
    // Start is called before the first frame update
    void OnEnable()
    {
        float panelHeight = Screen.height -  160;

        var info = new DirectoryInfo(Application.persistentDataPath + "/GameSaves");
        var fileInfo = info.GetDirectories();
        for (int i = 0; i < fileInfo.Length; i++)
        {
            if (i < maxSaves)
            {
                SavePanelProperties panel = Instantiate(displayPrefab, new Vector2(Screen.width / 2, panelHeight), Quaternion.Euler(0, 0, 0), transform).GetComponent<SavePanelProperties>();
                GameData data;
                panel.saveFile = fileInfo[i].ToString() + "/GameData.Arcade";
                if (File.Exists(panel.saveFile))
                {
                    using (StreamReader streamReader = File.OpenText(panel.saveFile))
                    {
                        string jsonString = streamReader.ReadToEnd();
                        data = JsonUtility.FromJson<GameData>(jsonString);
                    }

                    panel.currentSave = FindObjectOfType<CurrentSave>();
                    panel.SetButton();
                    panel.saveFileName.text = "GameSave" + i;
                    panel.currencyText.text += data.economyData.money;
                    panel.gameTimeText.text = data.cycleData.timeText;
                    panel.weekCount.text = data.cycleData.weekText;
                    panel.saveTime.text = data.saveTime;
                    panel.saveImage.sprite = panel.LoadImage(fileInfo[i].ToString() + "/SaveScreenShot.png");
                    savePanels.Add(panel.gameObject);
                    panelHeight -= 320;
                }
                else
                {
                    Destroy(panel.gameObject);
                }
              
            }
        }
    }


    private void OnDisable()
    {
        foreach (var panel in savePanels)
        {
            Destroy(panel.gameObject);
        }
        savePanels.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
