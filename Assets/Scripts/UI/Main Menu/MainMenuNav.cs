﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.EventSystems;

public class MainMenuNav : MonoBehaviour
{
    public GameObject mainMenu;
    public List<GameObject> menus = new List<GameObject>();
    private GameObject currentSelectedButton;
    private GameObject firstSelected;
    private GameObject currentMenu;
    private Player input;
    public EventSystem eventSystem;


    // Start is called before the first frame update
    void Start()
    {
        firstSelected = eventSystem.firstSelectedGameObject;
        currentMenu = mainMenu;
        input = ReInput.players.GetPlayer(0);
    }

    // Update is called once per frame
    void Update()
    {

        if (input.GetAxis("CursorX") != 0 || input.GetAxis("CursorY") != 0)
        {
            eventSystem.SetSelectedGameObject(null);
        }

        if (input.GetButtonDown("UIVertical") || input.GetNegativeButtonDown("UIVertical") || input.GetNegativeButtonDown("UIHorizontal") || input.GetButtonDown("UIHorizontal"))
        {
            if (eventSystem.currentSelectedGameObject == null)
            {
                eventSystem.SetSelectedGameObject(firstSelected);
            }
        }

        foreach (var menu in menus)
        {
            if (menu.activeInHierarchy)
            {
                currentMenu = menu;
            }
        }

        if (input.GetButtonDown("UICancel"))
        {
            currentMenu.SetActive(false);
            mainMenu.SetActive(true);
            eventSystem.SetSelectedGameObject(firstSelected);
        }
    }

    public void SetCurrentSelectedButton(GameObject button)
    {
        eventSystem.SetSelectedGameObject(button);
        firstSelected = button;
    }

}
