﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.IO;




public class CustomSongs : MonoBehaviour
{
    [HideInInspector]
    public List<AudioClip> songs = new List<AudioClip>();
    public List<float> songLength = new List<float>();
    public string musicFolderName;

    private void Start()
    {
#if UNITY_WINDOWS || UNITY_EDITOR
        LoadMusic();
#endif
    }

    void LoadMusic()
    {
        if (!Directory.Exists(Application.persistentDataPath + musicFolderName))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + musicFolderName);
        }

        var info = Directory.GetFiles(Application.persistentDataPath + "/" + musicFolderName + "/");

        foreach (var file in info)
        {
            if (Path.GetExtension(file) == ".wav" || Path.GetExtension(file) == ".ogg")
            {
                songs.Add(LoadClip(file, Path.GetFileName(file)));
            }

        }
    }
    AudioClip LoadClip(string path, string songName)
    {
        //Get file path
        string file = path;

        //load file bytes
        byte[] wav = File.ReadAllBytes(file);

        //get channels
        int channels = wav[22];

        //get past all the other subchunks to get to the data chunks
        int pos = 12; //first subchunkID from 12 to 16

        //keep iterating until we find the data chunk
        while (!(wav[pos] == 100 && wav[pos + 1] == 97) && wav[pos + 2] == 116 && wav[pos + 3] == 97)
        {
            pos += 4;
            int chuckSize = wav[pos] + wav[pos + 1] * 256 + wav[pos + 2] * 65536 + wav[pos + 3] * 16777216;
            pos += 4 + chuckSize;
        }

        pos += 8;

        //(wav.Length - pos) / 8;

        //pos is now positioned to the start of actual sound data
        int samples = wav.Length - pos;

        int bytesPerSample = 16 / 8; //16 bit mono sound
        int bytesPerFrame = bytesPerSample * channels; //bytes per frame

        byte[] audioData = new byte[samples]; //create audio byte data array

        Array.Copy(wav, pos, audioData, 0, samples); //copy over data chunk 

        float[] audioSamples = BytesToFloats(audioData); //get audio samples
        int frequency = BitConverter.ToInt32(wav, 24); //get the frequency of the audio being loaded
        float songlength = wav.Length / (frequency * channels * bytesPerSample); //calculate the song length
        songLength.Add(songlength - 5);

        AudioClip clip = AudioClip.Create(songName, samples, channels, frequency, true, false); //create the clip.
        clip.SetData(audioSamples, 0); //pass in the data.


        //songLength.Add(clip.length);


        return clip;
    }

    private static float[] Int16ToFloats(Int16[] array)
    {
        float[] floatArray = new float[array.Length];
        for (int i = 0; i < floatArray.Length; i++)
        {
            floatArray[i] = ((float)array[i] / short.MaxValue);
        }
        return floatArray;
    }

    private static float[] BytesToFloats(byte[] array)
    {
        //1 Short = 2 bytes, so divide by 2
        float[] floatArray = new float[array.Length / 2];
        //Iterate through and populate array
        for (int i = 0; i < floatArray.Length; i++)
        {
            //Convert each set of 2 bytes into a short and scale that to be in the range of -1, 1
            floatArray[i] = (BitConverter.ToInt16(array, i * 2) / (float)(short.MaxValue));
        }
        return floatArray;
    }
}
