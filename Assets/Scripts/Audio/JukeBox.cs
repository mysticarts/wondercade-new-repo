﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Audio;
using System.IO;

public class JukeBox : Item
{

    public AudioSource audioSource;
    public CustomSongs songsLibrary;
    private List<AudioClip> customSongs = new List<AudioClip>();
    public List<AudioClip> defaultSongs = new List<AudioClip>();
    public List<float> songLength = new List<float>();
    private int currentTrack = 0;
  //  public string musicFolderName;
    float audioPosition = 0;
    
   // public int frequency = 0;

    // Start is called before the first frame update
    void Start()
    {
        songsLibrary = FindObjectOfType<CustomSongs>();
        if (songsLibrary != null)
        {
            customSongs = songsLibrary.songs;
            songLength = songsLibrary.songLength;
            if (customSongs.Count > 0)
            {
                audioPosition = songLength[currentTrack];
            }
        }
       

    }

    // Update is called once per frame
    void Update()
    {
        audioSource.pitch = Time.timeScale;
        if (customSongs.Count > 0 && placed == true)
        {
            audioPosition += Time.deltaTime % 60;
            if (audioPosition >= songLength[currentTrack])
            {
                currentTrack++;
                audioPosition = 0;
                if (currentTrack == customSongs.Count)
                {
                    currentTrack = 0;
                }
                audioSource.clip = customSongs[currentTrack];
                audioSource.Play();
            }
        }
        if (customSongs.Count == 0 && placed == true)
        {
            if (!audioSource.isPlaying)
            {
                currentTrack++;
                if (currentTrack == defaultSongs.Count)
                {
                    currentTrack = 0;
                }
                audioSource.clip = defaultSongs[currentTrack];
                audioSource.Play();
            }
        }
    }


}
