﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnManager : MonoBehaviour {

    public AgarManager manager;
    public PlayerController_Agar player;
    public GameObject foodPrefab;
    public GameObject enemyPrefab;

    public List<GameObject> foods = new List<GameObject>();
    public List<GameObject> enemies = new List<GameObject>();

    public float minEnemyRadius = 0.1f;
    public float maxEnemyRadius = 10;
    public int maxEnemies = 10;
    public int maxFood = 10;

    public float safetyRadius = 5;
    public float spawnRadius = 15;

    public void Begin() {
        //initial spawning
        for(int count = 0; count < maxEnemies; count++)
            SpawnEnemy();
        for(int count = 0; count < maxFood; count++)
            SpawnFood();
    }

    void Update() {
        if(manager.gameState == GameState.GAME) {
            if(Random.Range(0, 100) < 20) //20% chance of spawn food
                SpawnFood();
            if(Random.Range(0, 100) < 2) //2% chance of spawn enemy
                SpawnEnemy();
        }
    }

    public void SpawnFood() {
        GameObject food = (GameObject) Instantiate(foodPrefab, GetRandomPosition(1), Quaternion.identity);
        if(!SceneManager.GetActiveScene().name.Contains("3D")) food.transform.Rotate(new Vector3(90, 0, 0), Space.World);
        foods.Add(food);
    }

    public void SpawnEnemy() {
        float randomRadius = Random.Range(minEnemyRadius, maxEnemyRadius + (player.radius/2));
        GameObject enemy = (GameObject) Instantiate(enemyPrefab, GetRandomPosition(2), Quaternion.identity);
        
        enemy.GetComponent<Enemy>().radius = randomRadius;

        enemy.transform.localScale = new Vector3(randomRadius, randomRadius, randomRadius);
        if(!SceneManager.GetActiveScene().name.Contains("3D")) enemy.transform.Rotate(new Vector3(90, 0, 0), Space.World);
        if(SceneManager.GetActiveScene().name.Contains("3D")) enemy.transform.Rotate(new Vector3(0, 180, 0), Space.World);

        enemies.Add(enemy);
    }

    public Vector3 GetRandomPosition(int depth) {
        float randomPosX;
        float randomPosY;

        float radius = safetyRadius;
        if(depth == 1) 
            radius /= 3;
        if(Random.Range(0, 2) == 0)
            randomPosX = Random.Range(radius, spawnRadius);
        else randomPosX = Random.Range(-radius, -spawnRadius);

        if(Random.Range(0, 2) == 0)
            randomPosY = Random.Range(radius, spawnRadius);
        else randomPosY = Random.Range(-radius, -spawnRadius);

        return new Vector3(player.transform.position.x + randomPosX, player.transform.position.y + randomPosY, (-0.1f * depth));
    }

}