﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class AgarManager : MiniGameManager {

    public GameObject scoreText;
    public GameObject currentMenu;
    public SpawnManager spawnManager;

    public GameObject[] mainMenuButtons;
    public GameObject cursor;
    public EventSystem eventSystem;

    public PlayerController_Agar player;

    public int score = 0;

    public void Start() {
        base.Start();
    }

    public override void Intro() {
        input.controllers.maps.SetMapsEnabled(true, "MenuNav");
        gameState = GameState.MAINMENU;
    }

    public void ExitMenu() {
        currentMenu.gameObject.SetActive(false);
        Play();
    }
    public void EnterMenu() {
        player.transform.localScale = titlePlayerTransform.transform.localScale;
        player.transform.position = titlePlayerTransform.transform.position;
        player.transform.rotation = titlePlayerTransform.transform.rotation;

        gameState = GameState.MAINMENU;
        scoreText.SetActive(false);
        currentMenu.gameObject.SetActive(true);
        currentMenu.transform.Find("Title").gameObject.SetActive(false);
        currentMenu.transform.Find("GameOver").gameObject.SetActive(true);
        currentMenu.transform.Find("GameOver").Find("ScoreEnd").GetComponent<TextMeshProUGUI>().text = "Score: " + score;
        cursor.SetActive(true);
        input.controllers.maps.SetMapsEnabled(true, "MenuNav");
        input.controllers.maps.SetMapsEnabled(false, "Default");
        eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject);

        //reset game
        score = 0;
        player.radius = 0.5f;
        List<GameObject> foodCopy = new List<GameObject>(spawnManager.foods);
        List<GameObject> enemiesCopy = new List<GameObject>(spawnManager.enemies);
        spawnManager.enemies.Clear();
        spawnManager.foods.Clear();
        foreach(GameObject food in foodCopy) {
            Destroy(food);
        }
        foreach(GameObject enemy in enemiesCopy) {
            Destroy(enemy);
        }
        player.transform.position = new Vector3(0,0,0);
        Camera.main.transform.position = new Vector3(0,0,-10);
    }

    public void Update() {
        if(eventSystem.currentSelectedGameObject != null)
            cursor.transform.position = new Vector3(-1.28f, eventSystem.currentSelectedGameObject.transform.position.y, -2.14f);

        switch (gameState) {
            case GameState.SPLASHSCREEN:
                break;
            case GameState.MAINMENU:
                MenuNavigation();
                break;
            case GameState.ENDGAME:
                MenuNavigation();
                break;
        }

        if(input.GetButtonDown("Exit")) {
            if(gameState == GameState.GAME) EnterMenu();
            if(gameState == GameState.MAINMENU) ExitGame();
        }
    }

    public GameObject titlePlayerTransform;
    public void Play() {
        gameState = GameState.GAME;
        cursor.SetActive(false);
        input.controllers.maps.SetMapsEnabled(false, "MenuNav");
        input.controllers.maps.SetMapsEnabled(true, "Default");
        spawnManager.Begin();
        scoreText.SetActive(true);
        scoreText.GetComponent<Text>().text = "Score: " + score;

        player.transform.localScale = new Vector3(player.radius, player.radius, player.radius);
    }

    private int Iter;
    public void MenuNavigation() {
        Button[] buttons = currentMenu.GetComponentsInChildren<Button>();
        int buttonCount = buttons.Length - 1;
        if(input.GetButtonDown("UIVert"))
            Iter--;
        if(input.GetNegativeButtonDown("UIVert"))
            Iter++;
        Iter = Mathf.Clamp(Iter, 0, buttonCount);

        eventSystem.SetSelectedGameObject(buttons[Iter].gameObject);
        cursor.transform.position = new Vector3(-1.28f, buttons[Iter].transform.position.y, -2.14f);

    }

    public override void ExitGame() {
        base.ExitGame();
    }

}
