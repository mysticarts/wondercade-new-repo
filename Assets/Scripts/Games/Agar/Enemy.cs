﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour {

    public AgarManager manager;
    public SpawnManager spawnManager;
    public PlayerController_Agar player;

    public float speed = 1;
    public float radius = 1;

    public void Update() {

        GameObject nearestEnemy = player.GetNearestEnemy();
        if(Vector3.Distance(player.transform.position, this.gameObject.transform.position) < (player.fleeAttackRadius+(radius/2))) {
            if(player.radius <= radius) {
                Attack();
                if(GameObject.ReferenceEquals(nearestEnemy, this.gameObject)) {
                    if(Vector3.Distance(player.transform.position, this.gameObject.transform.position) < (player.veryWowRadius+(radius/2)))
                        player.SetState(PlayerState.VERY_WOW);
                    else player.SetState(PlayerState.WOW);
                }
            } else {
                Flee();
                if(GameObject.ReferenceEquals(nearestEnemy, this.gameObject) && Vector3.Distance(player.transform.position, this.gameObject.transform.position) < (player.chompRadius+(radius/2)))
                    player.SetState(PlayerState.CHOMP);
            }
        } else { 
            Wander();
            
            GameObject nearestFood = player.GetNearestFood();
            if(nearestFood != null && Vector3.Distance(player.transform.position, nearestFood.transform.position) < ((player.radius/2) + player.chompRadius))
                player.SetState(PlayerState.CHOMP);
            else if(GameObject.ReferenceEquals(nearestEnemy, this.gameObject)) {
                int random = Random.Range(0,1000);
                if(random < 10) {
                    player.SetState(PlayerState.BLINK);
                } else player.SetState(PlayerState.NORMAL);
            }
        }

    }

    public GameObject GetNearestFood() {
        GameObject nearestFood = null;

        float nearestFoodDistance = float.MaxValue;
        foreach(GameObject food in spawnManager.foods) {
            float distance = Vector3.Distance(food.transform.position, this.gameObject.transform.position);
            if(distance < nearestFoodDistance) {
                nearestFoodDistance = distance;
                nearestFood = food;
            }
        }

        return nearestFood;
    }

    public void Attack() {
        speed = 2;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z), speed * Time.deltaTime);
        
		if(!SceneManager.GetActiveScene().name.Contains("3D")) {
			float distance = Vector3.Distance(transform.position, player.transform.position);
			float distanceBetween = distance - (radius/2) - (player.radius/2);
			if(distanceBetween <= -(player.radius/1.5f)) {
				manager.EnterMenu();
			}
		}
    }

	public void OnTriggerEnter(Collider other) {
        if(SceneManager.GetActiveScene().name.Contains("3D")) {
		    if(other.CompareTag("Player")) {

                if(radius <= player.radius) {
                    //update radius
                    player.radius = player.radius + (radius / 10);
                    player.transform.localScale = new Vector3(player.radius, player.radius + (radius / 10), player.radius);

                    spawnManager.enemies.Remove(gameObject);
                    Destroy(gameObject);

                    player.AddScore(Mathf.RoundToInt(radius * 3.33f));
                } else
			        manager.EnterMenu();
		    }
        }
	}

    public void Flee() {
        speed = 2;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z), -speed * Time.deltaTime);
    }

    public void Wander() {
        speed = 1;
        GameObject nearestFood = GetNearestFood();
        if(nearestFood != null) {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(nearestFood.transform.position.x, nearestFood.transform.position.y, transform.position.z), speed * Time.deltaTime);
            float distance = Vector3.Distance(transform.position, nearestFood.transform.position);
            if(distance < (radius/2)) {
                //update radius
                radius = radius + 0.1f;
                transform.localScale = new Vector3(radius, radius, radius);

                spawnManager.foods.Remove(nearestFood);
                Destroy(nearestFood);
            }
        }
    }
}
