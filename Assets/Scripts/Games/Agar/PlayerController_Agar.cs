﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum PlayerState {
    NORMAL,
    WOW,
    VERY_WOW,
    CHOMP,
    BLINK
};

public class PlayerController_Agar : PlayerController {

    public SpawnManager spawnManager;
    public AgarManager manager;

    public float fleeAttackRadius = 5;
    public float radius = 0.5f;
    public float speed = 5;
    public float veryWowRadius = 2.5f;
    public float chompRadius = 1.5f;
    public GameObject mouth;
    public GameObject eyeball;
    public Vector3 normalMouthSize;
    public Vector3 normalEyeSize;
    public Vector3 wowMouthSize;
    public Vector3 wowEyeSize;
    public Vector3 veryWowMouthSize;
    public Vector3 veryWowEyeSize;
    public Vector3 chompMouthSize;
    public Vector3 blinkEyeSize;

    private bool isBlinking = false;

    public void SetState(PlayerState state) {
        if (!isBlinking) { //if isn't blinking
            if (state == PlayerState.NORMAL) {
                mouth.transform.localScale = normalMouthSize;
                eyeball.transform.localScale = normalEyeSize;
            }
            if (state == PlayerState.WOW) {
                mouth.transform.localScale = wowMouthSize;
                eyeball.transform.localScale = wowEyeSize;
            }
            if (state == PlayerState.VERY_WOW) {
                mouth.transform.localScale = veryWowMouthSize;
                eyeball.transform.localScale = veryWowEyeSize;
            }
            if (state == PlayerState.CHOMP) {
                mouth.transform.localScale = chompMouthSize;
                eyeball.transform.localScale = normalEyeSize;
            }

            if (state == PlayerState.BLINK) {
                isBlinking = true;
                mouth.transform.localScale = normalMouthSize;
                eyeball.transform.localScale = blinkEyeSize;
                StartCoroutine(Blink());
            }
        }
    }

    private IEnumerator Blink() {
        yield return new WaitForSecondsRealtime(0.2f);
        isBlinking = false;
    }

    override public void Initialise() {
        input.controllers.maps.SetMapsEnabled(true, "Default");
    }

    public void Start() {
        base.Start();
        SetState(PlayerState.NORMAL);
    }

    public void Update() {
        if (manager.gameState == GameState.GAME) {

            Vector3 direction = Vector3.zero;

            if (input.GetButton("Vertical")) direction += Vector3.up;
            if (input.GetNegativeButton("Vertical")) direction += Vector3.down;

            if (input.GetNegativeButton("Horizontal")) {
                if(SceneManager.GetActiveScene().name.Contains("3D"))
                    transform.rotation = Quaternion.Euler(new Vector3(180, 90, 180));
                else transform.rotation = Quaternion.Euler(new Vector3(90, 180, 0));
                direction += Vector3.left;
            }
            if (input.GetButton("Horizontal")) {
                if(SceneManager.GetActiveScene().name.Contains("3D"))
                    transform.rotation = Quaternion.Euler(new Vector3(180, -90, 180));
                else transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
                direction += Vector3.right;
            }

            transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Camera.main.transform.position + direction, speed * Time.deltaTime);

            //eat food:
            if (!SceneManager.GetActiveScene().name.Contains("3D")) {
                GameObject nearestFood = GetNearestFood();
                if (nearestFood != null) {
                    float distance = Vector3.Distance(transform.position, nearestFood.transform.position);
                    if (distance < (radius / 2)) {
                        //update radius
                        radius = radius + 0.01f;

                        if (SceneManager.GetActiveScene().name.Contains("3D"))
                            transform.localScale = new Vector3(radius, radius, radius);
                        else transform.localScale = new Vector3(radius, 0.01f, radius);

                        spawnManager.foods.Remove(nearestFood);
                        Destroy(nearestFood);

                        AddScore(1);
                    }
                }
            }

            //eat enemies:
            GameObject nearestEnemy = GetNearestEnemy();
            if (nearestEnemy != null) {
                float distance = Vector3.Distance(transform.position, nearestEnemy.transform.position);
                if (radius > nearestEnemy.GetComponent<Enemy>().radius && distance < (radius / 2)) {
                    //update radius
                    radius = radius + (nearestEnemy.GetComponent<Enemy>().radius / 10);
                    transform.localScale = new Vector3(radius, radius + (nearestEnemy.GetComponent<Enemy>().radius / 10), radius);

                    spawnManager.enemies.Remove(nearestEnemy);
                    Destroy(nearestEnemy);

                    AddScore(Mathf.RoundToInt(nearestEnemy.GetComponent<Enemy>().radius * 3.33f));
                }
            }

        }
    }

    public void OnTriggerEnter(Collider other) {
        if(SceneManager.GetActiveScene().name.Contains("3D")) {
            GameObject nearestFood = GetNearestFood();
            if(nearestFood != null) {
                if(other.CompareTag("Food")) {
                    //update radius
                    radius = radius + 0.01f;

                    transform.localScale = new Vector3(radius, radius, radius);

                    spawnManager.foods.Remove(nearestFood);
                    Destroy(nearestFood);

                    AddScore(1);
                }
            }
        }
    }

    public Text scoreText;
    public void AddScore(int score) {
        manager.score += score;

        scoreText.text = "Score: " + manager.score;
    }

    public GameObject GetNearestEnemy() {
        GameObject nearestEnemy = null;

        float nearestEnemyDistance = float.MaxValue;
        foreach (GameObject enemy in spawnManager.enemies) {
            float distance = Vector3.Distance(enemy.transform.position, transform.position);
            if (distance < nearestEnemyDistance) {
                nearestEnemyDistance = distance;
                nearestEnemy = enemy;
            }
        }

        return nearestEnemy;
    }

    public GameObject GetNearestFood() {
        GameObject nearestFood = null;

        float nearestFoodDistance = float.MaxValue;
        foreach (GameObject food in spawnManager.foods) {
            float distance = Vector3.Distance(food.transform.position, this.gameObject.transform.position);
            if (distance < nearestFoodDistance) {
                nearestFoodDistance = distance;
                nearestFood = food;
            }
        }

        return nearestFood;
    }

}
