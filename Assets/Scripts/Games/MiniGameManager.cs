﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

    public enum GameState 
    {
        SPLASHSCREEN = 0,
        MAINMENU = 1,
        GAME = 2,
        ENDGAME = 3,
    }

public class MiniGameManager : MonoBehaviour
{

    protected Player input;
    private AsyncOperation asyncOperation;
    //public string gameName = "";

    public void Start() {
        input = ReInput.players.GetPlayer(0);
        Intro();
        asyncOperation = SceneManager.LoadSceneAsync("James B New");
        asyncOperation.allowSceneActivation = false;
    }

    public GameState gameState;

    virtual public void Intro() { }

    virtual public void ExitGame() {
         asyncOperation.allowSceneActivation = true;
    }

}
