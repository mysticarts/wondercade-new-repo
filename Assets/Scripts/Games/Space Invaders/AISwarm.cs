﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISwarm : MonoBehaviour
{

    [HideInInspector]
    public List<AIController> AI = new List<AIController>();
    [HideInInspector]
    public List<Invaders_Movement> AIMovement = new List<Invaders_Movement>();
    [HideInInspector]
    public bool AIKilled = false;
    public GameObject bullet;
    public float fireRate = 0;
    private float currentFireDelay = 0;
    private InvadersManager gameManager;

    //private Transform


    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<InvadersManager>();
    }

   // Update is called once per frame
    void Update()
    {

        if (AI.Count == 0)
        {
            if (gameManager.currentSave != null)
            {
                if (!gameObject.scene.name.Contains("3D"))
                {
                    gameManager.currentSave.beatenSpaceInvaders2D = true;
                }
                else
                {
                    gameManager.currentSave.beatenSpaceInvaders3D = true;
                }
            }

            gameManager.audioSource.clip = gameManager.winSong;
            gameManager.audioSource.Play();
            EndGame();
           
        }

        for (int i = 0; i < AI.Count; i++)
        {
            if (AI[i] != null)
            {
                if (AIKilled)
                {
                    AIMovement[i].ResetSpeed(AIMovement[i].movementSpeed/2);
                }               
                
                if (AI[i].transform.position.y >= 3.4f)
                {
                    gameManager.EndGame();
                    break;
                }
            }
        }
        if (AIKilled)
        {
            AIKilled = false;
        }

        currentFireDelay += Time.deltaTime * 2;
        if (currentFireDelay >= fireRate && AI.Count > 0)
        {
            Shoot();
        }

    }

    public void EndGame()
    {
        foreach (var ai in AI)
        {
            ai.gameObject.SetActive(false);
        }
        AI.Clear();
        AIMovement.Clear();
        gameManager.EndGame();
        enabled = false;
    }

    void Shoot()
    {
        int random = Random.Range(0, AIMovement.Count);

        AIController chosenAI = AI[random];

        RaycastHit hit;

        if (chosenAI != null)
        {
            if (Physics.Raycast(chosenAI.transform.position, Vector3.up, out hit, 100))
            {
                if (hit.transform.tag != "Enemy")
                {
                    Vector3 bulletRotation = Vector3.zero;

                    if (!gameObject.scene.name.Contains("3D"))
                    {
                        bulletRotation = chosenAI.transform.rotation.eulerAngles;

                    }




                    Destroy(Instantiate(bullet, chosenAI.transform.position + Vector3.up, Quaternion.Euler(bulletRotation)), 5);
                    currentFireDelay = 0;
                }
            }
        }

        
    }

    public void OnDeath(AIController ai)
    {
        int iter = AI.IndexOf(ai);

        if (iter > -1 && iter < AI.Count)
        {
            AI.RemoveAt(iter);
            AIMovement.RemoveAt(iter);
            AIKilled = true;
        }


    }




}
