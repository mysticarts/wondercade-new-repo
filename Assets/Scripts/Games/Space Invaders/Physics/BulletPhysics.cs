﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPhysics : MonoBehaviour
{
    public int scoreReward = 0;
    public float bulletSpeed = 0;
    public GameObject explosionImage;

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.up * bulletSpeed;
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        InvadersManager gameManager = FindObjectOfType<InvadersManager>();
        AISwarm swarm = FindObjectOfType<AISwarm>();

        if (collision.transform.tag == "Enemy" || collision.transform.tag == "Bullet")
        {
            Destroy(Instantiate(explosionImage, collision.transform.position - new Vector3(0,0, 3.80246f), Quaternion.Euler(0, 0, 0)), 0.5f);
        }

        if (collision.gameObject.tag == "Enemy")
        {
            AIController controller = collision.transform.GetComponent<AIController>();
            swarm.OnDeath(controller);
            gameManager.score += scoreReward;
        }


        if (collision.transform.tag == "Player")
        {
           gameManager.audioSource.clip = gameManager.loseSong;
            gameManager.audioSource.Play();
            gameManager.EndGame();
        }


        if (collision.transform.tag != "Player")
        {
            Destroy(collision.gameObject);
        }

        Destroy(gameObject);
    }
}
