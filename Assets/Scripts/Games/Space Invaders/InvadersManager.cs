﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class InvadersManager : MiniGameManager
{
    public PlayerController_SpaceInvaders player;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI finalScoreText;
    public GameObject returnToMenuButton;
    public GameObject gamerOverMenu;
    public EventSystem eventSystem;
    public GameObject cursor;
    public GameObject titleMenu;
    public GameObject game;
    private GameObject currentGame;
    public AISwarm swarm;
    [HideInInspector]
    public CurrentSave currentSave;
    [HideInInspector]
    public int score = 0;
    [HideInInspector]
    public AudioSource audioSource;
    public AudioClip winSong;
    public AudioClip loseSong;

    private void Start()
    {
        eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject);
        base.Start();
        input.controllers.maps.SetMapsEnabled(true, "UI");
        currentSave = FindObjectOfType<CurrentSave>();
        audioSource = GetComponent<AudioSource>();
      //  Debug.Log(Vector3.Distance(cursor.transform.position, eventSystem.currentSelectedGameObject.transform.position));

    }

    // Update is called once per frame
    void Update()
    {
        if (cursor.activeInHierarchy)
        {
            if (eventSystem.currentSelectedGameObject != null)
            {
                cursor.transform.position = eventSystem.currentSelectedGameObject.transform.position - new Vector3(1.335f, 0, 0);
            }
        }


        switch (gameState)
        {
            case GameState.SPLASHSCREEN:
                break;
            case GameState.MAINMENU:
                //MenuNavigation();
               
                break;
            case GameState.GAME:
                scoreText.text = "Score: " + score;             
                
                if (input.GetButtonDown("LeaveGame"))
                {
                    audioSource.clip = loseSong;
                    audioSource.Play();
                    EndGame();
                }


                break;

            case GameState.ENDGAME:
                eventSystem.SetSelectedGameObject(returnToMenuButton);

               

                break;
        }
    }

    public void EndGame()
    {
        input.controllers.maps.SetMapsEnabled(true, "UI");
        input.controllers.maps.SetMapsEnabled(false, "Invaders");
        player.gameObject.SetActive(false);
        scoreText.gameObject.SetActive(false);
        gameState = GameState.ENDGAME;
        if (swarm.AI.Count > 0)
        {
            swarm.EndGame();
        }
        finalScoreText.text = "Final Score: " + score;
        gamerOverMenu.SetActive(true);
        cursor.SetActive(true);
    }

    public void ReturnToMenu()
    {
        Destroy(currentGame);
        gamerOverMenu.SetActive(false);
        titleMenu.SetActive(true);
        eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject);
        gameState = GameState.MAINMENU;
    }


    public void StartGame()
    {
        gameState = GameState.GAME;
        score = 0;
        scoreText.gameObject.SetActive(true);
        titleMenu.SetActive(false);
        input.controllers.maps.SetMapsEnabled(false, "UI");
        input.controllers.maps.SetMapsEnabled(true, "Invaders");
        swarm.enabled = true;
        cursor.SetActive(false);
        currentGame = Instantiate(game);
        player = currentGame.GetComponentInChildren<PlayerController_SpaceInvaders>();
    }

}
