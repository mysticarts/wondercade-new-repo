﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Rewired.ControllerExtensions;

public class PuckMovement : MonoBehaviour
{

    public Vector3 velocity;
    public float horizontalSpeed;
    public float verticalSpeed;
    private float spawnSpeed = 0;
    public Rigidbody puckRigidBody;
    private Vector3 previousPos;
    private Vector3 currentPos;
    [HideInInspector]
    public Vector3 spawnPos;
    public GameObject middleLine;
    private List<PlayController_Pong> players;
    public float direction = 0;
    public bool respawnDelay = false;
    public float respawnTime = 0;
    private AudioSource audioSource;
    public AudioClip scoreClip;
    public AudioClip collisionClipPlayer;
    public AudioClip collisionClipWall;


    // Start is called before the first frame update
    void Start()
    {
        spawnPos = transform.position;
        spawnSpeed = verticalSpeed;
        Debug.Log(Vector3.Distance(spawnPos, GameObject.Find("TopEdge").transform.position));
        puckRigidBody = GetComponent<Rigidbody>();
        puckRigidBody.velocity = new Vector3(-horizontalSpeed, -verticalSpeed, 0);
        players = new List<PlayController_Pong>(FindObjectsOfType<PlayController_Pong>());
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {        
        puckRigidBody.velocity = new Vector3(horizontalSpeed, verticalSpeed, 0);

        direction = Vector3.Dot(puckRigidBody.velocity, middleLine.transform.right);

        if (respawnDelay)
        {
            puckRigidBody.angularVelocity = new Vector3(0,0,0);
            respawnTime += Time.deltaTime % 60;

            if (respawnTime > 4)
            {
                Respawn();
            }
        }

    }



    public void Respawn()
    {
        
        if (verticalSpeed == 0 && horizontalSpeed == 0)
        {
            int[] directions = new int[2] { -1, 1 };
            int direction = Random.Range(directions[0], directions[1]);
            verticalSpeed = spawnSpeed * direction;
            horizontalSpeed = spawnSpeed * direction;
            if (verticalSpeed != 0 && horizontalSpeed != 0)
            {
                respawnTime = 0;
                respawnDelay = false;
            }

        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "Middle Line")
        {
            audioSource.clip = scoreClip;
            audioSource.Play();
            if (other.name == "EndField Left")
            {
                players[0].score++;
            }
            if (other.name == "EndField Right")
            {
                players[1].score++;
            }
            float randOffSet = Random.Range(-3.5f, 3.5f);
            transform.position = spawnPos + new Vector3(0, randOffSet, 0);
            respawnDelay = true;
           // spawnSpeed = verticalSpeed;
            verticalSpeed = 0;
            horizontalSpeed = 0;
        }       
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            audioSource.clip = collisionClipPlayer;
            horizontalSpeed *= -1;
          //  player = collision.gameObject.GetComponent<PlayController_Pong>();
        }
        else
        {
            audioSource.clip = collisionClipWall;
            verticalSpeed *= -1;
        }

        //only do it if its the updated graphics.
        if (puckRigidBody.gameObject.scene.name.Contains("3D"))
        {
            Debug.Log("add torque");
            puckRigidBody.AddTorque(new Vector3(puckRigidBody.velocity.x, puckRigidBody.velocity.y, 0) * 20, ForceMode.Impulse);
        }

        audioSource.Play();
    }

   
}
