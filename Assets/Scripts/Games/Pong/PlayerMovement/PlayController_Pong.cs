﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using TMPro;

public class PlayController_Pong : MonoBehaviour
{

    public Player input;
    public TextMeshPro scoreText;
    [HideInInspector]
    public int score;
    public int playerID = 0;
    public float movementSpeed = 0;
    private float currentSpeed = 0;
    public Transform[] bounds;
    public bool inputEnabled = false;



    public void Start()
    {
        //input = ReInput.players.GetPlayer(playerID);
        //input.controllers.maps.SetMapsEnabled(true, "Pong");


    }

    public void EnableInput(bool enable)
    {
        if (input == null)
        {
            input = ReInput.players.GetPlayer(playerID);
        }
        input.controllers.maps.SetMapsEnabled(enable, "Pong");
    }


    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (!Physics.Raycast(transform.position + new Vector3(0, transform.localScale.y * 0.5f, 0), Vector3.up, 0.1f))
        {
            if (input.GetButton("MoveVertical"))
            {
                Movement(movementSpeed, 1);
            }
        }
        if (!Physics.Raycast(transform.position - new Vector3(0, transform.localScale.y * 0.5f, 0), -Vector3.up, 0.1f))
        {
            if (input.GetNegativeButton("MoveVertical"))
            {
                Movement(movementSpeed, -1);
            }
        }


        scoreText.text = score.ToString();
    }
    public Vector3 Movement(float speed, float direction)
    {
       
        currentSpeed = movementSpeed;
        Vector3 velocity = new Vector3(0, movementSpeed * Time.deltaTime, 0);
        return transform.position += velocity * direction;
        
     
    }


 
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Top" || collision.gameObject.tag == "Bottom")
        {
            currentSpeed = 0;
        }
        
    }
   

}
