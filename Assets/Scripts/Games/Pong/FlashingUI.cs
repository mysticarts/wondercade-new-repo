﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingUI : MonoBehaviour
{
    public List<GameObject> UI = new List<GameObject>();
    public float flashTime = 0;
    private float currentTime = 0;
    private bool active = false;

    // Update is called once per frame
    void Update()
    {
        if (currentTime <= flashTime && active)
        {
            currentTime += Time.deltaTime * 1;
        }
        if (currentTime >= flashTime && active)
        {
            foreach (var ui in UI)
            {
                ui.SetActive(false);
            }
            active = false;
        }
        if (currentTime >= 0 && !active)
        {
            currentTime -= Time.deltaTime * 1;
        }
        if (currentTime <= 0 && !active)
        {
            foreach (var ui in UI)
            {
                ui.SetActive(true);
            }
            active = true;
        }
    }
}
