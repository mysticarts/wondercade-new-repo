﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Rewired;
using TMPro;

public class PongManager : MiniGameManager
{
    // Start is called before the first frame update
    public PlayController_Pong[] players;

    public TextMeshPro[]scoreTexts;
    public int winScore = 0;
    [Header("----Menus----")]
    public GameObject currentMenu;

    public GameObject[] mainMenuButtons;
    public GameObject cursor;

    public GameObject endGameMenu;

    private int Iter;
    public bool buttonPressed = false;

    public EventSystem eventSystem;

    public GameObject game;
    private CurrentSave currentSave;
    public GameObject mainMenu;
    public PuckMovement puck;
    private AudioSource audioSource;

    public override void Intro()
    {
        base.Intro();
        input.controllers.maps.SetMapsEnabled(true, "MenuNav");
        audioSource = GetComponent<AudioSource>();
        gameState = GameState.MAINMENU;
        currentSave = FindObjectOfType<CurrentSave>();
    }

    void Update()
    {
        //ExitGame();
        switch (gameState)
        {
            case GameState.SPLASHSCREEN:
                break;
            case GameState.MAINMENU:
                MenuNavigation();
                if (!audioSource.enabled)
                {
                    audioSource.enabled = true;
                }
                break;
            case GameState.GAME:

                for (int i = 0; i < players.Length; i++)
                {
                    if (players[i].score == winScore)
                    {
                        if (i == 1 && players[1].GetComponent<AIController>().enabled)
                        {
                            currentSave.beatenPongAI = true; 
                        }


                        EndGame();
                    }
                }

                if (input.GetButtonDown("LeaveGame"))
                {
                    EndGame();
                }



                break;
            case GameState.ENDGAME:
                MenuNavigation();
                break;
        }
    }
    public void ResetGame()
    {
        foreach (var player in players)
        {
            player.transform.position = new Vector3(player.transform.position.x, 2.42f, player.transform.position.z);
            player.score = 0;
        }
        gameState = GameState.MAINMENU;
    }

    public void MenuNavigation()
    {
        Button[] buttons = currentMenu.GetComponentsInChildren<Button>();
        int buttonCount = buttons.Length - 1;
        if (input.GetButtonDown("UIVert"))
        {
            Iter--;
        }
        if (input.GetNegativeButtonDown("UIVert"))
        {
            Iter++;
        }
        Iter = Mathf.Clamp(Iter, 0, buttonCount);

        eventSystem.SetSelectedGameObject(buttons[Iter].gameObject);
        cursor.transform.position = buttons[Iter].transform.position - new Vector3(0.7f, 0, 0);

    }

    public void EndGame()
    {
        GameObject.Find("Game").SetActive(false);
        endGameMenu.SetActive(true);
        currentMenu = endGameMenu;
        gameState = GameState.ENDGAME;
        cursor.SetActive(true);
        input.controllers.maps.SetMapsEnabled(true, "MenuNav");
  
        puck.respawnTime = 0;
        float randOffSet = Random.Range(-3.5f, 3.5f);
        puck.respawnDelay = true;
        puck.transform.position = puck.spawnPos + new Vector3(0, randOffSet, 0);
        puck.verticalSpeed = 0;
        puck.horizontalSpeed = 0;
        ResetGame();

    }


    public void Play(bool multiplayer)
    {
        gameState = GameState.GAME;
        cursor.SetActive(false);
        input.controllers.maps.SetMapsEnabled(false, "MenuNav");
        foreach (var scoreText in scoreTexts)
        {
            scoreText.gameObject.SetActive(true);
        }

        audioSource.enabled = false;

        if (multiplayer)
        {
            foreach(var player in players)
            {
                player.enabled = true;
                player.EnableInput(true);
                if (player.GetComponent<AIController>() != null)
                {
                    player.GetComponent<AIController>().enabled = false;
                }
            }
        }
        else
        {

            players[0].enabled = true;
            players[0].EnableInput(true);
            players[1].enabled = true;
            players[1].EnableInput(false);
            players[1].GetComponent<AIController>().enabled = true;
        }
    }


    public void MenuChange(GameObject newMenu)
    {
        currentMenu = newMenu;
        currentMenu.SetActive(true);
    }

    public override void ExitGame()
    {
        base.ExitGame();
    }
   
    // Update is called once per frame
}
