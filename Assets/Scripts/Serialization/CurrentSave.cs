﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class CurrentSave : MonoBehaviour
{
    public string selectedSave; //the file location of the selected save file.
    public Serialization serialization; //game save system, gets reference once loaded into the main scene
    public bool newGame = false; //used to check if a new game is loaded
    public string playersGender; //set in cutscene
    public string arcadesName; //set in cutscene
    [HideInInspector]
    public AsyncOperation asyncOperation; //used to load scenes in the background
    private bool loadMainGame = false; //used to check whether to load the main scene
    private CutSceneEvents cutSceneEvents; //the events that occur during cutscene (Written by designer)
    [HideInInspector]
    public int totalGamesPlayed = 0; //the total amount of mini games played (increases every time the player interacts with the cabinet)
    [HideInInspector]
    public int totalMachinesRepaired = 0; //how many cabinets the player has repaired
    [HideInInspector]
    public bool beatenPongAI = false; //player has beaten the pong ai before
    [HideInInspector]
    public bool beatenSpaceInvaders2D = false; //the player has beaten 2D space invaders before
    [HideInInspector]
    public bool beatenSpaceInvaders3D = false;//the player has beaten 3D space invaders before
    public GameObject sceneManage; //prefab, contains SceneManage component and is used to load a desired scene with the loading screen.
    public GameData ps4SaveData; //the data struct the game saves to when playing on PS4
    [HideInInspector]
    public bool loadFromMenu = false;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject); //this script must persist through out the whole game. (is only destroyed when quitting to main menu as a new one is created).
    }

    public void NewGame()
    {
        //if PC build
#if UNITY_STANDALONE_WIN
        if (!Directory.Exists(Application.persistentDataPath + "/GameSaves")) //create folder if it doesnt exist
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/GameSaves");
        }
        var info = new DirectoryInfo(Application.persistentDataPath + "/GameSaves");
        //  var fileInfo = info.GetDirectories();

        //set the selected save
        selectedSave = Application.persistentDataPath + "/GameSaves/" + "GameSave" + info.GetDirectories().Length;
        Directory.CreateDirectory(selectedSave); //create the file
#endif
        newGame = true;

        loadFromMenu = true;
    }

    /// <summary>
    /// Attached as an event on a load file button that gets generated when opening the load game menu.
    /// </summary>
    public void LoadGame(string savePath)
    {
        //load the file at the savePath
#if UNITY_STANDALONE_WIN
        selectedSave = savePath;
        selectedSave = selectedSave.Replace("/GameData.Arcade","");
#endif
        loadFromMenu = true;

    }

    public void Update()
    {     
        if (SceneManager.GetActiveScene().name == "CutSceneUpdated") //if in the cutscene
        {
            if (!loadMainGame) //if just loaded in.
            {
                //begin loading the next loading screen.
                cutSceneEvents = FindObjectOfType<CutSceneEvents>();
                Debug.Log("Begin Load");
                asyncOperation = SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
                asyncOperation.allowSceneActivation = false;
                loadMainGame = true;
            }
            
            //cutscene has ended, load loading screen to begin loading the main scene.
            if (cutSceneEvents.cutsceneEnded)
            {
                asyncOperation.allowSceneActivation = true;
                SceneManage manage = Instantiate(sceneManage).GetComponent<SceneManage>();
                manage.selectedlevel = "James B New"; //We forgot to rename the scene throughout development
            }
        }
        if (SceneManager.GetActiveScene().name == "LoadingScreen" && loadMainGame) //if main scene has begin loading.
        {
            loadMainGame = false;
        }
    }

}
