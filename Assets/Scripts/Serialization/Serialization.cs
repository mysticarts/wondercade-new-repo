﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[Serializable]
public struct GameData
{
    public PlayerData playerData; //Saved player data (position, rotation)
    public TutorialData tutorialData; //Saved tutorial data (checks which ones are complete)
    public List<AIData> AIData; //All behaviour data that gets saved (the current state of the behaviour)
    public CycleData cycleData; //the current time of day and date.
    public List<ItemData> itemsData; //all placed items
    public int PopulationCount; //the current population density
    public EconomyData economyData; //how much money the player has when saving
    public List<string> DestroyedGrids; //what walls the player has knocked down
    public string gender; //what gender the player has picked for the main character
    public string arcadeName; //the chosen arcade name
    public List<RoomData> roomData; //the current state of the room (dirty or clean)
    public List<bool> lightActive; //what lights are active in arcade.
    public ProgressionData progressionData; //what items have been unlocked
    public AdvertisingData advertisingData; //the current hints and effectiveness in each category..
    //and how much the user has invested in each category
    public ExpensesData expensesData; //the current bills the user has obtained
    public int aiInBuildingCount; //how many AI in building
    public int currentbuildingCapacity; //whats the current max capacity of the building. Increases...
    //when another room is unlocked
    public string saveTime; //the current time (local time) the game was saved.
}





public class Serialization : MonoBehaviour
{
    public float autoSaveFrequency = 0; //how often the game saves
    public float saveIconDisplayTime = 0;
    private float autoSaveTime = 0; //the current timer for the auto save (passes the autoSaveFrequency, the game will save and the timer resets)
    private float saveIconTime = 0; //the current timer for displaying the icon, resets when passing 0 threshold.
    public GameObject saveIcon; 
    [HideInInspector]
    public GameData gameData; //the data to save
    public string itemAssetsLocation = ""; //used to fetch the prefabs of the placed items.
    private string folderName = "GameSaves"; //the folder to write to.
    private string fileExtension = ".Arcade"; //files type.
    private PlayerController_Default player; //player
    private DayNightCycle cycle;
    [HideInInspector]
    public AISpawning AISpawner;
    private Economy economy;
    private BuildingManager buildingManager;
    [Tooltip("The text mesh found at the front of the building")]
    public TextMesh arcadeNameDisplay;
    public List<CleanRoom> rooms = new List<CleanRoom>(); //the mop icons that appear above the rooms in cleaning mode
    public List<GameObject> lights = new List<GameObject>(); //the lights that are in each room
    private CurrentSave currentSave; //the current save thats loaded and being written to.
    private Progression progression;
    private Tutorial tutorial;
    private AdvertisingManager advertisingManager;
    private ExpensesManager expensesManager;
    [Tooltip("The gen points that hold the data for each generated grid (grid size and where to generate from)")]
    public List<GenPointProperties> gridsGenerated = new List<GenPointProperties>();
    [Tooltip("keeps track of how many items of each type has been placed (i.e. 1/3 invaders cabinets)")]
    public MachineManager machineManager;


    private void Start()
    {
        //fetch references
        AISpawner = FindObjectOfType<AISpawning>();
        autoSaveTime = autoSaveFrequency;
        saveIconTime = saveIconDisplayTime;
        player = FindObjectOfType<PlayerController_Default>();
        cycle = FindObjectOfType<DayNightCycle>();
        economy = FindObjectOfType<Economy>();
        buildingManager = FindObjectOfType<BuildingManager>();
        currentSave = FindObjectOfType<CurrentSave>();
        tutorial = GetComponent<Tutorial>();
        progression = GetComponent<Progression>();
        advertisingManager = GetComponent<AdvertisingManager>();
        expensesManager = GetComponent<ExpensesManager>();
        currentSave.serialization = this;
        machineManager = GetComponent<MachineManager>();
        if (currentSave.newGame) //if loading a new game
        {
            advertisingManager.CalculateNewEffectiveness(); //calculate default advertising effectiveness
            expensesManager.NewGame(); //set default bills.
            player.SetGender(currentSave.playersGender); //set the players model based on the gender selected
            arcadeNameDisplay.text = currentSave.arcadesName; //set arcades name.
            currentSave.newGame = false; //no longer a new game
            machineManager.AddDefaultItems(); //add items to the shop
            SaveGame(); //Save game (in case the player leaves strait after cutscene they can load back in from the starting state)
        }
        else
        {
            LoadGame(); //load game from loaded file.
        }

        currentSave.loadFromMenu = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (saveIcon != null)
        {
            if (autoSaveTime > 0) //take time
            {
                autoSaveTime -= Time.unscaledDeltaTime % 60;
            }
            if (autoSaveTime <= 0) //if auto save timer passes threshold
            {
                if (saveIconTime > 0)
                {
                    if (!saveIcon.activeInHierarchy) //icon isnt already active
                    {
                        //display and save
                        saveIcon.SetActive(true);
                        SaveGame();
                    }
                    saveIconTime -= Time.unscaledDeltaTime % 60; //tick down
                }
                else
                {
                    //reset timers and stop displaying icon.
                    autoSaveTime = autoSaveFrequency;
                    saveIconTime = saveIconDisplayTime;
                    saveIcon.SetActive(false);
                }
            }
        }
        
    }

    public void LoadGame()
    {
        //get folder directory
        string folderPath = currentSave.selectedSave;

        string dataPath = "";

        //get file directory
        dataPath = Path.Combine(folderPath, "GameData" + fileExtension);

        //begin reading data
        ReadData(gameData, dataPath);

    }

    public void ReadData(GameData data, string path)
    {
        //if PC build
#if UNITY_STANDALONE_WIN
        if (!File.Exists(path)) //check if file exists
        {
            return;
        }

        //read file
        using (StreamReader streamReader = File.OpenText(path))
        {
            string jsonString = streamReader.ReadToEnd(); //get JSON string
            data = JsonUtility.FromJson<GameData>(jsonString); //convert JSON to data struct
        }
       
#endif
        //if PS4 build
#if UNITY_PS4
       data = currentSave.ps4SaveData; //get data struct (current save is a dont destroy on load object)
#endif
        if (arcadeNameDisplay != null)
        {
            arcadeNameDisplay.text = data.arcadeName; //set the arcade text
        }

        if (player != null)
        {
            player.LoadData(data); //load saved player data
            cycle.LoadData(data); //load save DayNightCycle data

            Item[] items = FindObjectsOfType<Item>();

            //destroy default items (items placed in editor)
            foreach (var item in items)
            {
                if (!item.ignoreSerialization)
                {
                    Destroy(item.gameObject);
                }
            }

            //load and instantiate items
            for (int i = 0; i < data.itemsData.Count; i++)
            {
                Item prefab = Resources.Load<Item>("PlaceableObjects/" + data.itemsData[i].itemName);
                Item loadedItem = Instantiate(prefab, data.itemsData[i].itemPosition, Quaternion.Euler(data.itemsData[i].itemRotation));
                loadedItem.iData = data.itemsData[i];
                loadedItem.LoadData(data);
            }

            //check and compare which walls have been knocked down
            foreach (var destroyedGrid in data.DestroyedGrids)
            {
                foreach (var grid in gridsGenerated)
                {
                    if (grid != null)
                    {
                        if (grid.name == destroyedGrid)
                        {
                            Destroy(grid.gameObject);
                            break;
                        }
                    }              
                }
            }

            //check which rooms have been cleaned
            for (int i = 0; i < data.roomData.Count; i++)
            {
                rooms[i].LoadData(data.roomData[i]);
            }

            int lightIter = 0;

            //check which lights are active
            foreach (var activeLight in data.lightActive)
            {
                lights[lightIter].SetActive(activeLight);
                lightIter++;
            }


            //Load AI data
            if (AISpawner != null)
            {
                AISpawner.populationDensity = data.PopulationCount;

                if (AISpawner.AIPool.Count == 0 && AISpawner.AITypes.Count > 0) //generate an AI pool if one hasnt been created
                {
                    AISpawner.GeneratePool();
                }

                List<AIController> AIInScene = FindAllAI(); //get all the AI in scene.

                
                //begin loading behaviour and stats data
                for (int i = 0; i < data.AIData.Count; i++)
                {
                    AIStats loadedAI = null;
                    if (FindAIByName(AIInScene,data.AIData[i].objID).tag != "Car") //if not the other AI type in scene (Car), load data
                    {
                        for (int j = 0; j < AISpawner.AIPool.Count; j++)
                        {
                            //if correct type and isnt active
                            if (AISpawner.AIPool[j].name.Contains(data.AIData[i].AIType) && data.AIData[i].AIType != "" && !AISpawner.AIPool[j].gameObject.activeInHierarchy)
                            {
                                //assign loaded data
                                loadedAI = AISpawner.AIPool[j].GetComponent<AIStats>();
                                loadedAI.gameObject.SetActive(true);
                                loadedAI.aData = data.AIData[i];
                                loadedAI.LoadData(data);
                                AISpawner.activeAI.Add(loadedAI.controller);
                                AISpawner.activeAICounter++;
                                loadedAI.controller.navigationalMesh.enabled = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        //is car, load car data
                        loadedAI = GameObject.Find(data.AIData[i].objID).GetComponent<AIStats>();
                        loadedAI.aData = data.AIData[i];
                        loadedAI.LoadData(data); 
                    }
                }
            }

            progression.LoadData(data.progressionData); //load current progression data
            tutorial.LoadData(data.tutorialData); //load tutorial data
            economy.LoadData(data); //load economy data
            advertisingManager.LoadData(data.advertisingData); //load advertising data
            expensesManager.LoadData(data.expensesData); //load expenses data

            buildingManager.currentCapacity = data.aiInBuildingCount;
        }
    }

    public void SaveGame()
    {
        string dataPath = "";
        //if PC build
#if UNITY_STANDALONE_WIN
        if(currentSave != null) {
            string folderPath = currentSave.selectedSave; //get save folder path

            if (!Directory.Exists(folderPath)) //folder exists
            {
                //create one if not
                Directory.CreateDirectory(folderPath);
            }

            //get file path
            dataPath = Path.Combine(folderPath, "GameData" + fileExtension);
            //take screen shot and save it in same folder for displaying in load game menu
            ScreenCapture.CaptureScreenshot(Path.Combine(folderPath, "SaveScreenShot.png"));
        }
#endif
        //save room data
        gameData.roomData = new List<RoomData>(); 
        foreach (var room in rooms)
        {
            room.SaveData(ref gameData);
        }

        //save which lights are active
        gameData.lightActive = new List<bool>();
        foreach (var light in lights)
        {
            if (light != null)
            {
                gameData.lightActive.Add(light.activeInHierarchy);
            }
        }

        //save arcades name
        gameData.arcadeName = arcadeNameDisplay.text;
 
        if (player != null)
        {
            //save player data
            player.SaveData(ref gameData);

            //save DayNightCycle data
            cycle.SaveData(ref gameData);

            //saved placed items data
            Item[] placedItems = FindObjectsOfType<Item>(); 
            gameData.itemsData = new List<ItemData>();
            foreach (var item in placedItems)
            {
                if (!item.ignoreSerialization)
                {
                    item.SaveData(gameData);
                }
            }

            //save what walls have been destroyed
            gameData.DestroyedGrids = buildingManager.destoryedGrids;

            //save AI data
            if (AISpawner != null)
            {
                gameData.PopulationCount = AISpawner.populationDensity;

                AIStats[] AI = FindObjectsOfType<AIStats>();

                gameData.AIData = new List<AIData>();

                foreach (var ai in AI)
                {
                    ai.SaveData(gameData);
                }
            }
            //Save economy data  
            economy.SaveData(ref gameData);
        }

        //save tutorial data
        tutorial.SaveData(ref gameData);
        //save progression data
        progression.SaveData(ref gameData);

        //save advertising data
        advertisingManager.SaveData(ref gameData);
        //save expenses data
        expensesManager.SaveData(ref gameData);

        //save current capacity and how many AI in arcade.
        gameData.aiInBuildingCount = buildingManager.currentCapacity;
        gameData.currentbuildingCapacity = buildingManager.buildingCapacity;

        //save the current time the game was saved (Only on PC)
#if UNITY_STANDALONE_WIN

        int hour = DateTime.Now.Hour - 12;
        int minutes = DateTime.Now.Minute;
        string merdies = "AM";
        if (DateTime.Now.Hour >= 12)
        {
            merdies = "PM";
        }

        gameData.saveTime = hour + ":" + minutes + merdies;
#endif
        WriteData(gameData, dataPath); //begin writing to file
    }


    public void WriteData(GameData data, string path)
    {
        //write to JSON if on PC
#if UNITY_STANDALONE_WIN
        string jsonString = JsonUtility.ToJson(data);


        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(jsonString);
        }
#endif
        //write to data struct if on PS4
#if UNITY_PS4
        currentSave.ps4SaveData = data;
#endif
    }

    public GameObject FindAIByName(List<AIController> AI,string name)
    {
        foreach (var agent in AI) //search list of agents
        {
            if (name == agent.gameObject.name) //name matches
            {
                return agent.gameObject;
            }
        }

        return null;
    }

    /// <summary>
    /// Fetch All AI in the current scene.
    /// </summary>
    public List<AIController> FindAllAI()
    {
        List<AIController> AI = new List<AIController>(Resources.FindObjectsOfTypeAll<AIController>());

        for (int i = 0; i < AI.Count; i++)
        {
            if (!AI[i].gameObject.scene.IsValid())
            {
                AI.Remove(AI[i]);
            }
        }

        return AI;

    }



}
