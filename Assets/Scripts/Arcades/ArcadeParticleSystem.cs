﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcadeParticleSystem : MonoBehaviour
{
    
    public ParticleSystem SmokeBrokenDownMachine;
    public ParticleSystem Sparks;
    public ParticleSystem Stars;
    public ParticleSystem Dust;
   // public GameObject PointLight;
    public ParticleSystem Fireworks;

    // Start is called before the first frame update
   



    // Update is called once per frame
   public void Play()
    {
        SmokeBrokenDownMachine.Play();
        Sparks.Play();
        Dust.Play();
        Stars.Play();
        Fireworks.Play();
       // PointLight.SetActive(true);       


    }




    public void Stop()
    {
        SmokeBrokenDownMachine.Stop();
        Sparks.Stop();
        Dust.Stop();
        Stars.Stop();
        Fireworks.Play();
        //Destroy(PointLight);
        



    }
}
