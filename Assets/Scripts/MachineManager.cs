﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class MachineManager : MonoBehaviour {

    public Item itemSelected = null; //the item selected in purchase mode

    public Office office;
    public BuildingManager buildingManager;
    public MenuUI_Office menu;
    public ViewManager viewManager;
    public EditModeUI editMode_ui;

    public static List<Arcade> arcades = new List<Arcade>();
    public static List<Vendor> vendors = new List<Vendor>();
    public static List<Item> decorations = new List<Item>();

    public List<Item> defaultItems; //a list of all the default items in the scene that the player begins with

    private NotificationSystem notifications;
    public void Start() {
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
    }

    public void AddDefaultItems() {
         foreach(Item item in defaultItems) {
            if(item.itemType == ItemType.ARCADE)
                arcades.Add(item.gameObject.GetComponent<Arcade>());
            if(item.itemType == ItemType.VENDOR)
                vendors.Add(item.gameObject.GetComponent<Vendor>());
            if(item.itemType == ItemType.DECORATION)
                decorations.Add(item);
        }
    }

    //get the number of machines the player has purchased for item 'itemName'
    public static int GetNumberOfMachines(string itemName) {
        int amount = 0;
        foreach(Arcade arcade in arcades) {
            if(arcade.iname == itemName)
                amount++;
        }
        foreach(Vendor vendor in vendors) {
            if(vendor.iname == itemName)
                amount++;
        }
        foreach(Item decoration in decorations) {
            if(decoration.iname == itemName)
                amount++;
        }
        return amount;
    }

    //can be called when button is clicked - feed reference of the shop item
    public void EnterPurchaseMode(GameObject shopItem) {
        ShopItem shop = shopItem.GetComponent<ShopItem>(); //get the item that was clicked | get the item to buy

        //ensure player doesn't buy more than 3 of each item
        if(GetNumberOfMachines(shop.item.iname) < 3) {
            itemSelected = shopItem.GetComponent<ShopItem>().item; //the item reference

            //enter edit mode:
            office.PopDownButton();
            menu.PopDown(true);
            viewManager.Switch(View.EDIT);

            //enter purchase mode with the selected item:
            if(buildingManager != null) {
                buildingManager.SetPurchaseMode(true);
                buildingManager.placement.SetSelectedObject(itemSelected);

                if(buildingManager.selectedItem.placeType == CellType.WALL)
                    buildingManager.EnableGrids(true, GridType.WALL); //wall item, show wall grids
                else buildingManager.EnableGrids(true, GridType.FLOOR); //floor item, show floor grids
            }
        } else notifications.Add(new Notification("Arcade Management", "We have the maximum amount of " + shop.item.iname + " machines!"));
        editMode_ui.PopUp(); //show the edit mode UI
    }

    public PopulateGrid content; //reference to the shop grid

    //go back to office from edit/purchase mode:
    public void ExitPurchaseMode() {
        itemSelected = null;
        office.PopUpButton();
        menu.PopUp(true);
        
        editMode_ui.PopDown();

        //update shop grid
        content.Populate(content.currentDisplayed);
    }

}
