﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayStats : MonoBehaviour {

    public MenuUI_EOD menu;

    //sales
    public int numberOfCustomers = 0;
    public float drinkSales = 0;
    public float foodSales = 0;
    public float arcadeSales = 0;
    
    //profit (expenses)
    public float repairExpenses = 0;
    public float upgradeExpenses = 0;
    public float billsExpenses = 0;
    public float marketingExpenses = 0;

    //morale
    public float totalCustomerSatisfaction = 0;
    //when customer leaves store, get their satisfaction level and add to this, divide by number of customers

    //machines
    public int dirtyMachines = 0;
    public int brokenMachines = 0;

    public float GetAverageSatisfaction() {
        if (numberOfCustomers == 0) return 100;
        return totalCustomerSatisfaction / numberOfCustomers;
    }

    //returns to 2 decimal places ($ amount)
    public float GetTotalSales() {
        return Mathf.Round((drinkSales + foodSales + arcadeSales) * 100) / 100;
    }

    //returns to 2 decimal places ($ amount)
    public float GetAverageSpending() {
        if(numberOfCustomers == 0) return 0;
        float spending = GetTotalSales() / numberOfCustomers;
        return Mathf.Round(spending * 100) / 100;
    }

    //returns to 2 decimal places ($ amount)
    public float GetTotalExpenses() {
        return Mathf.Round((repairExpenses + upgradeExpenses + billsExpenses + marketingExpenses) * 100) / 100;
    }

    //returns to 2 decimal places ($ amount)
    public float GetProfit() {
        return GetTotalSales() - GetTotalExpenses();
    }
}
