﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Rewired;
using Pixelplacement;
using TMPro;

[System.Serializable]
public struct TutorialSection
{
    public string sectionName;
    public int timeScale;
    public bool debug;
    public GameObject objectiveMarker;
    public bool autoAdjustCamera;
    public List<Transform> tutorialCameraLocations;
    [TextArea]
    public List<string> tutorialText;
    public List<UnityEvent> tutorialEvent;
    public List<bool> useTweens;
    public UnityEvent onDissmiss;
    public bool completed;
}

[System.Serializable]
public struct TutorialData
{
    public List<bool> sectionsCompleted;
    public bool hadFirstCustomer;
}


public class Tutorial : MonoBehaviour
{
    public Camera playerCamera;
    public Camera tutorialCamera;
    public GameObject panel;
    public Vector3 panelPopOutPos;
    public Vector3 panelPopInPos;
    public TextMeshProUGUI textField;
    private int currentSectionIndex = 0;
    private TutorialSection currentSection;
    public List<TutorialSection> tutorial = new List<TutorialSection>();
    private int sectionIter = 0;
    private float currentDelayTimer = 0;
    public Button nextButton;
    public Button dismmissButton;
    public List<GameObject> UIHud = new List<GameObject>();
    private float currentNextDelayTimer = 0;
    public PlayerController_Default player;
    private Player input;
    public DayNightCycle cycle;
    [HideInInspector]
    public bool sectionActive = false;
    private List<int> activeInputIds = new List<int>();
    public EventSystem eventSystem;
    public SimulationSpeed simulationSpeed;
    public List<Button> buttonsToDisable = new List<Button>();
    public List<Slider> slidersToDisable = new List<Slider>();
    public BuildingManager buildingManager;


    // Start is called before the first frame update
    void Start()
    {
        input = ReInput.players.GetPlayer(0);
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (sectionActive)
        {
            player.viewManager.UICamera.transform.position = tutorialCamera.transform.position;
            player.viewManager.UICamera.transform.rotation = tutorialCamera.transform.rotation;
            if (currentDelayTimer < 1)
            {
                currentDelayTimer += Time.deltaTime % 60;
            }
          
            if (sectionIter == currentSection.tutorialText.Count - 1)
            {
                nextButton.gameObject.SetActive(false);
                dismmissButton.gameObject.SetActive(true);
                eventSystem.SetSelectedGameObject(dismmissButton.gameObject);
            }
            else
            {
                nextButton.gameObject.SetActive(true);
                dismmissButton.gameObject.SetActive(false);
                eventSystem.SetSelectedGameObject(nextButton.gameObject);
            }
        }     
    }

    public bool FindSection(string name, out TutorialSection desiredSection)
    {
        currentSectionIndex = 0;
        foreach (var section in tutorial)
        {
            if (section.sectionName == name)
            {
                desiredSection = section;
                return true;
            }
            currentSectionIndex++;
        }

        desiredSection = new TutorialSection();

        return false;
    }



    public void ResetListeners(Button button, UnityAction action)
    {
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(action);
    }
    /// <summary>
    /// Check whether the section is completed.
    /// </summary>
    public bool SectionCompleted(string sectionName)
    {
        foreach (var section in tutorial)
        {
            if (section.sectionName == sectionName)
            {
                if (section.completed)
                {
                    return true;
                }
            }
        }


        return false;
    }


    /// <summary>
    /// This function assumes the user has assigned the values in the inspector for the tutorial section that is being called.
    /// </summary>
    public void PromptTutorial(string name)
    {
        if (!FindSection(name, out currentSection))
        {
            return;
        }

        if (!currentSection.debug)
        {
            if (!currentSection.completed)
            {
                foreach (var section in tutorial)
                {
                    if (section.sectionName == name)
                    {
                        currentSection = section;
                        break;
                    }
                }

                if (currentSection.objectiveMarker != null)
                {
                    Destroy(currentSection.objectiveMarker);
                }

                SetUp();
            }
        }
    }

    /// <summary>
    /// Add an onDissmiss event programmatically.
    /// </summary>
    public void AddDissmissEvent(string name, UnityAction action)
    {
        TutorialSection section = new TutorialSection();

        if (!FindSection(name, out section))
        {
            return;
        }
        section.onDissmiss.AddListener(action);

    }

    /// <summary>
    /// Add an event programmatically based on a provided index in the section.
    /// </summary>
    public void AddEvent(string name,int index, UnityAction action)
    {
        TutorialSection section = new TutorialSection();

        if (!FindSection(name, out section))
        {
            return;
        }
        section.tutorialEvent[index].AddListener(action);

    }

    /// <summary>
    /// This assumes there is no camera position setup and the user would like to set it via script.
    /// </summary>
    public void PromptTutorial(string name,Transform cameraLocation, int cameraLocationCount)
    {

        if (!FindSection(name, out currentSection))
        {
            return;
        }
        
        if (!currentSection.debug)
        {
            if (!currentSection.completed)
            {
                foreach (var section in tutorial)
                {
                    if (section.sectionName == name)
                    {
                        currentSection = section;
                        break;
                    }
                }

                for (int i = 0; i < cameraLocationCount; i++)
                {
                    currentSection.tutorialCameraLocations.Add(cameraLocation);
                }

                SetUp();
            }        
        }
    }

    private void SetUp()
    {
        player.isRotating = false;
        if(currentSection.tutorialCameraLocations[0] != null) {
            foreach (var ui in UIHud)
            {
                ui.SetActive(false);
            }

            player.enabled = false;
            cycle.enabled = false;
            panel.SetActive(true);


            foreach (var map in input.controllers.maps.GetAllMapsInCategory(0))
            {
                if (map.controller.enabled)
                {
                    if (map.categoryId != 2) //if the map isnt the UI map
                    {
                        activeInputIds.Add(map.categoryId);
                        input.controllers.maps.SetMapsEnabled(false, map.categoryId);
                    }
                }
            }
            ResetListeners(nextButton, Next);
            ResetListeners(dismmissButton, DismissTutorialPromt);

            if (currentSection.tutorialText.Count > 1)
            {
                eventSystem.SetSelectedGameObject(nextButton.gameObject);
            }
            else
            {
                eventSystem.SetSelectedGameObject(dismmissButton.gameObject);
            }
            simulationSpeed.SetSpeed(1);

            playerCamera.gameObject.SetActive(false);
            tutorialCamera.gameObject.SetActive(true);

            if (currentSection.autoAdjustCamera)
            {
                RaycastHit hit;
                Ray ray = new Ray(currentSection.tutorialCameraLocations[sectionIter].position, currentSection.tutorialCameraLocations[sectionIter].forward);

                if (Physics.Raycast(ray, out hit, 5))
                {
                    if (hit.transform.tag == "Wall" || hit.transform.tag == "GridSquare")
                    {
                        float side = Vector3.Dot(currentSection.tutorialCameraLocations[sectionIter].position, hit.transform.position);
                        if (side < 0)
                        {
                            side = -1;
                        }
                        if (side > 0)
                        {
                            side = 1;
                        }

                        Vector3 offsetPos = currentSection.tutorialCameraLocations[sectionIter].localPosition;
                        Vector3 offsetRot = currentSection.tutorialCameraLocations[sectionIter].rotation.eulerAngles;
                        currentSection.tutorialCameraLocations[sectionIter].position += new Vector3(offsetPos.x * side, 0, 0);
                        currentSection.tutorialCameraLocations[sectionIter].rotation = Quaternion.Euler(offsetRot.x, offsetRot.y * -side, offsetRot.z);
                    }
                }

            }

            tutorialCamera.transform.position = currentSection.tutorialCameraLocations[0].position;
            tutorialCamera.transform.rotation = currentSection.tutorialCameraLocations[0].rotation;
            Tween.AnchoredPosition(panel.GetComponent<RectTransform>(), panelPopOutPos, 0.2f, 0, Tween.EaseIn);
            currentSection.tutorialEvent[sectionIter].Invoke();
            textField.text = currentSection.tutorialText[0];
            sectionActive = true;
            EnableButtonsAndSliders(false);
        }
    }


    public void Next()
    {
        if (currentDelayTimer > 1)
        {
            sectionIter++;

            textField.text = currentSection.tutorialText[sectionIter];
            tutorialCamera.transform.rotation = currentSection.tutorialCameraLocations[sectionIter].rotation;

            if (currentSection.autoAdjustCamera)
            {
                RaycastHit hit;
                Ray ray = new Ray(currentSection.tutorialCameraLocations[sectionIter].position, currentSection.tutorialCameraLocations[sectionIter].forward);

                if (Physics.Raycast(ray, out hit, 5))
                {
                    if (hit.transform.tag == "Wall" || hit.transform.tag == "GridSquare")
                    {
                        float side = Vector3.Dot(currentSection.tutorialCameraLocations[sectionIter].position, hit.transform.position);
                        if (side < 0)
                        {
                            side = -1;
                        }
                        if (side > 0)
                        {
                            side = 1;
                        }

                        Vector3 offsetPos = currentSection.tutorialCameraLocations[sectionIter].localPosition;
                        Vector3 offsetRot = currentSection.tutorialCameraLocations[sectionIter].rotation.eulerAngles;
                        currentSection.tutorialCameraLocations[sectionIter].position += new Vector3(offsetPos.x * side, 0, 0);
                        currentSection.tutorialCameraLocations[sectionIter].rotation = Quaternion.Euler(offsetRot.x, offsetRot.y * -side, offsetRot.z);
                    }
                }
            }

          
            if (currentSection.useTweens[sectionIter])
            {
                Tween.Position(tutorialCamera.transform, currentSection.tutorialCameraLocations[sectionIter].position, 0.5f, 0);
            }
            else
            {
                tutorialCamera.transform.position = currentSection.tutorialCameraLocations[sectionIter].position;
            }
            currentSection.tutorialEvent[sectionIter].Invoke();
            currentDelayTimer = 0;
        }
      
    }

    public void DismissTutorialPromt()
    {
        Tween.AnchoredPosition(panel.GetComponent<RectTransform>(), panelPopInPos, 0.2f, 0, Tween.EaseInOut,Tween.LoopType.None, null, DisablePanel);

        foreach (var ui in UIHud)
        {
            ui.SetActive(true);
        }

        foreach (var id in activeInputIds)
        {
            input.controllers.maps.SetMapsEnabled(true, id);
        }
        EnableButtonsAndSliders(true);

        bool returnToEditView = false;

        if (currentSection.sectionName == "Customer" && (buildingManager.editMode || buildingManager.purchaseMode || buildingManager.destroyWallMode || buildingManager.cleaningMode))
        {
            returnToEditView = true;
        }

        
        activeInputIds.Clear();
        currentSection.onDissmiss.Invoke();
        tutorialCamera.gameObject.SetActive(false);
        playerCamera.gameObject.SetActive(true);

        player.viewManager.UICamera.gameObject.SetActive(true);
        player.enabled = true;
        cycle.enabled = true;
        currentSection.completed = true;
        tutorial[currentSectionIndex] = currentSection;
        //tutorial.Remove(currentSection);
        sectionIter = 0;
        Time.timeScale = 1;
        sectionActive = false;

        if (returnToEditView)
        {
            player.viewManager.Switch(View.EDIT);
        }
        else
        {
            player.viewManager.Switch(player.viewManager.currentView);
        }

    }

    public void EnableButtonsAndSliders(bool status)
    {
        Debug.Log(status);
        foreach (var button in buttonsToDisable)
        {
            if (button != null)
            {
                button.interactable = status;
                ColorBlock newBlock = button.colors;
                Color disableColor = newBlock.disabledColor;
                newBlock.disabledColor = new Color(disableColor.r, disableColor.g, disableColor.b, 1);
                button.colors = newBlock;
            }
        }
        foreach (var slider in slidersToDisable)
        {
            if (slider != null)
            {
                slider.interactable = status;
                ColorBlock newBlock = slider.colors;
                Color disableColor = newBlock.disabledColor;
                newBlock.disabledColor = new Color(disableColor.r, disableColor.g, disableColor.b, 1);
                slider.colors = newBlock;
            }
        }
    }


    public void DisablePanel()
    {
        panel.SetActive(false);
    }

    public void SaveData(ref GameData data)
    {
        data.tutorialData.sectionsCompleted.Clear();
        data.tutorialData.sectionsCompleted = new List<bool>();
        data.tutorialData.hadFirstCustomer = buildingManager.firstCustomer;
        foreach (var section in tutorial)
        {
            data.tutorialData.sectionsCompleted.Add(section.completed);
        }
    }

    public void LoadData(TutorialData data)
    {
        buildingManager.firstCustomer = data.hadFirstCustomer;
        for (int i = 0; i < tutorial.Count; i++)
        {
            TutorialSection section = tutorial[i];
            section.completed = data.sectionsCompleted[i];
            tutorial[i] = section;

            if (tutorial[i].objectiveMarker != null && tutorial[i].completed)
            {
                Destroy(tutorial[i].objectiveMarker);
            }
        }
    }




}
