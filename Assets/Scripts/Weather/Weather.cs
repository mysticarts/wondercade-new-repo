﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class Weather : MonoBehaviour
{

    public ParticleSystem rain;
    public bool debug = false;
    public AdvertisingManager advertisingManager;
    private bool rainPlaying = false;
    public List<Material> effectedMaterials = new List<Material>();
    public List<float> metallicValues = new List<float>();
    public Transform rainTransform;
    public AudioSource audioSource;
    // Start is called before the first frame update

    private void OnApplicationQuit()
    {
        for (int i = 0; i < effectedMaterials.Count; i++)
        {
            effectedMaterials[i].SetFloat("_GlossMapScale", metallicValues[i]);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (advertisingManager.GetKeyWord("Cold") || debug)
        {
            rain.transform.position = rainTransform.position;
            if (!rainPlaying)
            {
                audioSource.enabled = true;
                rain.Play();
                for (int i = 0; i < effectedMaterials.Count; i++)
                {
                    Tween.ShaderFloat(effectedMaterials[i],"_GlossMapScale",1,4,0);
                    //effectedMaterials[i].SetFloat("_Metallic", 1);
                    //effectedMaterials[i].SetFloat("_Glossiness", 1);

                    // Tween.ShaderFloat(renderers[i].material, "_Metallic", 1, 4, 0);
                }
                rainPlaying = true;
            }
        }
        else
        {
            if (rainPlaying)
            {
                audioSource.enabled = false;
                rain.Stop();
                for (int i = 0; i < effectedMaterials.Count; i++)
                {
                    Tween.ShaderFloat(effectedMaterials[i], "_GlossMapScale", metallicValues[i], 4, 0);
                }
                rainPlaying = false;
            }
        }
    }






}
