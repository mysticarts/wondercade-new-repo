﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class Invaders_Movement : Action
{
    public enum MovementDirection
    {
        RIGHT = 1,
        LEFT = -1,
    }

    [HideInInspector]
    public MovementDirection direction = MovementDirection.RIGHT;

    public float movementSpeedIncreaseRate = 0; //the rate at which AI increase every level
    public float movementSpeed = 0;
    private float changingDirectionDelay = 0; //the delay in changing direction
    private float currentDelayTimer = 0;
    private float currentMovementSpeed = 0;
    private AISwarm swarm; //the swarm manager 
    private bool changingDirection = false;

    public override void Start()
    {
        swarm = FindObjectOfType<AISwarm>();
        swarm.AI.Add(controller); //add itself to the known swarm
        swarm.AIMovement.Add(this);
    }

    public override void OnEnter()
    {
       
    }

    public override BehaviourResult execute()
    {

        if (changingDirection) //if its changing direction
        {
            currentDelayTimer += Time.deltaTime * 2;
            if (currentDelayTimer >= changingDirectionDelay) //if timer is greater than threshold
            {
                currentMovementSpeed = 0; //stop movement
                agentObj.transform.position += agentObj.transform.up * 1; //move up
                currentDelayTimer = 0;
                changingDirection = false;
            }
            return result = BehaviourResult.SUCCESS;
        }


        RaycastHit hit;

        if (direction == MovementDirection.RIGHT) //if moving right
        {
            if (Physics.Raycast(agentObj.transform.position, agentObj.transform.right, out hit, 1))
            {
                if (hit.transform.tag == "Right") //raycast has hit the right barrier
                {
                    foreach (var ai in swarm.AIMovement)
                    {
                        ai.ChangeDirection(MovementDirection.LEFT); //change movement direction
                    }
                    return result = BehaviourResult.SUCCESS;
                }
            }
        }
        if (direction == MovementDirection.LEFT) 
        {
            if (Physics.Raycast(agentObj.transform.position, -agentObj.transform.right, out hit, 1))
            {
                if (hit.transform.tag == "Left") //raycast has hit the left barrier
                {
                    foreach (var ai in swarm.AIMovement)
                    {
                        ai.ChangeDirection(MovementDirection.RIGHT); //change movement direction
                    }
                    return result = BehaviourResult.SUCCESS;
                }
            }
        }

        currentMovementSpeed += Time.deltaTime * 2;
        if (currentMovementSpeed >= movementSpeed && !changingDirection) //if moving and not changing direction
        {
            currentMovementSpeed = 0;
            agentObj.transform.position += agentObj.transform.right * (int)direction; //move
        }

        return result = BehaviourResult.SUCCESS;
    }

    public void ChangeDirection(MovementDirection dir)
    {
        //change direction.
        if (agentObj != null)
        {
            changingDirection = true;
            movementSpeed -= movementSpeedIncreaseRate;
            changingDirectionDelay = movementSpeed;
            currentMovementSpeed = 0;
            direction = dir;
        }

    }
  
    public void ResetSpeed(float speed)
    {
        currentMovementSpeed = speed;
    }


    public override float CalculateWeight()
    {
        return 0;
    }
}
