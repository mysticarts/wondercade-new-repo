﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

//Car behaviour
public class Drive : Action
{
    public float acceleration = 0;
    public float deacceleration = 0;
    private float speed = 0;
    public float maxSpeed = 0;
    private Material paintMaterial;
    private Vector3 startPosition;

    public override void Start()
    {
        paintMaterial = agentObj.GetComponent<Renderer>().material;
        startPosition = agentObj.transform.position;
        ChangeColour();
    }

    public override void OnEnter()
    {
    }

    public override BehaviourResult execute()
    {
        RaycastHit hit;
        if (Physics.BoxCast(agentObj.transform.position, new Vector3(1, 1, 3), -agentObj.transform.forward, out hit, agentObj.transform.rotation, 10))
        {
            //if it collides with the reset trigger
            if (hit.transform.tag == "CarTrigger")
            {
                agentObj.transform.position = startPosition; //reset the position
                ChangeColour(); //change its colour
            }
            if (hit.transform.tag == "AI") //if its a customer
            {
                speed -= deacceleration * Time.deltaTime;   //slow down   
            }
        }
        else
        {
            speed += acceleration;
        }

        speed = Mathf.Clamp(speed, 0, maxSpeed);

        agentObj.transform.position -= agentObj.transform.forward * speed * Time.deltaTime;

        return result = BehaviourResult.SUCCESS;
    }


    void ChangeColour()
    {
        float r = Random.Range(0, 255);
        float g = Random.Range(0, 255);
        float b = Random.Range(0, 255);

        paintMaterial.color = new Color(r / 255, g / 255, b / 255, 1);

    }

    public override float CalculateWeight()
    {
        return 0;
    }

    //SaveData
    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(speed) + speed.ToString());
        base.SaveData(ref data);
    }

    //LoadData
    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(speed), out speed);
        base.LoadData(data);

    }
}
