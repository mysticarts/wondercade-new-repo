﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tree.Editor;
using Pixelplacement;

public class ChooseArcade : Action
{
    private AIStats stats; //current stats of the AI reference
    private AIAnimationHandler anim; //current stats of the Animation
    public Transform chosenArcadePos; //the chosen arcades transform.
    private Arcade arcadeToIgnore; //arcade to ignore when selecting a new game, which is always the previous game.
    [HideInInspector]
    public Vector3 targetPos; //the position to feed the nav mesh agent.
    public Arcade chosenArcade; //the chosen arcade
    private string chosenArcadesID; //the ID of the chosen arcade.
    private GameName previouslyDesired = GameName.None; //the name of the previously played game.
    public GameName desiredArcade;
    public float playTime = 0;
    private float currentPlayTime = 0;
    private bool playing = false;
    private Image progressDisplay;
    public bool angryReact = false;
    private Vector3 modelsOrgin; //used to reposition the kid model.
 
    //get references
    public override void Start()
    {
        stats = agentObj.GetComponent<AIStats>();
        progressDisplay = agentObj.GetComponentInChildren<Image>();
        anim = agentObj.GetComponentInChildren<AIAnimationHandler>();
        modelsOrgin = anim.transform.localPosition;
        if (progressDisplay != null)
        {
            if (playing) //if loading in from a save and it was playing the game when the player left the game
            {
                progressDisplay.transform.parent.gameObject.SetActive(true); //enable display
            }
            else
            {
                progressDisplay.transform.parent.gameObject.SetActive(false);
                progressDisplay.fillAmount = 0;
            }
        }

    }
    public override void OnEnter()
    {
     
    }

    public override BehaviourResult execute()
    {
        if (chosenArcadePos == null) //no arcade has been chosen
        {
            ChooseGame(agentObj.transform, out chosenArcadePos); //choos
       //     navigation.SetDestination(targetPos);
        }
        if (chosenArcadePos != null && !angryReact) //an arcade has been chosen and isnt angry.
        {
            tree.SetBool(true, "Chosen Action"); //set behaviour tree parameter
            chosenArcadesID = chosenArcade.InstanceID; //set chosen arcade ID

            if (chosenArcade.queue.Contains(controller)) //if lining up
            {
                if (chosenArcade.state == ArcadeState.BROKEN && !angryReact) //if the arcade breaks
                {
                    //angry react
                    anim.animator.SetBool("Wandering", false); 
                    anim.animator.SetBool("Angry", true); //angry reaction
                    angryReact = true;
                }

                if (angryReact) //exit behaviour if playing angry react animation
                {
                    return result = BehaviourResult.RUNNING;
                }
                if (navigation.enabled) //Error checking
                {
                    navigation.SetDestination(targetPos);
                }
            }

            if (chosenArcade.customer != null) //if there isnt a customer already
            {
                if (chosenArcade.customer != controller) //if was walking to arcade but another AI has reached it first
                {
                    if (!chosenArcade.queue.Contains(controller)) //and its not currently in the queue to play
                    {
                        if (GoToQueue() && chosenArcade.queue.Count < chosenArcade.queueSize) //decide whether to line up
                        {
                            Debug.Log("Imma Line up");
                            Queue();
                            return BehaviourResult.RUNNING;
                        }
                        else //begin looking for another
                        {
                            Debug.Log("Nah ill find another");
                            chosenArcade = null;
                            chosenArcadePos = null;
                            return BehaviourResult.RUNNING;
                        }
                    }  
                }               
            }
            if (chosenArcade.customer == null) //ther is no customer
            {
                if (chosenArcade.queue.Count == 0) //no one is lining up
                {
                    if (targetPos != chosenArcadePos.position) // set the destination to be the chosen arcade
                    {
                        targetPos = chosenArcadePos.position;
                        navigation.SetDestination(targetPos);
                    }
                }            
            }

            if (playing) //if in the act of playing the arcade
            {
                progressDisplay.fillAmount = currentPlayTime / playTime; //update the progress bar
                if (currentPlayTime < playTime) //if playing
                {
                    agentObj.transform.LookAt(new Vector3(chosenArcade.transform.position.x, agentObj.transform.position.y, chosenArcade.transform.position.z), agentObj.transform.up); //set customer to look at the machine
                    currentPlayTime += Time.deltaTime % 60; //update timer
                    return result = BehaviourResult.RUNNING;
                }
                else //exiting machine 
                {
                    if (chosenArcade.cabinetType == ArcadeType.CLASSIC) //if was playing a classic arcade
                    {
                        anim.animator.SetBool("Playing Classic Arcade", false); //exit animation
                    }
                    if (chosenArcade.cabinetType == ArcadeType.COCKTAIL) //if was playing cocktail arcade
                    {
                        anim.animator.SetBool("Playing Cocktail Arcade", false); //exit animatio
                    }
                    anim.animator.SetBool("Wandering", true); //enter wandering animation
                    arcadeToIgnore = null; //remove reference to ignore
                    chosenArcade.Deteriorate(); //update the acrades condition stats
                    progressDisplay.transform.parent.gameObject.SetActive(false); //disable progress display
                    currentPlayTime = 0; //reset timer
                    playing = false;
                    chosenArcadePos = null; //no longer has an arcade chosen
                    stats.wanderTimer = 0; //reset wander time
                    stats.morale += stats.ReconfigureMorale(stats.moraleGainRange); //gain morale
                    tree.SetBool(true, "Wandering"); //enter wander behaviour
                    tree.SetBool(false, "Find Arcade"); //no longer need to find an arcade
                    stats.boredom = 0; //no longer in boredom
                   // chosenArcade.customer = controller; //
                    chosenArcade = null; 
                    previouslyDesired = GameName.None;
                    anim.transform.localPosition = modelsOrgin; //reset the models position
                    return result = BehaviourResult.SUCCESS;
                }
            }
            if (Vector3.Distance(agentObj.transform.position, chosenArcade.transform.position) < 1.5f) //if close enough to an arcade
            {
                if (!playing) //not already playing
                {
                    if (anim != null) //animation isnt null
                    {
                        anim.animator.SetBool("Wandering", false); //exit wandering animation
                        if (chosenArcade.cabinetType == ArcadeType.CLASSIC) //playing the standard arcade
                        {
                            if (stats.aiType == AIType.KID) //if its a kid
                            {
                                anim.transform.localPosition += new Vector3(0, 0.23f,0); //reposition model to appear on top of the standing stool.
                                Debug.Log("Standing Stool activated.");

                            }
                            anim.animator.SetBool("Playing Classic Arcade", true); //set correct animation
                        }
                        if (chosenArcade.cabinetType == ArcadeType.COCKTAIL) //playing a cocktail
                        {
                            anim.animator.SetBool("Playing Cocktail Arcade", true); //set correct animation
                        }
                    }
                    progressDisplay.transform.parent.gameObject.SetActive(true); //enable progress display
                    playing = true;
                    chosenArcade.advanceQueue = true; //advance the lineup
                    chosenArcade.customer = controller; //set the current customer.                  
                    stats.economySystem.Give(chosenArcade.ussageCost); //give the player the items cost.
                    stats.wallet -= chosenArcade.ussageCost; //take the money from the AI's wallet
                    stats.cycle.currentDayStats.arcadeSales += chosenArcade.ussageCost; //update current day stats
                    chosenArcade.occupant = stats.ID; //set the occupent ID
                    chosenArcade.timesPlayed++; //update arcades stats
                    chosenArcade.totalSales += chosenArcade.ussageCost;
                    stats.gamesPlayed++;
                    //determines whether the AI thinks the place is dirty or not
                    if(stats.InDirtyRoom()) stats.cleanliness = 0; // a dirty room is instantly 0
                    else stats.cleanliness = 100 - (chosenArcade.dirtiness); // else cleanliness is based on last arcade played

                }
                return result = BehaviourResult.RUNNING;
            }
            else
            {
                navigation.SetDestination(targetPos); //navigate towards target.
                return result = BehaviourResult.RUNNING;
            }
        }
        else
        {
            stats.morale -= stats.ReconfigureMorale(stats.moraleLossRange); //is bordem.
            previouslyDesired = desiredArcade;
        }
        return result = BehaviourResult.FAILURE;
    }
    public override float CalculateWeight()
    {
        return stats.boredom;
    }

    public bool ChooseGame(Transform origin, out Transform target)
    {
        List<Arcade> machines = new List<Arcade>(FindObjectsOfType<Arcade>());
        List<GameName> availableGames = new List<GameName>();
        int brokenMachineCount = 0;

        //Determines what games are available in the arcade.
        foreach (var machine in machines)
        {
            if (!availableGames.Contains(machine.gameName))
            {
                availableGames.Add(machine.gameName);
            }
            if (machine.state == ArcadeState.BROKEN)
            {
                brokenMachineCount++;
            }
        }

        //all the machines are broken.
        if (brokenMachineCount == machines.Count)
        {
            target = null;
            return false;
        }
        desiredArcade = GameName.None;

        //if there was a previously desired game, remove it from the selection
        if (previouslyDesired != GameName.None)
        {
            availableGames.Remove(previouslyDesired);
        }
        Random.InitState(System.DateTime.Now.Millisecond);
        int random = Random.Range(0, availableGames.Count);

        if (random >= 0 && random < availableGames.Count) //Error checking
        {
            desiredArcade = availableGames[random];//Randomly Choose Game
        }

        float minDist = Mathf.Infinity;
        target = null;
        foreach (var arcade in machines)
        {
            //Choose the closet arcade based on whether its the game it wants to play, if it can afford it and if its not currently being moved around by the player in edit mode.
            if (desiredArcade == arcade.gameName  && arcade.state != ArcadeState.BROKEN && stats.wallet >= arcade.ussageCost && arcade.transform.parent == null)
            {
                float dist = Vector3.Distance(origin.position, arcade.transform.position);
                if (dist < minDist)
                {
                    if(!CostToHigh(arcade)) //determine if it thinks the arcade is too high
                    {
                        target = arcade.playerStandingPos.transform;
                        chosenArcade = arcade;
                        minDist = dist;
                    }
                }
         
            }
        }

        if (target != null)
        {       
            if (!chosenArcade.occupied && chosenArcade.queue.Count == 0) //if the arcade isnt occupied and doesnt have a queue
            {
               // previousPlayed = desiredArcade;
                targetPos = target.position;
                //chosenArcade.occupied = true;
                tree.SetBool(false, "Wandering");
                navigation.SetDestination(targetPos);
                return true;
            }
            else
            {
                if (GoToQueue() && chosenArcade.queue.Count < chosenArcade.queueSize) //if it decides to line up and the queue size is small enough
                {
                    //previousPlayed = desiredArcade;
                    Queue();
                    return true;
                }
                else
                {
                    tree.SetFloat(0, "Wander Time"); //continue wandering
                    target = null;
                    return false;
                }
            }
        }


        return false;
    }

    private void Queue()
    {
        if (!chosenArcade.queue.Contains(controller)) //isnt already in queue
        {
            targetPos = chosenArcade.transform.position;
            targetPos -= chosenArcade.transform.forward * 2 + new Vector3(0, 0, chosenArcade.queue.Count); //get the queues position.
            chosenArcade.queue.Add(controller);
            tree.SetBool(false, "Wandering");
            navigation.SetDestination(targetPos);
        }
    }

    bool GoToQueue()
    {
        return (Random.value < 0.5f);
    }

    public bool CostToHigh(Arcade arcade)
    {
        if (arcade.ussageCost  == 0) //if its free
        {
            return false;
        }

        if (arcadeToIgnore != null)
        {
            if (arcade == arcadeToIgnore)
            {
                return true;
            }
        }
        float sum = stats.wallet; //get the sum
        float prob = (sum / arcade.ussageCost) * (arcade.currentLevel * 2); //calculate of the probability based on the arcades cost, current wallet balance and current level.

        float dice = Random.Range(0, sum); //roll a random value of not wanting to play it.

        if (prob > dice) //if the probabilty of wanting to play it is greater than the value of not wanting to play it
        {
            //Debug.Log(arcade.name + " is a good value" + " , " + "prob = " + prob + " , " + "dice = " + dice);
            return false;
        }

     //   Debug.Log(arcade.name + " costs too much" + " , " + "prob = " + prob + " , " + "dice = " + dice);

        arcadeToIgnore = arcade;

        return true;
    }

    //save game data
    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(playing) + playing.ToString()); //is playing arcade?
        bData.fieldData.Add(nameof(currentPlayTime) + currentPlayTime.ToString()); //if so how long has it played it for?

        bData.fieldData.Add(nameof(desiredArcade) + (int)desiredArcade); //what its desired arcade was
        bData.fieldData.Add(nameof(previouslyDesired) + (int)previouslyDesired); //what it previously desired
        if (chosenArcade != null) //did it have a chosen arcade
        {
            if (chosenArcade.occupied) //was it occupied?
            {
                if (chosenArcade.queue.Contains(controller)) //its currently in the queue to use it
                {
                    bData.fieldData.Add("QueueIndex" + chosenArcade.queue.IndexOf(controller)); //save its position in the queue
                }
            }
            bData.fieldData.Add(nameof(chosenArcadesID) + chosenArcadesID); //save the ID of the chosen arcade
        }
      
        if (chosenArcadePos != null)
        {
            //save the chosen arcades position
            bData.fieldData.Add(nameof(chosenArcadePos) + "x" + chosenArcade.transform.position.x.ToString());
            bData.fieldData.Add(nameof(chosenArcadePos) + "y" + chosenArcade.transform.position.y.ToString());
            bData.fieldData.Add(nameof(chosenArcadePos) + "z" + chosenArcade.transform.position.z.ToString());

            //save the destination position
            bData.fieldData.Add(nameof(targetPos) + "x" + targetPos.x.ToString());
            bData.fieldData.Add(nameof(targetPos) + "y" + targetPos.y.ToString());
            bData.fieldData.Add(nameof(targetPos) + "z" + targetPos.z.ToString());
        }

        //save data in base class (_behaviour)
        base.SaveData(ref data);

    }

    //Load save data
    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(playing), out playing); //load if it was playing an arcade
        AssignLoadedFieldData(nameof(currentPlayTime), out currentPlayTime); //if so how long was it playing it for

        int prevDesired, desArcade;

        AssignLoadedFieldData(nameof(desiredArcade), out desArcade);
        desiredArcade = (GameName)desArcade; //load desired arcade
        AssignLoadedFieldData(nameof(previouslyDesired), out prevDesired);
        previouslyDesired = (GameName)prevDesired; //load what it previously desired

        //load destination coords
        AssignLoadedFieldData(nameof(targetPos) + "x", out targetPos.x);
        AssignLoadedFieldData(nameof(targetPos) + "y", out targetPos.y);
        AssignLoadedFieldData(nameof(targetPos) + "z", out targetPos.z);

        Vector3 chosenArcadePosition;

        //load chosenArcade pos
        AssignLoadedFieldData(nameof(chosenArcadePos) + "x", out chosenArcadePosition.x);
        AssignLoadedFieldData(nameof(chosenArcadePos) + "y", out chosenArcadePosition.y);
        AssignLoadedFieldData(nameof(chosenArcadePos) + "z", out chosenArcadePosition.z);

        //load chosen arcades ID
        AssignLoadedFieldData(nameof(chosenArcadesID), out chosenArcadesID);

        //find all cabinets
        Arcade[] arcadeCabinets = FindObjectsOfType<Arcade>();

        foreach (var arcade in arcadeCabinets)
        {
            if (arcade.InstanceID == chosenArcadesID) //if chosen arcade exists
            {
                //assign it
                chosenArcadePos = arcade.playerStandingPos.transform;
                chosenArcade = arcade;
            }
        }

        if (chosenArcade != null)
        {
            if (chosenArcade.occupied) //if it is occupied
            {
                int QueueIndex;
                AssignLoadedFieldData(nameof(QueueIndex), out QueueIndex); //get position in Queue 
                if (chosenArcade.queueIDs.Contains(stats.ID)) //if the queue contained the agent, assign its position in line.
                {
                    chosenArcade.queue[QueueIndex] = controller;
                }
                else
                {
                    if (chosenArcade.cabinetType == ArcadeType.CLASSIC && stats.aiType == AIType.KID) //if was a standard arcade and the agent is a kid
                    {
                        //set the standing stool active and reposition the kid on top of the standing stool.
                        chosenArcade.standingStool.SetActive(true);
                        anim.transform.position = chosenArcade.stoolStandingPos.position;
                        navigation.enabled = false;
                    }
                     //set the customer to this agent.
                    chosenArcade.customer = controller;

                }
            }
        }
        //load base data.
        base.LoadData(data);
    }
}
