﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class Pong_Movement : Action
{
    private PlayController_Pong pcontroller;
    private PuckMovement puckMovement;

    public override void Start()
    {
        pcontroller = agentObj.GetComponent<PlayController_Pong>();
        puckMovement = FindObjectOfType<PuckMovement>();
    }

    public override void OnEnter()
    {
    }
    public override BehaviourResult execute()//execute behaviour
    {
        //if raycast hasnt hit the top bar
        if (!Physics.Raycast(agentObj.transform.position + new Vector3(0, agentObj.transform.localScale.y * 0.5f, 0), Vector3.up, 0.1f))
        {
            if (puckMovement.verticalSpeed > 0 && puckMovement.transform.position.y + 0.11f > agentObj.transform.position.y) //if the puck is moving up and its y position is greater than its y position.
            {
                pcontroller.Movement(pcontroller.movementSpeed, 1); //move up
                return result = BehaviourResult.SUCCESS;
            }
        }
        //if raycast hasnt hit bottom bar
        if (!Physics.Raycast(agentObj.transform.position - new Vector3(0, agentObj.transform.localScale.y * 0.5f, 0), -Vector3.up, 0.1f))
        {
            if (puckMovement.verticalSpeed < 0 && puckMovement.transform.position.y - 0.11f < agentObj.transform.position.y) //if puck is moving down and its y position is less than its y position.
            {
                pcontroller.Movement(pcontroller.movementSpeed, -1);
                return result = BehaviourResult.SUCCESS;
            }
        }



        return result = BehaviourResult.SUCCESS;
    }


    public override float CalculateWeight()
    {
        return weightValue;
    }

    public override void SaveData(ref AIData data)
    {
        base.SaveData(ref data);

    }

    public override void LoadData(AIData data)
    {
        base.LoadData(data);

    }
}
