﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;


public class Flocking : Action
{
    public float detectionRadius = 0;
    public AISpawning spawner; //the ai spawner

    public override void Start()
    {
        spawner = FindObjectOfType<AISpawning>();   
    }

    public override void OnEnter()
    {
        
    }

    public override BehaviourResult execute()
    {
        foreach (var ai in spawner.activeAI)
        {
            if (agentObj != ai.gameObject) //if its not its self
            {
                if (Vector3.Distance(agentObj.transform.position, ai.transform.position) < 2) //is within distance
                {
                    if (navigation.isOnNavMesh)
                    {
                        Vector3 agent1NewPos = Vector3.MoveTowards(agentObj.transform.position, ai.transform.position, -1 * 0.0008f); //calculate avoidance vector
                        navigation.Move(agent1NewPos - agentObj.transform.position); //move the AI
                    }
                }
            }         
        }




        return result = BehaviourResult.RUNNING;
    }


    public bool IsOnNavMesh()
    {
        NavMeshHit hit;

        if (NavMesh.SamplePosition(agentObj.transform.position, out hit, 4, NavMesh.AllAreas))
        {
            if (Mathf.Approximately(agentObj.transform.position.x, hit.position.x) && 
                Mathf.Approximately(agentObj.transform.position.z, hit.position.z))
            {
                return agentObj.transform.position.y >= hit.position.y;
            }
        }

        return false;
    }


    public override float CalculateWeight()
    {
        return weightValue;
    }



   
}
