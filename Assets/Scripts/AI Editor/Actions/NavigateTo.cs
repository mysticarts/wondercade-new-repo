﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

//for navigating to the arcade entrance
public class NavigateTo : Action
{
    public List<Transform> points = new List<Transform>();
    public int iter = 0;
    public bool ResetPath = false;
    private AIStats stats;

    public override void Start()
    {
        stats = agentObj.GetComponent<AIStats>();
    }
    public override void OnEnter()
    {

    }
    public override BehaviourResult execute()//execute behaviour
    {
       //has navigation points
        if (points.Count > 0)
        {        
            if (iter < points.Count) //iterator hasnt pass bounds of list
            {
                if (navigation.path.status != UnityEngine.AI.NavMeshPathStatus.PathInvalid) { //if the path is valid
                navigation.SetDestination(points[iter].position);//navigate to
                }

                if (Vector3.Distance(agentObj.transform.position, points[iter].position) < 1) //has reached its point
                {
                    tree.SetBool(false, "Navigate"); //set behaviour parameter

                    if (stats.wallet == 0) //if its wallet balance is nothing
                    {
                        tree.SetBool(false, "Entered Arcade"); //set behaviour parameters
                        tree.SetBool(false, "Navigate Arcade");
                        return result = BehaviourResult.SUCCESS;
                    }


                    if (stats.cycle.isDay && tree.Test("Morale")) //if its currently the day and its morale parameter is met
                    {
                        stats.cycle.customers.Add(stats); //add itself to the known customers
                        tree.SetBool(true, "Entered Arcade");
                        stats.buildingManager.currentCapacity++; //increase AI in building count
                    }
                    else //if morale was low enough when it exit
                    {
                        tree.SetBool(false, "Entered Arcade"); 
                        tree.SetBool(false, "Navigate Arcade");      
                        stats.leftStoreUnsatisfied = true;
                        stats.buildingManager.currentCapacity--; //decrease building capacity
                    }
                    iter++; //increase iter
                    if (iter >= points.Count && ResetPath) //if the reset path option is enabled
                    {
                        iter = 0;
                    }

                    return result = BehaviourResult.SUCCESS;
                }
                if (Vector3.Distance(agentObj.transform.position, points[iter].position) > 1) //if havent reached destination point
                {
                    return result = BehaviourResult.RUNNING;
                }
            }
         
        }


        return result = BehaviourResult.FAILURE;
    }
    public override float CalculateWeight()//calc weight score
    {
        return 0;
    }

    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(iter) + iter.ToString());
        base.SaveData(ref data);

    }

    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(iter), out iter);
        base.LoadData(data);
    }
}
