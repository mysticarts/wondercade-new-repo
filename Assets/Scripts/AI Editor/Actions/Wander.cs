﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;

public class Wander : Action
{
    public Vector3 wanderPos; //the current destination on the nav mesh the agent wants to navigate to.
    public float minDist = 0; //the minimal distance the wander position from the agent has to be.
    public float radius = 0; //the radius the agent calculates a random position in.
    private bool foundPosition = false; //check if position is ffound
    private bool delay = false; //check if its in waiting delay.
    private float currentDelayTimer = 0; 
    public float delayTimer = 0; //the delay period
    public float edgeDistance = 0; //how far the position has to be away from the edge of the nav mesh.
    public float dist = 0;

    public override void Start()
    {
    }

    public override void OnEnter()
    {

    }
    private bool CalculateRandomPos(Vector3 centre, float radius, out Vector3 result, NavMeshAgent agent)
    {
        //calculate a random point within a sphere of size radius
        Vector3 randomPoint = centre + Random.insideUnitSphere * radius;
        NavMeshHit hit;

        if (NavMesh.SamplePosition(randomPoint,out hit, radius, 1 << NavMesh.GetAreaFromName("Arcade"))) //sample a positon from the Arcade area
        {
            if (Vector3.Distance(randomPoint,hit.position) > minDist) //its far enough
            {
                NavMeshPath path = new NavMeshPath();
                agent.CalculatePath(hit.position, path);
                if (path.status != NavMeshPathStatus.PathPartial) //is it possible to reach position
                {
                    result = hit.position; //passout the sampled position.
                    return true;
                }
            }           
        }

        result = Vector3.zero; //no conidtions met, return 0 and false
        return false;
    }

    public bool NearEdge(Vector3 centre)
    {
        NavMeshHit hit;
        if (NavMesh.FindClosestEdge(wanderPos, out hit, 1 << NavMesh.GetAreaFromName("Arcade"))) //sample the closest edge from the calculated wander position.
        {
            if (Vector3.Distance(wanderPos, hit.position) > edgeDistance) //is sampled edge far enough away
            {
                return true;

            }
        }
        return false;
    }

    public override BehaviourResult execute()//execute behaviour
    {       
        if (!foundPosition && !delay) //if no position is found and there is no delay period
        {
            if (CalculateRandomPos(agentObj.transform.position, radius, out wanderPos, navigation)) //calculate random wander position
            {
               if (NearEdge(wanderPos)) //check if wander position is on an edge
               {
                    if (wanderPos != Vector3.zero)
                    {
                        navigation.SetDestination(wanderPos); //set destination
                        foundPosition = true; //destination fround
                        return result = BehaviourResult.SUCCESS;
                    }

               }        
            }
        }
       
        if (foundPosition && !delay)
        {
            dist = Vector3.Distance(agentObj.transform.position, wanderPos);
            if (navigation.destination != wanderPos)
            {
                foundPosition = false;
                
                return result = BehaviourResult.FAILURE;
            }
            if (Vector3.Distance(agentObj.transform.position, wanderPos) > 3) //if agent is far enough from wander position
            {
                return result = BehaviourResult.SUCCESS;
            }
            else
            {
                delay = true; //begin delay period
                foundPosition = false;
            }
        }

        if (delay)
        {
            currentDelayTimer += Time.deltaTime % 60;
            if (currentDelayTimer > delayTimer)
            {
                currentDelayTimer = 0;
                delay = false;
            }
            return result = BehaviourResult.RUNNING;
        }


        foundPosition = false;
        delay = false;
        return result = BehaviourResult.FAILURE;
    }

    public override float CalculateWeight()
    {
        return weightValue;
    }

    //Save Game
    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(foundPosition) + foundPosition.ToString());
        bData.fieldData.Add(nameof(delay) + delay.ToString());
        bData.fieldData.Add(nameof(currentDelayTimer) + currentDelayTimer.ToString());
        bData.fieldData.Add("WX" + wanderPos.x.ToString());
        bData.fieldData.Add("WY" + wanderPos.y.ToString());
        bData.fieldData.Add("WZ" + wanderPos.z.ToString());

        base.SaveData(ref data);

    }

    //Load Game
    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(foundPosition), out foundPosition);
        AssignLoadedFieldData(nameof(delay), out delay);
        AssignLoadedFieldData(nameof(currentDelayTimer), out currentDelayTimer);

        AssignLoadedFieldData("WX", out wanderPos.x);
        AssignLoadedFieldData("WY", out wanderPos.y);
        AssignLoadedFieldData("WZ", out wanderPos.z);    
   
        base.LoadData(data);

    }
}
