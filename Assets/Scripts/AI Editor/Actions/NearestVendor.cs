﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;
using Pixelplacement;

public class NearestVendor : Action
{
    public Transform chosenVendorPos; //chosen vendor transform
    public Vendor chosenVendor; //chosen vendor
    public string chosenVendorID; 
    private Vendor vendorToIgnore; //the vendor to ignore (based on whether it looked at a vendor that cost to much)
    public AIStats stats;
    private AIAnimationHandler anim;
    public VendorType desiredVendor; //desired vendor type
    private bool lookAt = false;

    public override void Start()
    {
        stats = agentObj.GetComponent<AIStats>();
        anim = agentObj.GetComponentInChildren<AIAnimationHandler>();
    }
    /// <summary>
    /// executes upon entering the behaviour
    /// </summary>
    public override void OnEnter()
    {
        
    }
    public bool FindNearestVendor(Transform origin, out Transform target)
    {
        Vendor[] vendors = FindObjectsOfType<Vendor>(); //fetch all known vendors

        float minDist = Mathf.Infinity;
        target = null;
        foreach (var vendor in vendors)
        {
            //if the vendor matches its desire, isnt occupied, has stock and is not currently being moved in edit mode.
            if (vendor.itemType == ItemType.VENDOR && desiredVendor == vendor.vendorType && vendor.stock > 0 && !vendor.occupied && vendor.transform.parent == null)
            {
                if (!CostToHigh(vendor) && stats.wallet >= vendor.ussageCost) //if it doesnt cost to much and it can afford it
                {
                    float dist = Vector3.Distance(origin.position, vendor.transform.position);
                    if (dist < minDist) //if looked at vendor is closer than the previous
                    {
                        target = vendor.AIStandingPos;
                        chosenVendor = vendor;
                        minDist = dist;
                    }
                }
               
            }
        }

        if (target != null)
        {
            tree.SetBool(true, "Vending");
            //tree.SetBool(true, "Vendor Found");
            anim.currentVendor = chosenVendor;
            return true;
        }

        return false;
    }

    public override BehaviourResult execute()//execute behaviour
    {      
        if (chosenVendorPos == null) //no vendor is chosen
        {
            if (FindNearestVendor(agentObj.transform, out chosenVendorPos)) //Look for one
            {
                navigation.SetDestination(chosenVendorPos.position); //set the destination
                return result = BehaviourResult.RUNNING;
            }
        }

        if (chosenVendorPos != null)
        {
            chosenVendorID = chosenVendor.InstanceID;
            navigation.SetDestination(chosenVendorPos.position);
            tree.SetBool(true, "Chosen Action");

            if (chosenVendor.occupant.Length > 0 && chosenVendor.occupant != stats.ID) //if another AI made it to machine before it, continue looking for another
            {
                chosenVendorPos = null;
                return BehaviourResult.RUNNING;
            }


            if (Vector3.Distance(agentObj.transform.position, chosenVendorPos.position) < 2) //if its close enough to view
            {
                agentObj.transform.LookAt(new Vector3(chosenVendor.transform.position.x, agentObj.transform.position.y, chosenVendor.transform.position.z)); //aim ai at the machine
                chosenVendor.occupant = stats.ID;
                //if the ai is facing the machine
                if (!lookAt)
                {
                    stats.economySystem.Give(chosenVendor.ussageCost); //give the player money
                    //Tween.LookAt(agentObj.transform, chosenVendor.transform, agentObj.transform.up, 0.5f, 0);
                    lookAt = true;
                }
                anim.animator.SetBool("Wandering", false);

                SetVendorAnimation(true); //set the vendor animation

                chosenVendor.customer = controller; //set the vendors customer

                if (anim.replenish) //when the animation event
                {
                    vendorToIgnore = null; //no longer looking for a vendor
                    chosenVendorPos = null;
                    tree.SetBool(false, "Vending");
                    if (desiredVendor == VendorType.DRINK)
                    {
                        stats.cycle.currentDayStats.drinkSales += chosenVendor.ussageCost; //update stats
                        stats.thirst = 0;
                    }
                    if (desiredVendor == VendorType.FOOD)
                    {
                        stats.cycle.currentDayStats.foodSales += chosenVendor.ussageCost; //update stats
                        stats.hunger = 0;
                    }
                    stats.wallet -= chosenVendor.ussageCost; //deduct cost from AI
                    chosenVendor.TakeStock(); //decrease stock amount
                    anim.animator.SetBool("Wandering", true); 
                    tree.SetBool(true, "Wandering");
                    anim.replenish = false; 
                    lookAt = false;
                    SetVendorAnimation(false);
                    chosenVendor.occupied = false; //no longer occupied
                    chosenVendor.occupant = "";
                    chosenVendor = null;
                    anim.currentVendor = null;
                    return result = BehaviourResult.SUCCESS;
                }
            }
            return result = BehaviourResult.RUNNING;
        }
     //   tree.SetBool(false, "Chosen Action");

        return result = BehaviourResult.FAILURE;
    }

   
    void SetVendorAnimation(bool status)
    {
        //set the
        if (desiredVendor == VendorType.DRINK)
        {
            anim.animator.SetBool("Using Soda Machine Round", status);
        }
        if (desiredVendor == VendorType.FOOD)
        {
            if (chosenVendor.name.Contains("Food Vending")) //is a standard food vending machine.
            {
                //is meant to be called food machine but due to poor communication and a rushed schedual it was never renamed to food.
                anim.animator.SetBool("Using Soda Machine Square", status); //Using Food Machine Squared
            }
            if (chosenVendor.name.Contains("Popcorn"))
            {
                anim.animator.SetBool("Using Popcorn Machine", status);
            }
        }
    }



    public override float CalculateWeight()//calc weight score
    {    
        if (desiredVendor == VendorType.DRINK)
        {
            weightValue = stats.thirst;
        }
        if (desiredVendor == VendorType.FOOD)
        {
            weightValue = stats.hunger;
        }

        return weightValue;
    }

    public bool CostToHigh(Vendor vendor)
    {
        if (vendor.ussageCost == 0) //its free, so doesnt cost too much
        {
            return false;
        }

        if (vendorToIgnore != null)
        {
            if (vendor == vendorToIgnore) //if ignoring this machine
            {
               // Debug.Log("Already looked at this, Price is to high");
                return true;
            }
        }
        float sum = stats.wallet; //set sum to wallet
        float prob = (sum / vendor.ussageCost) * (vendor.currentLevel * 2); //calculate the probability of buying stock based on the cost and current level

        float dice = Random.Range(0, sum); //rolling dice

        if (prob > dice) //if probability of buying is greater than dice value
        {
    //        Debug.Log(vendor.name + " is a good value" + " , " + "prob = " + prob + " , " + "dice = " + dice);
            return false;
        }

    //    Debug.Log(vendor.name + " costs too much" + " , " + "prob = " + prob + " , " + "dice = " + dice);

        vendorToIgnore = vendor;
        return true;
    }
    //Save Data
    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(desiredVendor) + (int)desiredVendor);

        if (chosenVendorPos != null)
        {
            bData.fieldData.Add(nameof(chosenVendorID) + chosenVendorID);

            bData.fieldData.Add(nameof(chosenVendorPos) + "x" + chosenVendorPos.position.x.ToString());
            bData.fieldData.Add(nameof(chosenVendorPos) + "y" + chosenVendorPos.position.y.ToString());
            bData.fieldData.Add(nameof(chosenVendorPos) + "z" + chosenVendorPos.position.z.ToString());
        }

        base.SaveData(ref data);
    }

    //Load Data
    public override void LoadData(AIData data)
    {
        int venType;
        AssignLoadedFieldData(nameof(desiredVendor), out venType);
        desiredVendor = (VendorType)venType;

        Vector3 vendorPos;

        AssignLoadedFieldData(nameof(chosenVendorPos) + "x", out vendorPos.x);
        AssignLoadedFieldData(nameof(chosenVendorPos) + "y", out vendorPos.y);
        AssignLoadedFieldData(nameof(chosenVendorPos) + "z", out vendorPos.z);

        AssignLoadedFieldData(nameof(chosenVendorID), out chosenVendorID);

        Vendor[] vendors = FindObjectsOfType<Vendor>();

        foreach (var vendor in vendors)
        {
            if (vendor.InstanceID == chosenVendorID)
            {
                chosenVendor = vendor;
                chosenVendorPos = vendor.AIStandingPos;
                chosenVendor.customer = controller;
                vendor.occupied = true;  
            }
        }


        base.LoadData(data);
    }
}
