﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tree.Editor
{
    public abstract class Action : _Behaviour
    {

        public Selector selector; //the attached selector node. (If attached.)

        public float weightValue = 0; //the calculated weight value.

        public float probability = 0; //used for selector nodes. Determines the chances of a this behaviour being picked based on the calculated weight value.

        public Condition condition; //a reference to a condition script (if the previous node is a condition, this will be assigned to it. else it will never be assigned)

        public abstract override void Start();

        public abstract override BehaviourResult execute();//execute behaviour

        public abstract override float CalculateWeight();//calc weight score

        public abstract override void OnEnter();
    }
}


