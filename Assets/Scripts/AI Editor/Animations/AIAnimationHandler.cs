﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;

[System.Serializable]
public struct AIAnimationData
{
    public List<string> animParamNames;
    public List<string> animParamValue;
    public List<string> animParamType;
    public string currentAnimation;
    public float currentAnimationTime;
}



public class AIAnimationHandler : MonoBehaviour
{
    public AIController controller;
    public AIAnimationData animData;
    public Animator animator;
    public NavMeshAgent agent;
    public List<string> states = new List<string>(); //the state names available in the animator
    private float velocityX = 0; //blend tree y
    private float velocityY = 0; //blend tree x
    [Tooltip("Used to scale the velocity threshold for changing movement animation.")]
    public float scalar = 0;
    public Vector3 velocity; //AI's velocity
    private Vector3 lastPosition; //previous position of the AI
    private Vector3 rotationLast; //previous rotation of the AI
    public Vector3 angularVelocity; //angular velocity of the AI
    public AIStats stats;

    [HideInInspector]
    public bool replenish = false; //Used in animation event for replenishing the AI's hunger and thirst
    private bool gameSaveLoaded = false;
    public Transform itemHoldTransform; //where the items are held.
    [HideInInspector]
    public Vendor currentVendor;

    // Start is called before the first frame update
    void Start()
    {
        if (!gameSaveLoaded) //if a game save wasnt loaded
        {
            animator.SetBool("Wandering", true);
        }
        rotationLast = agent.transform.rotation.eulerAngles;
        lastPosition = agent.transform.position;
        
    }

    void Update()
    {
        if (!controller.tree.Test("Morale")) //if morale is below parameter threshold
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Movement")) //play movement animation if its currently not
            {
                animator.Play("Movement", 0);
            }
        }

        //Calculate velocity
        velocity = agent.transform.position - lastPosition;
        lastPosition = agent.transform.position;

        //if velocity is high enough
        if ((velocity.magnitude * scalar) > 4)
        {
            velocityY = velocity.magnitude * scalar; //set blend tree value
        }
        else
        {
            velocityY = 0;
        }

        //Calculate angular velocity
        angularVelocity = agent.transform.rotation.eulerAngles - rotationLast;
        rotationLast = agent.transform.rotation.eulerAngles;

        //if angular velocity is high enough
        if ((angularVelocity.magnitude) > 2.5f)
        {
            velocityX = angularVelocity.magnitude;
        }
        else
        {
            velocityX = 0;
        }

        //clamp values
        velocityX = Mathf.Clamp(velocityX, -1, 1);
        velocityY = Mathf.Clamp(velocityY, -1, 1);

        //set blend tree values
        animator.SetFloat("BlendY", velocityX);
        animator.SetFloat("BlendX", velocityY);
    }


    //Animation Event
    public void DispenseItem()
    {
        if (currentVendor != null)
        {
            AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(0);
            float animationLength = currentState.length - currentState.normalizedTime; //get the length left of the current animation
            GameObject dispensedItem = null;
            Destroy(dispensedItem = Instantiate(currentVendor.vendorItem, itemHoldTransform.position, itemHoldTransform.rotation), animationLength); //Spawn item and destroy when animation ends

            dispensedItem.transform.SetParent(itemHoldTransform);


        }
    }


    //Animation Event.
    public void Replenish()
    {
        replenish = true;
    }

    //get the parameter values
    public string GetParameterValue(AnimatorControllerParameter param)
    {
        if (param.type == AnimatorControllerParameterType.Int)
        {
            animData.animParamType.Add("Int");
            return animator.GetInteger(param.name).ToString();
        }
        if (param.type == AnimatorControllerParameterType.Float)
        {
            animData.animParamType.Add("Float");
            return animator.GetFloat(param.name).ToString();
        }
        if (param.type == AnimatorControllerParameterType.Bool)
        {
            animData.animParamType.Add("Bool");
            return animator.GetBool(param.name).ToString();
        }
        return "";
    }

    public void SaveData(ref AIData data)
    {
        animData.animParamNames = new List<string>();
        animData.animParamValue = new List<string>();
        animData.animParamType = new List<string>();

        AnimatorControllerParameter[] param_s = animator.parameters; //get all the parameters

        foreach (var param in param_s)
        {
            animData.animParamNames.Add(param.name); //add their names
            animData.animParamValue.Add(GetParameterValue(param)); //add there values
        }

        AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(0); //get the current state
        foreach (var state in states) //loop through all the states to test which is the current animation
        {
            if (currentState.IsName(state)) //if the state matches the current state
            {
                animData.currentAnimation = state; //save it
                animData.currentAnimationTime = currentState.normalizedTime; //save its current play time
            }
        }
        data.animationData = animData;
    }

    public void LoadData(AIAnimationData data)
    {
        gameSaveLoaded = true;
        for (int i = 0; i < data.animParamNames.Count; i++) //loop through all the animators parameters and assign their values
        {
            if (data.animParamType[i] == "Int")
            {
                int value = 0;
                if (int.TryParse(data.animParamValue[i], out value))
                {
                    animator.SetInteger(data.animParamNames[i], value);
                }
            }
            if (data.animParamType[i] == "Float")
            {
                float value = 0;
                if (float.TryParse(data.animParamValue[i], out value))
                {
                    animator.SetFloat(data.animParamNames[i], value);
                }
            }
            if (data.animParamType[i] == "Bool")
            {
                bool value = false;
                if (bool.TryParse(data.animParamValue[i], out value))
                {
                    animator.SetBool(data.animParamNames[i], value);
                }
            }
        }

        if (data.currentAnimationTime != 0) //set the correct animation time
        {
            animator.Play(data.currentAnimation, 0, data.currentAnimationTime);
        }
    }

    //Animation Event (Called at the end of the animation)
    public void AngryReact()
    {
        ChooseArcade chooseArcade = (ChooseArcade)controller.tree.GetBehaviour("Find&Play Game");

        //return to wandering
        if (chooseArcade != null)
        {
            chooseArcade.angryReact = false;
            animator.SetBool("Angry", false);
            animator.SetBool("Wandering", true);
            chooseArcade.chosenArcade = null;
            chooseArcade.chosenArcadePos = null;
            controller.tree.SetBool(false, "Chosen Action");
        }
    }



}
