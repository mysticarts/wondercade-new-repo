﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tree.Editor
{
    public enum SocketType
    {
        Input = -1,
        Output = 1
    }
    [System.Serializable]
    public class Socket
    {   
        public List<NodeData> connection = new List<NodeData>(); //the connected nodes
        public NodeData attachedNode; //the node the socket is attached to
        public NodeData previousNode; //the previous node the its connected to.
        public Socket previousSocket; //the socket attached to the previous node.
        public SocketType type; //its type
        [HideInInspector]
        public int id = 0;
        //only use in editor
#if UNITY_EDITOR
        private GUIStyle displayStyle = new GUIStyle();
        public Rect display; //the display

        public void MakeConnection(NodeData node1, NodeData node2)
        {
            node1.SelectedSocket.connection.Add(node2); //add connection to the node.
            previousSocket = node1.SelectedSocket; //assign the previous socket to the node.
            this.previousNode = node1; //assign previous node
        }


        public void Draw()
        {

            if (previousNode != null && previousSocket == null) //Error Checking
            {
                previousSocket = previousNode.sockets[1]; //set the previous socket to the previous nodes output node. This is done due to previous socket reference being set null when entering playmode.
            }
            if (type == SocketType.Input)
            {
                display = new Rect(attachedNode.displayWindow.position.x - 20, attachedNode.displayWindow.position.y + attachedNode.displayWindow.height / 2, 20, 10); //Draw on the left
                GUI.color = Color.black;
               //
                
            }
            if (type == SocketType.Output)
            {
                display = new Rect(attachedNode.displayWindow.position.x + attachedNode.displayWindow.width, attachedNode.displayWindow.position.y + attachedNode.displayWindow.height / 2, 20, 10); //Draw on the right
                GUI.color = Color.white;
            }

            if (GUI.Button(display, "")) //if the button is pressed
            {
                if (type == SocketType.Output) //if its an output socket
                {
                    attachedNode.editorWindow.currentSelectedNode = attachedNode; //set the current node to the attached node of the button clicked.
                }
                if (attachedNode.editorWindow.currentSelectedNode != null) //if a current node is selected
                {
                    if (attachedNode.editorWindow.currentSelectedNode.startConnection) //if a connection has been started.
                    {
                        //if type is an input socket, and the user is attempting to connect a selector or condition node to a selector.
                        if (type == SocketType.Input && attachedNode.editorWindow.currentSelectedNode.nodeType == NodeType.Selector && (attachedNode.nodeType == NodeType.Condition || attachedNode.nodeType == NodeType.Selector))
                        {
                            //stop connection and exit.
                            previousNode = null;
                            attachedNode.editorWindow.currentSelectedNode.startConnection = false;
                            attachedNode.editorWindow.currentSelectedNode = null;
                            return;
                        }
                        //if an input node
                        if (type == SocketType.Input)
                        {
                            //a previous node is already assigned.
                            if (previousNode != null)
                            {
                                //remove connections
                                foreach (var socket in previousNode.sockets)
                                {
                                    if (socket.connection.Contains(attachedNode))
                                    {
                                        socket.connection.Remove(attachedNode);
                                    }
                                }
                            }
                            previousNode = null;
                            //assign new connection
                            MakeConnection(attachedNode.editorWindow.currentSelectedNode, this.attachedNode);
                        }

                        //stop connection
                        attachedNode.editorWindow.currentSelectedNode.startConnection = false;
                        attachedNode.editorWindow.currentSelectedNode = null;
                    }
                    else if (type == SocketType.Output)
                    {
                        //Only root and selector nodes can have multiple connections
                        if (attachedNode.nodeType == NodeType.Root || attachedNode.nodeType == NodeType.Selector)
                        {
                            StartConnection();
                        }
                        //actions and connections can only have one.
                        else if (attachedNode.nodeType != NodeType.Root && connection.Count == 0)
                        {
                            StartConnection();
                        }
                    }
                    else if (previousSocket != null && type == SocketType.Input) //if this socket is connected to a previous socket and is an input.
                    {
                        previousSocket.attachedNode.editorWindow.currentSelectedNode = previousSocket.attachedNode; //remove connections
                        previousSocket.connection.Remove(attachedNode);
                        previousNode = null; //remove previous references.
                        previousSocket = null; 
                        attachedNode.editorWindow.currentSelectedNode.startConnection = true; //open connection. (Reassign connections)
                    }
                }
            }

            //draw curves between connections
            if (previousNode != null && previousSocket != null)
            {
                attachedNode.editorWindow.UpdateConnectionCurve(previousSocket.display, display,attachedNode, previousSocket.connection);
            }

        }

        void StartConnection()
        {
            attachedNode.startConnection = true;//start connection
            attachedNode.SelectedSocket = this; //set the selected socket to this socket.
            attachedNode.editorWindow.currentSelectedNode = attachedNode; //set the global selected node
        }
#endif

    }
}


