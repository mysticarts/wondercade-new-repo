﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tree.Editor
{
    public abstract class Condition : _Behaviour
    {
        public abstract override void Start();
        public abstract override BehaviourResult execute();//execute behaviour
        public abstract override float CalculateWeight();//calc weight score
        public abstract override void OnEnter();
    }
}

