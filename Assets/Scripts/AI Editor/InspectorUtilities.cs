﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR

namespace Tree.Editor
{
    public class InspectorUtilities
    {
        public static void BehaviourPopUpMenu(TreeEditorWindow editorWindow)
        {
            string[] folders = new string[]
                    {
                        "Assets/Scripts/AI Editor/Conditions", "Assets/Scripts/AI Editor/Actions",
                    };


            string[] behavioursAddress = AssetDatabase.FindAssets("", folders);
            List<string> behaviours = new List<string>();

            behaviours.Add("None");
            for (int i = 0; i < behavioursAddress.Length; i++)
            {
                behavioursAddress[i] = AssetDatabase.GUIDToAssetPath(behavioursAddress[i]);
                if (behavioursAddress[i].Contains(folders[0]))
                {
                    behavioursAddress[i] = behavioursAddress[i].Remove(0, folders[0].ToCharArray().Length + 1);
                    behavioursAddress[i] = behavioursAddress[i].Remove(behavioursAddress[i].Length - 3);
                }
                if (behavioursAddress[i].Contains(folders[1]))
                {
                    behavioursAddress[i] = behavioursAddress[i].Remove(0, folders[1].ToCharArray().Length + 1);
                    behavioursAddress[i] = behavioursAddress[i].Remove(behavioursAddress[i].Length - 3);
                }
                behaviours.Add(behavioursAddress[i]);
            }

            editorWindow.currentSelectedNode.selected = EditorGUILayout.Popup("Behaviours", editorWindow.currentSelectedNode.selected, behaviours.ToArray());

            if (editorWindow.currentSelectedNode.selected != 0)
            {
                if (editorWindow.currentSelectedNode.behaviour == null)
                {

                    editorWindow.currentSelectedNode.behaviour = BehaviourMenus.CreateBehaviour(behaviours[editorWindow.currentSelectedNode.selected], editorWindow.tree);
                    //SerializedObject so = new SerializedObject(currentSelectedNode.behaviour);

                }
                if (editorWindow.currentSelectedNode.behaviour != null)
                {
                    if (editorWindow.currentSelectedNode.behaviour.GetType().Name != behaviours[editorWindow.currentSelectedNode.selected])
                    {
                        editorWindow.currentSelectedNode.behaviour = BehaviourMenus.CreateBehaviour(behaviours[editorWindow.currentSelectedNode.selected], editorWindow.tree);
                        //  SerializedObject so = new SerializedObject(currentSelectedNode.behaviour);

                    }
                }
            }
            else
            {
                editorWindow.currentSelectedNode.behaviour = null;
            }
        }

        public static void DrawFields(TreeEditorWindow editorWindow)
        {
            if (editorWindow.currentSelectedNode.behaviour != null) //if there is a behaviour assigned to the current Node
            {
                var variables = editorWindow.currentSelectedNode.behaviour.GetType().GetFields(); //Get all the variables in the class
                
                SerializedObject serializedObject = new SerializedObject(editorWindow.currentSelectedNode.behaviour); //create a new serialized object
                for (int i = 0; i < variables.Length; i++)
                {
                    SerializedProperty serializedProperty = serializedObject.FindProperty(variables[i].Name); //create a new serialized property  from the current field
                    EditorGUILayout.PropertyField(serializedProperty,true); //create the property field
                }
                serializedObject.ApplyModifiedProperties(); //apply any changes made
            }
        }

    }
}


#endif