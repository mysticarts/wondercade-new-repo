﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;
using Pixelplacement;


//Data to Save.
[System.Serializable]
public struct AIData
{
    public string objID;
    public string firstName;
    public string lastName;
    public Vector3 position;
    public Vector3 rotation;
    public Vector4 color;
    public int wallet;
    public float morale;
    public float hunger;
    public float thirst;
    public float boredom;
    public float wanderTime;
    public string AIType;
    public bool inStore;
    public string instanceID;
    public Vector3 velocity;
    public List<BehaviourData> behavioursData;
    public List<ParameterData> parameterData;
    public AIAnimationData animationData;
    public AIOutFitData outfitData;
}

public enum AIType
{ 
    KID = 0,
    ADULT = 1
}


public class AIStats : MonoBehaviour
{
    public bool car = false; //if the ai is a car
    public AIAppearance appearanceGenerator; //the outft generator
    public AIAnimationHandler animation; //Animation handler
    [HideInInspector]
    public string ID; //the assigned ID of the AI
    [HideInInspector]
    public AISpawning spawnManager; //AI game saves.
    public AIType aiType;
    [HideInInspector]
    public AIController controller;
    public DayNightCycle cycle;
    [HideInInspector]
    public AIData aData = new AIData();
    [Range(0,100)]
    [Tooltip("The current value of the thirst meter")]
    public float thirst = 0;
    [Range(0, 100)]
    [Tooltip("The current value of the hunger meter")]
    public float hunger = 0;
    [Range(0, 100)]
    [Tooltip("The current value of the boredom meter")]
    public float boredom = 0;
    [Tooltip("The current value of the cleanliness satisfaction meter")]
    public float cleanliness = 100;
    [Tooltip("The amount of money this AI has")]
    public int wallet = 0;
    [Tooltip("The maximum amount of money this AI can hold")]
    public int maxWalletCapacity = 0;
    [HideInInspector]
    public Economy economySystem;
    [Header("----Moral Values----")]
    [Tooltip("The current value of the moral meter")]
    [Range(0,100)]
    public float morale = 0; //starting morale 100
    private float maxMorale = 0;
    [Tooltip("Moral Lost Rate")]
    public float mlr = 0;
    [Range(0, 100)]
    public float moraleGainRange = 0;
    [Range(0, 100)]
    public float moraleLossRange = 0;
    private float mlrTimer = 0; //morale lost rate timer

    public int gamesPlayed = 0;

    [Header("----Utility OverTime----")]
    [Tooltip("The rate at which the AI gets hungry over time")]
    [Range(0,100)]
    public float hungerOverTime = 0;
    [Tooltip("The rate at which the AI gets thirsty over time")]
    [Range(0, 100)]
    public float thirstOverTime = 0;

    [Tooltip("The minimum value required for AI to get food")]
    public float hungerThreshold = 0;
    [Tooltip("The minimum value required for AI to get drinks")]
    public float thirstThreshold = 0;
    [Tooltip("The minimum value required for AI to get bored")]
    public float boredomThreshold = 0;
    [Tooltip("The minimum value required for AI to be happy")]
    public float happyThreshold = 0;
    public float angryThreshold = 0;

    [Tooltip("The morale lost in dirty rooms")]
    public float moraleLossMultiplier = 0;
    [Tooltip("the morale lost when doing an activity")]
    public float moraleLossScalar = 0;
    public LayerMask mask;

    public bool goToStore = false; //determines whether to go to the store or not
    public bool leftStoreUnsatisfied = false; //if its left the store on 0 morale or below the angry threshold.
    private GameObject frontEntrance; //the object that represents the
    [HideInInspector]
    public BuildingManager buildingManager;

    [HideInInspector]
    public float wanderTimer = 0;
    
    public MenuUI_Customer customerMenu;

    public string firstName = "";
    public string lastName = "";
    private NotificationSystem notifications;
    private Tutorial tutorial;

    //choose a random first name based on its gender
    public string FirstName() {
        if(appearanceGenerator.gender == Gender.MALE)
            return spawnManager.firstNames_Male[UnityEngine.Random.Range(0, spawnManager.firstNames_Male.Count)];
        else return spawnManager.firstNames_Female[UnityEngine.Random.Range(0, spawnManager.firstNames_Female.Count)];
    }
    //choose a random last name
    public string LastName() {
        return spawnManager.lastNames[UnityEngine.Random.Range(0, spawnManager.lastNames.Count)];
    }


    // Start is called before the first frame update
    public void Start()
    {
        frontEntrance = FindObjectOfType<DoorSensor>().gameObject;
        tutorial = FindObjectOfType<Tutorial>();
        ID = "AI" + GetInstanceID(); //assign ID
        if (controller == null)
        {
            controller = GetComponent<AIController>();
        }
        if (!car) //if its not a car
        {
            economySystem = FindObjectOfType<Economy>();
            cycle = FindObjectOfType<DayNightCycle>();
          
            controller.tree.SetFloat(morale, "Morale");
            maxMorale = morale;
            notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();

            buildingManager = FindObjectOfType<BuildingManager>();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!car)
        {

            if (controller != null)
            {
                //if the AI has enough money and and its currently day and it hasnt left the store unsatisfied and its walking past the front entrance and no tutorial is active.
                if (cycle.isDay && !leftStoreUnsatisfied && wallet > 0 && buildingManager.currentCapacity < buildingManager.buildingCapacity && Vector3.Distance(transform.position, frontEntrance.transform.position) < 5
                    && !cycle.customers.Contains(this) && !tutorial.sectionActive)
                {
                    if (!goToStore) //navigate to the store.
                    {
                        goToStore = true;
                        controller.tree.SetBool(true, "Navigate");
                    }
                    controller.tree.SetBool(true, "Navigate Arcade");

                }
                //it its no longer day and havent left the store or has ran out of money and hasnt chosen an action (i.e. playing arcade)
                if (!cycle.isDay && !leftStoreUnsatisfied || wallet <= 0 && controller.tree.Test("Chosen Action"))
                {
                    if (goToStore)
                    {
                        foreach (var param in animation.states)
                        {
                            animation.animator.SetBool(param, false); //exit all animations
                        }
                        animation.animator.SetBool("Wandering", true); //enter wandering animation
                        controller.tree.SetBool(true, "Navigate");
                        controller.tree.SetBool(true, "Leaving");
                        goToStore = false;
                        buildingManager.currentCapacity--;
                    }
                }

                //if was was walking towards the entrance but another AI entered before it and the building capacity limit was reached. Continue wandering.
                if (!controller.tree.Test("Navigate Arcade") && buildingManager.currentCapacity == buildingManager.buildingCapacity && !cycle.customers.Contains(this))
                {
                    controller.tree.SetBool(false, "Navigate");
                    controller.tree.SetBool(false, "Navigate Arcade");
                    goToStore = false;
                }

                if (controller.tree.GetContinousBool("Entered Arcade")) //if it has entered the arcade
                {
                    controller.tree.SetFloat(wanderTimer, "Wander Time"); //set wander time parameter

                    //if a inside the arcade and havent chosen an action
                    if (controller.tree.Test("Entered Arcade") && controller.tree.Test("Chosen Action"))
                    {
                        controller.tree.SetBool(true, "Wandering"); //wander
                    }
                    else
                    {
                        controller.tree.SetBool(false, "Wandering"); //stop wandering
                    }
                    //if all thresholds have been crossed
                    if (hunger >= hungerThreshold || thirst >= thirstThreshold || boredom >= boredomThreshold)
                    {
                        if (InDirtyRoom()) //if in a dirty room
                        {
                            morale -= ReconfigureMorale(moraleLossRange) * moraleLossMultiplier; //lose morale faster
                            cleanliness = 0;
                        }
                        else
                        {
                            if (!controller.tree.Test("Chosen Action")) //if chosen an action
                            {
                                morale -= (ReconfigureMorale(moraleLossRange) / (moraleLossScalar - (cleanliness/100))); //lose morale slower
                            }
                            else
                            {
                                morale -= ReconfigureMorale(moraleLossRange); //lose morale at normal rate
                            }

                        }
                    }

                    if (controller.tree.Test("Wandering") && controller.tree.Test("Chosen Action")) // if wandering and havent chosen an action
                    {
                        if (hunger <= hungerThreshold || thirst <= thirstThreshold || boredom <= boredomThreshold) //any stats below threshold
                        {
                            morale += ReconfigureMorale(moraleGainRange); //gain morale
                        }
                        //add to wander time and stats meters
                        wanderTimer += Time.deltaTime % 60;
                        boredom += Time.deltaTime * 2;
                        thirst += thirstOverTime * Time.deltaTime / 2;
                        hunger += hungerOverTime * Time.deltaTime / 2;
                    }

                    if (controller.tree.Test("Wander Time")) //if crossed the wander time threshold
                    {
                        controller.tree.SetBool(true, "Find Arcade"); //begin to find an arcade
                    }

                    //clamp meters
                    thirst = Mathf.Clamp(thirst, 0, 100);
                    hunger = Mathf.Clamp(hunger, 0, 100);

                    //if meters are above their thresholds. Set behaviour parameters.
                    if (hunger >= hungerThreshold)
                    {
                        controller.tree.SetBool(true, "Need Food");
                    }
                    else
                    {     
                        controller.tree.SetBool(false, "Need Food");
                    }

                    if (thirst >= thirstThreshold)
                    {

                        controller.tree.SetBool(true, "Need Drink");
                    }
                    else
                    {
                        controller.tree.SetBool(false, "Need Drink");
                    }

                    if (!controller.tree.Test("Morale")) //if morale is below parameter threshold, leave the store.
                    {
                        if (!leftStoreUnsatisfied)
                        {
                            controller.tree.SetBool(true, "Navigate");
                        }
                    }
                }
                else
                {
                    controller.tree.SetBool(false, "Find Arcade"); //dont find an arcade.                
                }

                controller.tree.SetFloat(morale, "Morale");


                morale = Mathf.Clamp(morale, 0, maxMorale);

                //only update if hud is open
                if (customerMenu.transform.localScale.x > 0)
                {
                    UpdateStatus(); //update hud UI.
                }


            }
        }
        
    }
    /// <summary>
    /// Used to update the interaction HUD.
    /// </summary>
    public void UpdateStatus() {
        //Update HUD
        if(customerMenu != null) {

            if (morale >= happyThreshold)
            {
                customerMenu.moodStatus.text = "Very Happy";
            }
            if (morale > angryThreshold && morale < happyThreshold)
            {
                customerMenu.moodStatus.text = "Indifferent";
            }
            if (morale <= angryThreshold)
            {
                customerMenu.moodStatus.text = "Angry";
            }
            if (boredom >= boredomThreshold)
            {
                customerMenu.excitementStatus.text = "Bored";
            }
            else
            {
                customerMenu.excitementStatus.text = "Entertained";
            }
            if (hunger >= hungerThreshold)
            {
                customerMenu.hungerStatus.text = "Hungry";
            }
            else
            {
                customerMenu.hungerStatus.text = "Not Hungry";
            }
            if (thirst >= thirstThreshold)
            {
                customerMenu.thirstStatus.text = "Thirsty";
            }
            else
            {
                customerMenu.thirstStatus.text = "Not Thirsty";
            }
            if(cleanliness > 75)
                customerMenu.cleanlinessStatus.text = "Very Satisfied";
            else if(cleanliness > 50)
                customerMenu.cleanlinessStatus.text = "Satisfied";
            else if(cleanliness > 25)
                customerMenu.cleanlinessStatus.text = "Needs attention";
            else
                customerMenu.cleanlinessStatus.text = "Disgusted";

            customerMenu.walletBalance.text = wallet.ToString();
        }
    }

    private void OnDisable()
    {
        if (controller != null && tutorial != null)
        {
            if (!tutorial.sectionActive) //reset the tree when the AI object is disabled, however, only when it isnt a tutorial.
            {
                controller.ResetTree();
            }
        }
    }


    public bool InDirtyRoom()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up,out hit,4, mask)) //test whether the floor is dirty
        {
            if (hit.transform.tag == "Dirty")
            {
                return true;
            }
        }
        return false;
    }


    public float ReconfigureMorale(float range) //lose morale.
    {
        float moraleValue = 0;

        if (mlrTimer >= mlr)
        {
            mlrTimer = 0;
            moraleValue = Random.Range(0, range + 1);
        }
        mlrTimer += Time.deltaTime * 2;

        return moraleValue;
    }

    //save game Data
    public void SaveData(GameData data)
    {
        aData.objID = name;
        aData.position = transform.position;
        aData.rotation = transform.rotation.eulerAngles;
        aData.instanceID = ID;
        if (!car) //dont save traits if its a car.
        {
            aData.firstName = firstName;
            aData.lastName = lastName;
            aData.color = GetComponent<Renderer>().material.color;
            aData.morale = morale;
            aData.hunger = hunger;
            aData.thirst = thirst;
            aData.boredom = boredom;
            aData.wallet = wallet;
            aData.wanderTime = wanderTimer;
            aData.velocity = controller.navigationalMesh.velocity;
            aData.inStore = goToStore;
            animation.SaveData(ref aData);
            appearanceGenerator.SaveData(ref aData);
        }
       
        aData.behavioursData = new List<BehaviourData>();
        aData.parameterData = new List<ParameterData>();

        if (controller == null)
        {
            controller = GetComponent<AIController>();
        }
        //Save Behaviour Data.
        foreach (var node in controller.tree.nodes)
        {
            if (node.nodeType != NodeType.Root)
            {
                node.behaviour.bData.fieldData = new List<string>();
                node.behaviour.SaveData(ref aData);
                foreach (var param in node.localParams)
                {
                    param.SaveData(ref aData);
                }
            }
           
        }
        if (!car) //if not a car, dont save type
        {
            string instance = "(Clone)";
            aData.AIType = name.TrimEnd(instance.ToCharArray());
        }

        data.AIData.Add(aData);
    }

    //load game save
    public void LoadData(GameData data)
    {
        if (controller == null)
        {
            controller = GetComponent<AIController>();
        }

        if (!car) //if not a car
        {
            controller.navigationalMesh.enabled = false;
        }

        transform.position = aData.position;
        transform.rotation = Quaternion.Euler(aData.rotation);
        ID = aData.instanceID;
        if (!car) // if not a car, dont assign its traits.
        {
            GetComponent<Renderer>().material.color = aData.color;
            firstName = aData.firstName;
            lastName = aData.lastName;
            morale = aData.morale;
            hunger = aData.hunger;
            thirst = aData.thirst;
            boredom = aData.boredom;
            wallet = aData.wallet;
            wanderTimer = aData.wanderTime;
            controller.navigationalMesh.velocity = aData.velocity;
            goToStore = aData.inStore;
            appearanceGenerator.LoadData(aData.outfitData);
            animation.LoadData(aData.animationData);
        }
       //create tree
        controller.InitializeTree();
        controller.gameSaveLoaded = true;
        for (int i = 0; i < controller.tree.nodes.Count; i++) //load behaviour data.
        {
            if (controller.tree.nodes[i].nodeType != NodeType.Root)
            {
                controller.tree.nodes[i].behaviour.bData = aData.behavioursData[i - 1];
                controller.tree.nodes[i].behaviour.LoadData(aData);
                for (int j = 0; j < controller.tree.nodes[i].localParams.Count; j++)
                {
                    controller.tree.nodes[i].localParams[j].LoadData(aData);
                }
            }
           
        }      
    }
}
