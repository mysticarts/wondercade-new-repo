﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

namespace Tree.Editor
{
    public class BehaviourMenus : MonoBehaviour
    {
        public static _Behaviour CreateBehaviour(string behaviourName, AITree tree)
        {
            //Type typeas = Type.GetType(name);
            _Behaviour behaviour = ScriptableObject.CreateInstance(behaviourName) as _Behaviour;
           // string treePath = AssetDatabase.GetAssetPath(tree);
            if (AssetDatabase.Contains(tree))
            {
                AssetDatabase.AddObjectToAsset(behaviour, tree);
            }
            AssetDatabase.Refresh();
            return behaviour;

        }
    }
}
#endif


