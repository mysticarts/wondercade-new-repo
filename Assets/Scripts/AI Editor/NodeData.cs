﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Tree.Editor
{
    [System.Serializable]
    public enum NodeType
    {
        Root = 0,
        Condition = 1,
        Action = 2,
        Selector = 3
    }
    [System.Serializable]
    public class NodeData : ScriptableObject
    {      
        public _Behaviour behaviour; //the attached behaviour
        public int id = 0; 
        public NodeType nodeType; //its type
        public string name = "Untitled"; //its name
        public List<Socket> sockets = new List<Socket>(); //its sockets
        public bool startConnection = false; //whether a connection has been stqrted
        public Socket SelectedSocket; //the currently selected socket
        public List<Parameters> localParams = new List<Parameters>();
        //if the editor is currently being utilized
#if UNITY_EDITOR
        public Rect displayWindow; //its display rect
        public TreeEditorWindow editorWindow; //the current editor window that is open
        public Color colour; //current colour
        public Texture successTexture;
        public int selected = 0;
        public bool connectionSelected = false; //to test whether the connection curve from its self to its connected node is selected.
        public List<Rect> localParamsDisplay = new List<Rect>(); //rect transforms of all the parameter displays.
        public Parameters currentSelectedParam; //current selected param reference
        public Rect currentSelectedParamDisplay; //current selected param rect transform
        public float localParamsDisplayOffset = 80; //the offset for each param transform.
        public List<int> selectedParameter = new List<int>();
#endif


        public bool TestParameters()
        {
            foreach (var param in localParams)
            {
                if (param.Test()) //if the parameter conditions are met
                {
                    continue; //continue checks
                }
                else
                {
                    return false; //if one returns fails, return false
                }
            }
            return true; //if all parameter conditions are met, return true.
        }



#if UNITY_EDITOR
        public void CreateNode(int nodeID,int socketAmount, int socketID, TreeEditorWindow window)
        {
            displayWindow = new Rect(100, 100, 100, 100); //set nodes display
            CreateSockets(socketAmount, socketID); //create sockets
            id = nodeID; //set id
            editorWindow = window; //set reference to current editor window
        }       

        public void CreateSockets(int amount, int id)
        {           
            for (int i = 0; i < amount; i++)
            {
                //create new socket
                Socket socket = new Socket();
                if (i == 0) //i == 0, create input socket
                {
                    socket.type = SocketType.Input; //set type
                    socket.display = new Rect(displayWindow.position.x, displayWindow.position.y, 20, 10); //set display position to the left side of the node
                }
                if (i == 1) //i == 1, create output socket
                {
                    socket.type = SocketType.Output; //set type
                    socket.display = new Rect(displayWindow.position.x, displayWindow.position.y, 20, 10); //set display position to the right side of the node
                }
                socket.attachedNode = this; //set the sockets node reference to this.
                socket.id = id; //set the sockets id
                sockets.Add(socket); //add the socket to nodes list.
                id++; //increase id
            }
        }

        public void Remove()
        {
            editorWindow.tree.nodes.Remove(this);
            foreach (var socket in sockets)
            {
                if (socket.previousSocket != null) //if the current socket has a previouse socket reference
                {
                    socket.previousSocket.connection.Remove(this); //remove connection
                }
                foreach (var connections in socket.connection)
                {
                    foreach (var cS in connections.sockets)
                    {
                        if (cS.previousSocket != null) //if the connection socket has a previouse socket reference.
                        {
                            cS.previousSocket = null; //set references to null.
                            cS.previousNode = null; 
                        }
                    }
                }
            }
            //if the currently selected node is the one being removed, set current selected node null
            if (editorWindow.currentSelectedNode == this)
            {
                editorWindow.currentSelectedNode = null;
            }
            //Destroy asset
            string[] filePath = AssetDatabase.FindAssets(name);
            DestroyImmediate(behaviour, true);
            DestroyImmediate(this,true);
            

            //repaint
            editorWindow.Repaint();          
        }
        public void Draw(Event e)
        {
            if (EditorApplication.isPlaying) //if the application is running
            {
                if (behaviour != null) //if there is a behaviour
                {
                    if (behaviour.result == _Behaviour.BehaviourResult.SUCCESS) //paint display green if success
                    {
                        GUI.backgroundColor = Color.green;
                    }
                    else if (behaviour.result == _Behaviour.BehaviourResult.RUNNING) //paint display orange if the behaviour is running
                    {
                        GUI.backgroundColor = new Color(1, 0.55f, 0,1); 
                    }
                    else if (behaviour.result == _Behaviour.BehaviourResult.FAILURE) //else paint red
                    {
                        GUI.backgroundColor = Color.red; 
                    }
                }
            }

          
            displayWindow = GUI.Window(id, displayWindow, DrawWindow, "" + name);
            GUI.backgroundColor = Color.white;

            for (int i = 0; i < sockets.Count; i++)
            {
                if (nodeType == NodeType.Action) //if the current node is an action node
                { 
                    if (sockets[0].previousNode != null) //if previous node is not null
                    {
                        if (sockets[0].previousNode.nodeType == NodeType.Selector) //if the previous node is a selector
                        {
                            sockets[0].Draw(); //only draw the input socket
                            GUI.color = Color.white;
                        }
                        else
                        {
                            sockets[i].Draw(); //draw both sockets
                        }
                    }
                    else
                    {
                        sockets[i].Draw(); //draw both sockets if previous node is null
                    }
                }
                else
                {
                    sockets[i].Draw(); //draw both sockets for every other kind.
                }
            }

            if (e.isScrollWheel && e.type == EventType.ScrollWheel) //if the mouse wheel is scrolled
            {
                Resize(e); //resize
            }

            if (e.type == EventType.MouseDrag && e.button == 2) //if the scroll wheel is pressed, reposition. 
            {
                RepositionWindow(e);
            }

            if (displayWindow.Contains(e.mousePosition) && e.button == 1) //if the user has right clicked in window
            {
                GenericMenu menu = new GenericMenu(); //create new menu
                menu.AddItem(new GUIContent("Remove Node"), false, Remove); //remove option
                menu.ShowAsContext();
            }


            if (startConnection)
            {
                Vector2 mousePos = editorWindow.e.mousePosition; //get mouse pos
                Rect cursorRec = new Rect(mousePos, new Vector2(0, 0)); //create rec at mouse pos
                editorWindow.UpdateConnectionCurve(this.SelectedSocket.display, cursorRec); //pass to curve function.
            }
        }


        public void DrawWindow(int id)
        {
            Event e = Event.current;
            //if user left clicks on node window
            if (e.button == 0 && e.type == EventType.MouseDown)
            {
                editorWindow.currentSelectedNode = this; //set current node to this
                TreeEditorUtilities.OnCurveUnselect(editorWindow);
            }
            GUILayout.Label("" + nodeType);
            GUI.DragWindow();           
        }

        public void RepositionWindow(Event e)
        {
            //add mouse movement to displayWindow position
            displayWindow.position += e.delta;
        }

        public void Resize(Event e)
        {
            //add mouse movement to width and height
            displayWindow.width += e.delta.y * 2;
            displayWindow.height += e.delta.y * 2;

            //resize sockets
            foreach (var socket in sockets)
            {
                socket.display.width += e.delta.y * 2;
                socket.display.height += e.delta.y * 2;
                socket.display.width = Mathf.Clamp(socket.display.width, 10, 20);
                socket.display.height = Mathf.Clamp(socket.display.height, 5, 10);


            }
             //clamp width and height
            displayWindow.width = Mathf.Clamp(displayWindow.width, 50, 100);
            displayWindow.height = Mathf.Clamp(displayWindow.height, 50, 100);
        }
#endif

    }
}


