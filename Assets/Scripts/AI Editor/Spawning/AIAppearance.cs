﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AIOutFitData
{
    public List<string> outfit;
    public List<Vector4> outfitColor;
}

public enum Gender {
    MALE,
    FEMALE
}

public class AIAppearance : MonoBehaviour
{
    public AIOutFitData ofData; //outfit data
    public GameObject maleGlasses;
    public GameObject FemaleGlasses;
    public List<GameObject> maleHats = new List<GameObject>();
    public List<GameObject> femaleHats = new List<GameObject>();
    public List<GameObject> genders = new List<GameObject>();
    public List<GameObject> maleHair = new List<GameObject>();
    public List<GameObject> femaleHair = new List<GameObject>();
    public List<GameObject> maleShirts = new List<GameObject>();
    public List<GameObject> femaleShirts = new List<GameObject>();
    public List<GameObject> malePants = new List<GameObject>();
    public List<GameObject> femalePants = new List<GameObject>();
    public GameObject maleShoes;
    public GameObject FemaleShoes;

    public Gender gender;

    private List<GameObject> outfit = new List<GameObject>();
    // Start is called before the first frame update
    private void Start()
    {
        //if an outfit hasnt been assigned generate one
        if (outfit.Count == 0)
        {
            Generate();
        }
    }

    void Generate()
    {
        //choose gender
        int ran = Random.Range(0, genders.Count);
        genders[ran].SetActive(true);
        outfit.Add(genders[ran]);
        if (genders[ran].name.Contains("Male"))
        {
            GenMaleAppearance();
            gender = Gender.MALE;
        }
        if (genders[ran].name.Contains("Female"))
        {
            GenFemaleAppearance();
            gender = Gender.FEMALE;
        }
    }
    void GenMaleAppearance()
    {
        if (AssignAccessorie()) //Randomly decide if to assign an accessorie.
        {
            maleGlasses.SetActive(true);
            outfit.Add(maleGlasses);
            AssignColor(maleGlasses);
        }
        //assign clothes
        AssignClothing(maleHair, nameof(maleHair));
        AssignClothing(maleShirts, nameof(maleShirts));
        AssignClothing(malePants, nameof(malePants));
        AssignColor(maleShoes);//assign colour to outfit
        outfit.Add(maleShoes);
    }

    void GenFemaleAppearance() 
    {
        if (AssignAccessorie())//Randomly decide if to assign an accessorie.
        {
            FemaleGlasses.SetActive(true);
            outfit.Add(FemaleGlasses);
            AssignColor(FemaleGlasses);
        }
        //assign clothes
        AssignClothing(femaleHair, nameof(femaleHair));
        AssignClothing(femaleShirts, nameof(femaleShirts));
        AssignClothing(femalePants, nameof(femalePants));
        AssignColor(FemaleShoes); //assign colour to outfit
        outfit.Add(FemaleShoes);
    }

    //remove the outfit.
    void ResetApperance()
    {
        foreach (var piece in outfit)
        {
            piece.SetActive(false);
        }
        outfit.Clear();
    }

    void AssignClothing(List<GameObject> clothing, string clothingType)
    {
        //randomly choose a piece of clothing
        int ran = Random.Range(0, clothing.Count); 
        clothing[ran].SetActive(true);
        AssignColor(clothing[ran]);
        outfit.Add(clothing[ran]);

        //if its hair being chosen
        if (clothingType.Contains("Hair"))
        {
            if (AssignAccessorie()) //Randomly decide if to assign an accessorie.
            {
                //randomly choose an accessorie.
                int i = ran;
                ran = Random.Range(0, clothing[ran].transform.childCount);
                clothing[i].transform.GetChild(ran).gameObject.SetActive(true);
                AssignColor(clothing[i].transform.GetChild(ran).gameObject);
                outfit.Add(clothing[i].transform.GetChild(ran).gameObject);
            }
        } 
    }

    //randomly choose a colour
    void AssignColor(GameObject clothing)
    {
        Renderer ren = clothing.GetComponent<Renderer>();

        foreach (var mat in ren.materials)
        {
            float r = Random.Range(0, 256);
            float g = Random.Range(0, 256);
            float b = Random.Range(0, 256);

            mat.color = new Color(r / 255, g / 255, b / 255);
        }
    }
    //random bool
    bool AssignAccessorie()
    {
        return Random.value < 0.5f;
    }

    //save outfit data
    public void SaveData(ref AIData data)
    {
        ofData.outfit = new List<string>();
        foreach (var piece in outfit)
        {
            ofData.outfit.Add(piece.name);
            ofData.outfitColor.Add(piece.GetComponent<Renderer>().material.color);
        }
        data.outfitData = ofData;
    }

    //reload outfit
    public void LoadData(AIOutFitData data)
    {
        ResetApperance();
        outfit.Clear();

        List<GameObject> availableClothes = new List<GameObject>();
        availableClothes.Add(maleGlasses);
        availableClothes.Add(FemaleGlasses);
        availableClothes.AddRange(genders);
        availableClothes.AddRange(maleHats);
        availableClothes.AddRange(femaleHats);
        availableClothes.AddRange(maleHair);
        availableClothes.AddRange(femaleHair);
        availableClothes.AddRange(maleShirts);
        availableClothes.AddRange(femaleShirts);
        availableClothes.AddRange(malePants);
        availableClothes.AddRange(femalePants);
        availableClothes.Add(maleShoes);
        availableClothes.Add(FemaleShoes);


        for (int i = 0; i < data.outfit.Count; i++)
        {
            GameObject clothing = FindClothingItem(data.outfit[i], availableClothes);
            if (clothing != null)
            {
                clothing.SetActive(true);
                clothing.GetComponent<Renderer>().material.color = data.outfitColor[i];
                outfit.Add(clothing);
            }

        }
     
    }

    GameObject FindClothingItem(string name, List<GameObject> clothes)
    {
        foreach (var piece in clothes)
        {
            if (piece.name == name)
            {
                return piece;
            }
        }
        return null;
    }


}
