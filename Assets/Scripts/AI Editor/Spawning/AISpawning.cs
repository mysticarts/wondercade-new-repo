﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System;

public class AISpawning : MonoBehaviour
{
    public AdvertisingManager advertisingManager; //advertising manager
    public int populationDensity = 0; //the max population outside the building.

    public List<AIController> AITypes = new List<AIController>(); //the types of AI 
    [HideInInspector]
    public List<AIController> AIPool = new List<AIController>(); //the AI pool
    [HideInInspector]
    public List<AIController> activeAI = new List<AIController>(); //the AI active in scene
    public List<Transform> spawningPoints = new List<Transform>(); //the AI spawning points
    public int poolSize = 0; //the size of object pool
    public float spawnFrequency = 0; //how frequently the AI spawn.
    [HideInInspector]
    public int activeAICounter = 0; //how much AI are active
    private float currentSpawnTimer = 0;

    [Tooltip("The minimum amount of customers spawned with absolute worst shop stats")]
    public int minOffStreetCustomers = 1;
    [Tooltip("The maximum amount of customers spawned with absolute worst shop stats")]
    public int maxOffStreetCustomers = 4;

    public float moraleMultiplier = 2;

    private DayNightCycle cycle;

    //names
    public List<string> firstNames_Female;
    public List<string> firstNames_Male;
    public List<string> lastNames;

    //comment type
    public enum Comment {
        DIRTY,
        BORED,
        HUNGRY,
        THIRSTY,
        LOW_MONEY,
        EXCITED,
        CLEAN,
        LOTS_MONEY
    }

    //interaction comments
    public int veryTriggerPercentage = 80;
    public int normalTriggerPercentage = 50;
    public List<string> dirtyComments;
    public List<string> veryDirtyComments;
    public List<string> boredComments;
    public List<string> veryBoredComments;
    public List<string> hungryComments;
    public List<string> veryHungryComments;
    public List<string> thirstyComments;
    public List<string> veryThirstyComments;
    public List<string> lowMoneyComments;
    public List<string> veryLowMoneyComments;
    public List<string> excitedComments;
    public List<string> veryExcitedComments;
    public List<string> lotsMoneyComments;
    public List<string> veryLotsMoneyComments;
    public List<string> cleanComments;
    public List<string> veryCleanComments;
    public List<string> indifferentComments;
   

    void Start()
    {
        cycle = FindObjectOfType<DayNightCycle>();
        if (AIPool.Count == 0 && AITypes.Count > 0) //a pool hasnt been generated.
        {
            GeneratePool();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (cycle.storeOpen) //store is open, start spawning them
        {
            currentSpawnTimer += Time.deltaTime * 2;

            if (currentSpawnTimer >= spawnFrequency)
            {
                if (activeAI.Count < populationDensity)
                {
                    Spawn();
                }
            }
        }
      
    }


    //on new day calculate new max
    public void DeterminePopulationDensityforDay()
    {
        int bareMinimumAmount = UnityEngine.Random.Range(minOffStreetCustomers, maxOffStreetCustomers);
        int advertisingAddition = advertisingManager.GetAdvertisingAddition();
        float satisfaction = (WeekStats.GetSatisfaction() / 100) * moraleMultiplier;

        // your morale multplier will vary between this and (2-this), so make it between 0 and 1
        float baseMultiplier = 0.3f;

        populationDensity = Mathf.CeilToInt((bareMinimumAmount + advertisingAddition) * (baseMultiplier + (1 - baseMultiplier) * satisfaction));

    }

    public string GenerateComment(AIStats ai)
    {

        //get random order
        //choose first one that can have comment
        int count = 0;
        while (true)
        {
            //break if more than 100 tries as indifferent (very rare)
            if (count > 100) return indifferentComments[UnityEngine.Random.Range(0, indifferentComments.Count)];
            int randomIndex = UnityEngine.Random.Range(0, (int)Enum.GetValues(typeof(Comment)).Cast<Comment>().Last());
            count++;
            //picks random state, if it doesn't have comment, try again

            if ((Comment)randomIndex == Comment.BORED)
            {
                if (ai.boredom > veryTriggerPercentage)
                    return veryBoredComments[UnityEngine.Random.Range(0, veryBoredComments.Count)];
                else if (ai.boredom > normalTriggerPercentage)
                    return boredComments[UnityEngine.Random.Range(0, boredComments.Count)];
            }

            if ((Comment)randomIndex == Comment.EXCITED)
            {
                if (ai.boredom < (100 - veryTriggerPercentage))
                    return veryExcitedComments[UnityEngine.Random.Range(0, veryExcitedComments.Count)];
                else if (ai.boredom < (100 - normalTriggerPercentage))
                    return excitedComments[UnityEngine.Random.Range(0, excitedComments.Count)];
            }

            if ((Comment)randomIndex == Comment.DIRTY)
            {
                if (ai.cleanliness < (100 - veryTriggerPercentage))
                    return veryDirtyComments[UnityEngine.Random.Range(0, veryDirtyComments.Count)];
                else if (ai.cleanliness < (100 - normalTriggerPercentage))
                    return dirtyComments[UnityEngine.Random.Range(0, dirtyComments.Count)];
            }

            if ((Comment)randomIndex == Comment.CLEAN)
            {
                if (ai.cleanliness > veryTriggerPercentage)
                    return veryCleanComments[UnityEngine.Random.Range(0, veryCleanComments.Count)];
                else if (ai.cleanliness > normalTriggerPercentage)
                    return cleanComments[UnityEngine.Random.Range(0, cleanComments.Count)];
            }

            if ((Comment)randomIndex == Comment.HUNGRY)
            {
                if (ai.hunger > veryTriggerPercentage)
                    return veryHungryComments[UnityEngine.Random.Range(0, veryHungryComments.Count)];
                else if (ai.hunger > normalTriggerPercentage)
                    return hungryComments[UnityEngine.Random.Range(0, hungryComments.Count)];
            }

            if ((Comment)randomIndex == Comment.THIRSTY)
            {
                if (ai.thirst > veryTriggerPercentage)
                    return veryThirstyComments[UnityEngine.Random.Range(0, veryThirstyComments.Count)];
                else if (ai.thirst > normalTriggerPercentage)
                    return thirstyComments[UnityEngine.Random.Range(0, thirstyComments.Count)];
            }

            if ((Comment)randomIndex == Comment.LOTS_MONEY)
            {
                if (ai.wallet > ((float)ai.maxWalletCapacity * (veryTriggerPercentage / 100f)))
                    return veryLotsMoneyComments[UnityEngine.Random.Range(0, veryLotsMoneyComments.Count)];
                else if (ai.wallet > ((float)ai.maxWalletCapacity * (normalTriggerPercentage / 100f)))
                    return lotsMoneyComments[UnityEngine.Random.Range(0, lotsMoneyComments.Count)];
            }

            if ((Comment)randomIndex == Comment.LOW_MONEY)
            {
                if (ai.wallet < (100 - ((float)ai.maxWalletCapacity * (veryTriggerPercentage / 100f))))
                    return veryLowMoneyComments[UnityEngine.Random.Range(0, veryLowMoneyComments.Count)];
                else if (ai.wallet > (100 - ((float)ai.maxWalletCapacity * (normalTriggerPercentage / 100f))))
                    return lowMoneyComments[UnityEngine.Random.Range(0, lowMoneyComments.Count)];
            }

        }
    }


    //enable/disable all the AI movement when the AI tutorial is called
    public void TutorialEvent(bool status)
    {
        foreach (var ai in activeAI)
        {
            ai.enabled = status;
            ai.GetComponent<AIStats>().enabled = status;
            ai.GetComponentInChildren<EmotesHandler>().transform.localEulerAngles = Vector3.zero;
            ai.GetComponentInChildren<EmotesHandler>().enabled = status;
        }
    }


    //remove AI from active AI when they despawn
    public void Despawn(AIController controller)
    {
        controller.gameObject.SetActive(false);
      //  AIPool.Add(controller);
        activeAI.Remove(controller);
        activeAICounter--;
    }

    void Spawn()
    {
        if (activeAI.Count < AIPool.Count) //if object pool still has inactive AI
        {
            int AISelector = UnityEngine.Random.Range(0, AIPool.Count); //randomly select

            if (!AIPool[AISelector].gameObject.activeInHierarchy && !activeAI.Contains(AIPool[AISelector])) //if the selected AI hasnt been picked
            {
                //set it active
                AIPool[AISelector].gameObject.SetActive(true);
                AIStats stats = AIPool[AISelector].GetComponent<AIStats>();
                stats.spawnManager = this;
                stats.wallet = UnityEngine.Random.Range(0, stats.maxWalletCapacity); //give it money
                activeAI.Add(AIPool[AISelector]);
                // AIFound = true;
                currentSpawnTimer = 0; //reset timer

                stats.firstName = stats.FirstName(); //gen first and last names
                stats.lastName = stats.LastName();

            }
        }
    }


    //randomize pool
    public void ShufflePool()
    {
        int size = AIPool.Count;
        int last = size - 1;
        for (int i = 0; i < last; i++)
        {
            int r = UnityEngine.Random.Range(i, size);
            AIController temp = AIPool[i];
            AIPool[i] = AIPool[r];
            AIPool[r] = temp;
        }


    }



    public void GeneratePool()
    {
        int aiTypeIter = 0;
        for (int i = 0; i < poolSize; i++)
        {
            if (i == (poolSize/2)) //if halfway through pool, start spawning next type
            {
                aiTypeIter++;
            }

            int ranSpawnPoint = UnityEngine.Random.Range(0, spawningPoints.Count); //get random spawn point

            NavMeshHit hit;

            if (NavMesh.SamplePosition(spawningPoints[ranSpawnPoint].position, out hit,1000, 1 << NavMesh.GetAreaFromName("Walkable"))) //if the spawn point is on the nav mesh and not on the arcade floor.
            {
                AIController generatedAI = Instantiate(AITypes[aiTypeIter], hit.position, spawningPoints[ranSpawnPoint].rotation); //spawn AI.
                generatedAI.gameObject.SetActive(false);
                AIPool.Add(generatedAI);
            }
        }
        ShufflePool();
    }

 

}
