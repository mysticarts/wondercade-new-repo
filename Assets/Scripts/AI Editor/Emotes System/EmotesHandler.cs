﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;


public enum EmotionalStatus
{
    HAPPY = 0,
    ANGRY = 1,
    INDIFFERENT = 2,
    THIRSTY = 3,
    HUNGRY = 4,
}



public class EmotesHandler : MonoBehaviour
{
    public AIStats stats;
    public List<EmotionalStatus> moods = new List<EmotionalStatus>(); //the current emotions of the AI
    private List<GameObject> moodsDisplay = new List<GameObject>(); //the display for the current mood
    public float displayFrequency = 0; //how frequently the emotes display
    private float currentFrequency = 0; //timer
    public float displayTimer = 0; //how long the bubble displays
    private float currentDisplayTime = 0; //timer
    private float emoteDisplayTime = 0; //how long each emote displays for
    private float currentemoteDisplayTime = 0;
    private bool displayBubble = false;
    public float bubbleScale = 0; //bubbles scale
  //  private int iter = 0;
    [Header("----Emotes----")]
    public GameObject happyEmote;
    public GameObject angryEmote;
    public GameObject hungryEmote;
    public GameObject thirstyEmote;
    public GameObject indifferentEmote;
    private GameObject currentEmote;
      
    // Update is called once per frame
    void Update()
    {
        DisplayEmote();

        if (!displayBubble) //if not displaying the emote bubble
        {
            //check its moods
            CheckMood(stats.happyThreshold,0, stats.morale, EmotionalStatus.HAPPY, happyEmote, false, false);
            CheckMood(stats.angryThreshold,0, stats.morale, EmotionalStatus.ANGRY, angryEmote, true, false);
            CheckMood(stats.happyThreshold, stats.angryThreshold, stats.morale, EmotionalStatus.INDIFFERENT, indifferentEmote, false, true);
            CheckMood(stats.hungerThreshold,0, stats.hunger, EmotionalStatus.HUNGRY, hungryEmote, false, false);
            CheckMood(stats.thirstThreshold,0, stats.thirst, EmotionalStatus.THIRSTY, thirstyEmote, false, false);
        }

    }


    void DisplayEmote()
    {
        if (currentFrequency <= displayFrequency) //if timer hasnt passed threshold
        {
            currentFrequency += Time.deltaTime % 60;
        }
        else
        {
            if (!displayBubble) //if bubble isnt being displayed
            {
                Tween.LocalScale(transform, new Vector3(bubbleScale, bubbleScale, bubbleScale), 0.3f, 0); //tween to scale
                emoteDisplayTime = displayTimer / moods.Count; //determine how much display time each emote has.
                if (moodsDisplay.Count > 0) //if there are emotes
                {
                    moodsDisplay[0].SetActive(true);
                    currentEmote = moodsDisplay[0];
                    displayBubble = true;
                }
            }
            if (displayBubble)
            {
                currentDisplayTime += Time.deltaTime % 60;
                currentemoteDisplayTime += Time.deltaTime % 60;
                if (currentemoteDisplayTime >= emoteDisplayTime) //emote timer has passed threshold
                {
                    moods.RemoveAt(0); //remove it
                    moodsDisplay[0].SetActive(false);
                    moodsDisplay.RemoveAt(0);
                    if (moodsDisplay.Count > 0) // if there are emotes left to display, set the next one active
                    {
                        moodsDisplay[0].SetActive(true);
                        currentEmote = moodsDisplay[0];
                    }
                    currentemoteDisplayTime = 0;
                }

                if (currentDisplayTime >= displayTimer) //close bubble once threshold is crossed.
                {
                    displayBubble = false;
                    currentFrequency = 0;
                    currentDisplayTime = 0;
                    currentEmote.SetActive(false);
                    Tween.LocalScale(transform, new Vector3(0, 0, 0), 0.3f, 0);
                }
            }


        }
    }

    void CheckMood(float threshold, float secondThreshold, float continuous, EmotionalStatus mood, GameObject emoteDisplay, bool belowThreshold, bool betweenThreshold)
    {
        //checking if a value needs to be greater than a threshold
        if (!belowThreshold && !betweenThreshold)
        {
            if (continuous >= threshold) //if the continuously changing value is higher than threshold
            {
                if (!moods.Contains(mood)) //not already in known moods
                {
                    moods.Add(mood); 
                    moodsDisplay.Add(emoteDisplay);
                }
            }
            else
            {
                if (moods.Contains(mood)) //remove it if no longer true
                {
                    moods.Remove(mood);
                    moodsDisplay.Remove(emoteDisplay);
                }
            }
        }
        //checking if a value needs to be between to thresholds
        if (betweenThreshold)
        {
            if (continuous <= threshold && continuous >= secondThreshold) //if the continuously changing value is between threshold
            {
                if (!moods.Contains(mood)) //if not already in known moods
                {
                    moods.Add(mood);
                    moodsDisplay.Add(emoteDisplay);
                }
            }
            else
            {
                if (moods.Contains(mood))//remove it if no longer true
                {
                    moods.Remove(mood);
                    moodsDisplay.Remove(emoteDisplay);
                }
            }
        }
        //checking if a value needs to be below a threshold
        if (belowThreshold && !betweenThreshold)
        {
            if (continuous <=  threshold)//if the continuously changing value is below threshold
            {
                if (!moods.Contains(mood)) //if not already in known moods
                {
                    moods.Add(mood);
                    moodsDisplay.Add(emoteDisplay);
                }
            }
            else
            {
                if (moods.Contains(mood))//remove it if no longer true
                {
                    moods.Remove(mood);
                    moodsDisplay.Remove(emoteDisplay);
                }
            }
        }


    }



}
