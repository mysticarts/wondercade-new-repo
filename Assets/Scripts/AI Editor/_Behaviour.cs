﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;

[System.Serializable]
public struct BehaviourData
{    
    //all field data gets written out as a string.
    public List<string> fieldData;
}


[System.Serializable]
public abstract class _Behaviour : ScriptableObject {

    [HideInInspector]
    public GameObject agentObj; //the attached agent game object
    [HideInInspector]
    public NavMeshAgent navigation; //the attached agents nav mesh agent
    public NodeData node; //the behaviours node
    [HideInInspector]
    public AIController controller; //the agents controller 

    public BehaviourData bData = new BehaviourData();
    public bool gameSaveLoaded = false;

    public enum BehaviourResult
    {
        SUCCESS = 0,
        FAILURE = 1,
        RUNNING = 2
    }
    // Use this for initialization
    public AITree tree;

    /// <summary>
    /// Called Upon Application Start Up.
    /// </summary>
    public abstract void Start();

    /// <summary>
    /// Called before entering the behaviour execute function.
    /// </summary>
    public abstract void OnEnter();


    public BehaviourResult result = BehaviourResult.FAILURE; //attach to the end of every return if you wish to see its result in the editor tool.

    /// <summary>
    /// Called Every Frame. Do Main Behaviour Here.
    /// </summary>
    public abstract BehaviourResult execute(); //execute behaviour

    /// <summary>
    /// Returns The Weight Value Of The Executed Behaviour. (Based On Whats Been Provided).
    /// </summary>
    public abstract float CalculateWeight(); //calc weight score

    public virtual void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(result) + (int)result); //save result.
        data.behavioursData.Add(bData);
    }

    public virtual void LoadData(AIData data)
    {
        int r = 0;
        AssignLoadedFieldData(nameof(result), out r);
        result = (BehaviourResult)r;
        gameSaveLoaded = true;
    }

    /// <summary>
    /// pass in the name of the variable that is getting loaded and the variable its self (Vector
    /// coordinates have to be passed in one coordinate at a time).
    /// </summary>
    public void AssignLoadedFieldData<T>(string fieldName, out T input)
    {
        T value = default(T); //set the default value based on the type passed in.
        foreach (var fieldData in bData.fieldData) //loop through all field data strings
        {
            if (fieldData.Contains(fieldName)) //if name matches
            {
                //assign data based on value type passed.
                string field = fieldData; 
                field = field.Replace(fieldName, "");
                value = (T)System.Convert.ChangeType(field, typeof(T));
                break;
            }
        }
        //assign out value to default if field could not be found in saved data.
        input = value;
    }

}
