﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Tree.Editor
{
    public class TreeEditorUtilities
    {
#if UNITY_EDITOR
        /// <summary>
        /// Responsible for drawing the background grid
        /// </summary>
        public static void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor, TreeEditorWindow editorWindow)
        {
            //get the widths of the individual cells
            int widthDivs = Mathf.CeilToInt(editorWindow.position.width / gridSpacing);
            //get the heights of the individual cells
            int heightDivs = Mathf.CeilToInt(editorWindow.position.height / gridSpacing);


            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            //Draw horizontal lines
            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0), new Vector3(gridSpacing * i, editorWindow.position.height, 0f));
            }
            //draw vertical lines
            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0), new Vector3(editorWindow.position.width, gridSpacing * j, 0f));
            }
            //set line colours.
            Handles.color = Color.white;
            Handles.EndGUI();
        }

        public static void OnCurveUnselect(TreeEditorWindow editorWindow)
        {
            if (editorWindow.tree != null)
            {
                foreach (var node in editorWindow.tree.nodes)
                {
                    if (node != null)
                    {
                        node.connectionSelected = false;
                    }
                }
            }
        }

        public static void DrawConnectionCurves(Rect start, Rect end, TreeEditorWindow editorWindow, NodeData desiredNode = null,List<NodeData> nodes = null)
        {
            //get the start position
            Vector2 startPos = new Vector2(start.center.x + start.width / 2, start.y + start.height / 2);
            //get the end position
            Vector2 endPos = new Vector2(end.x, end.y + end.height / 2);
            //get the start direction
            Vector2 startTan = startPos + Vector2.right * 50;
            //get the end direction
            Vector2 endTan = endPos + Vector2.left * 50;
            //set curve colour


            Color curveColour = Color.grey;
            Handles.DrawBezier(startPos, endPos, startTan, endTan, curveColour, null, 9);
            //Draw Bezier Curves
            curveColour = Color.black;
            if (nodes != null)
            {
                foreach (var node in nodes)
                {
                    if (node == desiredNode)
                    {
                        if (node.connectionSelected)
                        {
                            curveColour = Color.cyan;
                        }
                        else
                        {
                            curveColour = Color.black;
                        }
                        Handles.DrawBezier(startPos, endPos, startTan, endTan, curveColour, null, 4);
                    }
                }
            }
            else
            {
                Handles.DrawBezier(startPos, endPos, startTan, endTan, curveColour, null, 4);
            }


            Event e = Event.current;

            if (nodes != null)
            {
                foreach (var node in nodes)
                {
                    if (node == desiredNode)
                    {
                        if (!node.connectionSelected)
                        {
                            float mouseDistance = HandleUtility.DistancePointBezier(e.mousePosition, startPos, endPos, startTan, endTan);

                            if (mouseDistance < 2)
                            {
                                if (e.button == 0 && e.type == EventType.MouseDown)
                                {
                                    OnCurveUnselect(editorWindow);
                                    editorWindow.currentSelectedNode = node;
                                    node.connectionSelected = true;
                                    break;
                                }
                            }
                        }
                    }
                   

                }              
            }
        }

#endif

    }
}



