﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;


struct Branch
{
    public List<NodeData> node; //the nodes in a branch
}

public class AIController : MonoBehaviour {

    private NodeData root; //root of the tree
    private AITree prefabedTree;
    public AITree tree; //the tree
   // private AITree _tree;
    public NavMeshAgent navigationalMesh; //the nav mesh agent
    private List<Branch> branches = new List<Branch>(); //the branchs
    [HideInInspector]
    //public Action choseAction; //the chosen action of the selector node.
    public bool gameSaveLoaded = false;
    private bool treeCloned = false;
    private float currentDelay = 0;



    // Use this for initialization
    void Start () {
        prefabedTree = tree;
        if (!gameSaveLoaded && !treeCloned)
        {
            InitializeTree();
        }
        
    }

    public void InitializeTree()
    {
        if (tree != null)
        {
            //  runTimeTree = Instantiate(tree);
            AITree reference = tree;
            tree = Instantiate(reference);

            DeepCopy(tree);


            root = tree.nodes[0]; //assign root node to the first in the tree.

            //create branchs based on the number connections the root node has
            for (int i = 0; i < root.sockets[1].connection.Count; i++)
            {
                Branch newBranch = new Branch(); //create new branch
                newBranch.node = CreateBranch(i);
                branches.Add(newBranch); //add branch               
            }

            foreach (var branch in branches)
            {
                NodeData previousNode = null;

                foreach (var node in branch.node)
                {
                    if (previousNode != null)
                    {
                        //assign the conditions references in the action scripts based on the previous node being a condition and the current node being an action.
                        if (previousNode.nodeType == NodeType.Condition && node.nodeType == NodeType.Action)
                        {
                            (node.behaviour as Action).condition = (Condition)previousNode.behaviour;
                        }
                    }
                    previousNode = node;
                }
            }

            treeCloned = true;
        }
    }

    /// <summary>
    /// Resets the tree to original scritable object asset and reclones it.
    /// </summary>
    public void ResetTree()
    {
        tree = prefabedTree;
        InitializeTree();
    }

    // Update is called once per frame
    void Update()
    {
        if (tree != null && treeCloned) //if the user has a nav mesh and a tree
        {
            ExecuteTree(); //execute tree
        }
    }

    /// <summary>
    /// Prevents Behaviours Staying Highlighted When Not Being Executed.
    /// </summary>
    private void OnExitBranch(Branch branch, int startIndex)
    {
        for (int n = startIndex; n < branch.node.Count; n++) //begin from the start index provided.
        {
            branch.node[n].behaviour.result = _Behaviour.BehaviourResult.FAILURE; //set the result to fail.

            if (branch.node[n].nodeType == NodeType.Selector) //if its a selector node. Set all results to fail.
            {
                Selector selector = (Selector)branch.node[n].behaviour;
                selector.chosenAction = null;
                for (int a = 0; a < selector.actions.Count; a++)
                {
                    selector.actions[a].result = _Behaviour.BehaviourResult.FAILURE;
                }
            }


        }
    }

    void ExecuteTree()
    {

        int b = 0; //b = branch
        int n = 0; //n = node
     
        while (b < branches.Count) //branch iter is less than branch count.
        {
            if (branches[b].node[n].TestParameters()) //if nodes parameters are met
            {
                if (branches[b].node[n].behaviour.execute() == _Behaviour.BehaviourResult.SUCCESS ||
                                branches[b].node[n].behaviour.execute() == _Behaviour.BehaviourResult.RUNNING) //if behaviour is a success or is running
                {
                    if (n == branches[b].node.Count - 1) //if there are no more nodes remaining in the branch
                    {
                        n = 0; //move onto the next
                        b++;
                    }
                    else
                    {
                        n++; //progress down the branch.
                    }

                }
                else
                {
                    OnExitBranch(branches[b], n + 1); //if behaviour has failed, leave branch and reset all results. from the failed node

                    n = 0;
                    b++;
                }
            }

            else
            {
                OnExitBranch(branches[b],n);  //if parameter has failed, leave branch and reset all results. from the failed node

                n = 0;
                b++;
            }
        }
    }
    /// <summary>
    /// Return a node based on its name.
    /// </summary>
    NodeData FindNodeByName(string name)
    {
        NodeData desiredNode = null;

        foreach (var node in tree.nodes)
        {
            if (node.name == name)
            {
                desiredNode = node;
                return desiredNode;
            }
        }

        return null;
    }



    void DeepCopy(AITree tree)
    {
        bool nodesCloned = false; //keeps track of whether all the nodes are cloned.

        for (int i = 0; i < tree.nodes.Count; i++)
        {
            if (!nodesCloned) //if all the nodes havent been cloned yet.
            {
                tree.nodes[i] = Instantiate(tree.nodes[i]); //clone itself and set it to its clone.
#if UNITY_EDITOR
                if (tree.nodes[i].connectionSelected)
                {
                    tree.nodes[i].connectionSelected = false;
                }
#endif

                if (tree.nodes[i].behaviour != null) //the there is a behaviour attached.
                {
                    tree.nodes[i].behaviour = Instantiate(tree.nodes[i].behaviour); //clone the behaviour and assign it.
                    tree.nodes[i].behaviour.agentObj = gameObject;
                    tree.nodes[i].behaviour.navigation = navigationalMesh;
                    tree.nodes[i].behaviour.tree = tree;
                    tree.nodes[i].behaviour.controller = this;
                    tree.nodes[i].behaviour.node = tree.nodes[i];
                    tree.nodes[i].behaviour.Start();

                }
            }         
            if (i == tree.nodes.Count - 1 && !nodesCloned) //if the iterator has reached the end of the tree.
            {
                nodesCloned = true; //set all value true
                i = 0;   //restart the loop
            }

            if (nodesCloned) //if all the nodes have been cloned
            {
                for (int j = 0; j < tree.nodes[i].sockets.Count; j++)
                {
                    Socket socket = tree.nodes[i].sockets[j]; //for tidyness assign a reference of the current socket.

                    socket.attachedNode = tree.nodes[i]; //reassign the attached node to the new cloned node

                    if (socket.type == SocketType.Input && i > 0) //if its not the root node and its an input.
                    {
                        socket.previousNode = FindNodeByName(socket.previousNode.name); //reassign the previous node by searching the new tree for the cloned node by name.
                        //since the cloned nodes will have the same name as the original tree, the cloned node should be found and replace the original one.
                    }

                    if (socket.type == SocketType.Output) //if its an output socket.
                    {
                        NodeData[] connectedNodes = socket.connection.ToArray(); // get the current connections for reference.

                        socket.connection.Clear(); //clear it.

                        foreach (var c in connectedNodes) // for each node in the references
                        {
                            socket.connection.Add(FindNodeByName(c.name)); //reconnect it with the new cloned node.
                        }
                    }
                }
            }       
        }
    }

    List<NodeData> CreateBranch(int i)
    {     
        List<NodeData> branch = new List<NodeData>();
        NodeData currentNode = root; //set the current node at the top of the tree

        //if a current node is set
        while (currentNode != null) //if the current node is at a node
        {
            if (currentNode.nodeType == NodeType.Root) //if its the root node
            {
                currentNode = currentNode.sockets[1].connection[i].sockets[0].attachedNode;//translate down branch to the first node based on direction provided (i)
                if (currentNode.behaviour != null && currentNode.nodeType != NodeType.Selector) //if it has a behaviour and it isnt a selector.
                {                    
                    branch.Add(currentNode); //create deep copy
                }
            }
            else
            {
                if (currentNode.sockets[1].connection.Count > 0) //if the current node has a connection
                {
                    if (currentNode.nodeType == NodeType.Selector) //if current node is a selector
                    {
                        if ((currentNode.behaviour as Selector).actions.Count == 0) //if selectors actions hasnt been assigned.
                        {
                            foreach (var connection in currentNode.sockets[1].connection) //assign actions based on what the selector is connected to.
                            {
                                (currentNode.behaviour as Selector).actions.Add((Action)connection.behaviour); //create deep copy
                                (connection.behaviour as Action).selector = (Selector)currentNode.behaviour;
                            }
                        }
                        if (currentNode.behaviour != null) //if behaviour isnt null on node
                        {
                            branch.Add(currentNode); //create a deep copy                           
                        }
                        currentNode = null; //end loop.
                    }
                    else
                    {
                        currentNode = currentNode.sockets[1].connection[0].sockets[0].attachedNode; //move to the next node              
                        if (currentNode.behaviour != null) //node has a behaviour
                        {
                            branch.Add(currentNode); //create a deep copy
                        }
                    }        
                }
                else
                {
                    currentNode = null; //end loop
                }
            }        
        }
        return branch;
    }

}
