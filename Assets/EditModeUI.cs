﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class EditModeUI : MonoBehaviour {

    public GameObject back;
    public Vector3 back_PoppedUpPos;
    public Vector3 back_PoppedDownPos;
    public List<GameObject> editorUI = new List<GameObject>();
    public List<GameObject> purchaseModeUI = new List<GameObject>();
    public BuildingManager buildingManager;
    public ScreenUI ui;

    //pop up the edit mode UI
    public void PopUp() {
        Tween.AnchoredPosition(back.GetComponent<RectTransform>(), back_PoppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(buildingManager != null) {

            if(buildingManager.editMode) {
                foreach(var ui in editorUI)
                    ui.SetActive(true);
            }
            if(buildingManager.purchaseMode) {
                foreach (var ui in purchaseModeUI)
                    ui.SetActive(true);
            }

        }

        if(ui.spaceBar != null && ui.spaceBar_poppedDownPos != null) Tween.AnchoredPosition(ui.spaceBar, ui.spaceBar_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);

    }

    //pop down the edit mode UI
    public void PopDown() {
        Tween.AnchoredPosition(back.GetComponent<RectTransform>(), back_PoppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        if(buildingManager != null) {

            if(buildingManager.editMode) {
                foreach (var ui in editorUI)
                    ui.SetActive(false);
            }
            if(buildingManager.purchaseMode) {
                foreach(var ui in purchaseModeUI)
                    ui.SetActive(false);
            }

        }
    }
}
